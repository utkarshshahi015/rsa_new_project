<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">वाहन/नियमावली एवं महत्वपूर्ण शासनादेश</h2>
               <table  width="100%" border="2" cellspacing="2" cellpadding="2" align="center">
                <tbody><tr valign="top"> 
                  <td width="4%"> 
                    <div align="center"></div>                  </td>
                  <td width="81%"><font face="Kruti Dev 010">राज्य सम्पत्ति 
          विभाग के वाहनों के आवंटन, नियंत्रण एवं अनुरक्षण विषयक नियम।<span lang="en-us">
          </span><a href="#">
          शा0सं0-एम0-12190/आर0एस0वी0-65 दि0-08.09.1965</a></font></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%"> 
                    <div align="center"></div>                  </td>
                  <td width="81%"><font face="Kruti Dev 010">स्टाफ कारों का 
          रख-रखाव, मरम्मती कार्य तथा पेट्रोल के उपयोग संबन्धी।<br>
                    </font><a href="#"><font face="Kruti Dev 010">
          शासनादेश सं0-एम0-6762/32-3-1992 दि0-21.09.1992</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%"> 
                    <div align="center"></div>                  </td>
                  <td width="81%"><font face="Kruti Dev 010">वाहनों के रख-रखाव 
          संबन्धी व्यय पर नियंत्रण विषयक कार्यादेश।<br>
                    </font><a href="#"><font face="Kruti Dev 010">
          सं0-एम-9208/32-3-98 दि0-06.10.1998</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%" height="11"> 
                    <div align="center"></div>                  </td>
                  <td width="81%" height="11"><font face="Kruti Dev 010">मा0 
          मंत्री/राज्य मंत्री से सम्बद्ध वाहनों के स्थानीय पेट्रोल की 
          सीमा का निर्धारण विषयक। </font><a href="#">
          <font face="Kruti Dev 010">शा0सं0-एम-390/32-3-99-85(38)/90 
          दि0-11.03.1999</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%"> 
                    <div align="center"></div>                  </td>
                  <td width="81%"><font face="Kruti Dev 010">वाहनों के पार्किंग 
          के सम्बन्द्ध में नियम। <br>
                    </font><a href="#"><font face="Kruti Dev 010">
          शा0सं0- एम0-10696/32-3-2000-48/92 टी.सी. दि0-16.03.2000</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%" height="2"> 
                    <div align="center"></div>                  </td>
                  <td width="81%" height="2"><font face="Kruti Dev 010">विधान 
          भवन परिसर में रात्रि के समय खड़े होने वाले वाहनों की सुरक्षा 
          व्यवस्था के सम्बन्ध मे। </font><a href="#">
          <font face="Kruti Dev 010">
          शा0सं0-2521/बीस-ई-4-2001-1(वि0)/97 दि0-15.06.2001</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%"> 
                    <div align="center"></div>                  </td>
                  <td width="81%"><font face="Kruti Dev 010">ग्रीष्मकालीन समय 
          में वाहनों की छत पर मैट लगाने के सिद्धान्त।<br>
                    </font><a href="#"><font face="Kruti Dev 010">
          सं0-एम0-3270/32-3-2001 दि0-03.06.2002</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%"> 
                    <div align="center"></div>                  </td>
                  <td width="81%"><font face="Kruti Dev 010">लखनऊ मुख्यालय से 
          बाहर वाहन ले जाने के सम्बन्ध में दिशा निर्देश।<br>
                    </font><a href="#"><font face="Kruti Dev 010">
          सं0-एम-5499/32-3-2005 दि0- 27.07.2005</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%"> 
                    <div align="center"></div>                  </td>
                  <td width="81%"><font face="Kruti Dev 010">मंत्रिमंडल के मा0 
          सदस्यों के उपयोगार्थ स्टाफकार हेतु निर्देश।<br>
                    </font><a href="#">
          <font face="Kruti Dev 010">सं0-एम-6799/32-3-2002-67/2002 
          दि0-14.11.2002</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%" height="42"> 
                    <div align="center"></div>                  </td>
                  <td width="81%" height="42"><font face="Kruti Dev 010">वाहन 
          चालकों के हेतु निर्देश(सुरक्षा के दृष्टि से वाहनों की डिग्गी, 
          दरवाजे एवं बोनट सदैव लाक रखे जाये तथा चालक वाहन को हमेशा अपनी 
          लिगरानी में रखें) </font>
          <a href="#">
          <font face="Kruti Dev 010">सं0-एम-2107/32-3-2006 
          दि0-10.03.2006</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%" height="27"> 
                    <div align="center"></div>                  </td>
                  <td width="81%" height="27"><font face="Kruti Dev 010">वाहनों 
          पर लाल/नीली बत्ती, हूटर का प्रयोग रजिस्ट्रेशन न0 प्लेट हेतु 
          निर्णय </font><a href="#">
          <font face="Kruti Dev 010">सं0-एम-8433/32-3-2006-43/2006 
          दि0-10.03.2006</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%" height="27"> 
                    <div align="center"></div>                  </td>
                  <td width="81%" height="27"><font face="Kruti Dev 010">
          <span lang="en-us">सचिवालय परिसर में चालकों द्वारा वाहनों को 
          पार्किग के संबंध में निर्देश&nbsp; </span><a href="#"> &nbsp;</a></font><a href="#"><font face="Kruti Dev 010">सं0-एम-5244/32-3-2007 
          दि0-31.05.2007</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%" height="27"> 
                    <div align="center"></div>                  </td>
                  <td width="81%" height="27"><font face="Kruti Dev 010">
          <span lang="en-us">स्टाफ कारों के उपयोग के संबंध में संशोधित 
          नियमावली&nbsp; </span></font>
          <a href="#">
          <font face="Kruti Dev 010">सं0-एम-6582/32-3-2007-90 
          (21)/91टी0सी0 दि0-04.07.2007</font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="4%" height="42"> 
                    <div align="center"></div>                  </td>
                  <td width="81%" height="42"><font face="Kruti Dev 010">
          <span lang="en-us">मा0 मंत्री/राज्य मंत्री/उपमंत्री स्तर 
          प्राप्त महानुभावों अथवा राज्य सरकार के निगम/आयोग/परिषद/स्वायत्तशासी 
          संस्था में नामित गैर सरकारी अध्यक्ष/उपाध्यक्ष/सलाहकार हेतु 
          वाहन की व्यवस्था </span><a href="#"> a</a></font><a href="#"><font face="Kruti Dev 010">सं0-एम-9017/32-3-2007-90(21)91टी0सी0-1 
          दि0-07.11.2007</font></a></td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>