<!-- <!DOCTYPE html> -->
<?php include 'header.php';?>
<html lang="en">
      <!--/.nav-wrapper-->
      <section class="wrapper banner-wrapper">
         <div id="flexSlider" class="flexslider">
            <div class="inner-banner"><img src="theme/images/banner/inner-banner.jpg" alt="Inner Banner of Consumer Affairs"></div>
         </div>
      </section>
      <div class="wrapper" id="skipCont"></div>
      <!--/#skipCont-->
      <section id="fontSize" class="wrapper body-wrapper ">
      <div class="bg-wrapper inner-wrapper">
      <div class="breadcam-bg breadcam">
         <div class="container common-container four_content">
            <ul>
               <li><a href="home.html">Home </a></li>
               <li><a href="#">About</a></li>
               <li><a href="#">About Us</a></li>
            </ul>
         </div>
      </div>
      <section id="page-head" class="wrapper headings-wrapper">
         <div class="container common-container four_content">
      
            <h2><em>राज्य सम्पत्ति विभाग, उ0प्र0 शासन के प्रमुख कार्यकलाप</em></h2>
          
            <hr />
         </div>
      </section>
      <!--/#page-head-->
      <section id="paragraph" class="wrapper paragraph-wrapper">
         <div class="container common-container four_content">
               <p>राज्य सम्पत्ति विभाग द्वारा राज्य मुख्यालय पर मा0 मंत्रीगण, मा0 विधायकगण, लखनऊ में कार्यरत सरकारी अधिकारियों/कर्मचारियों की आवासीय व्यवस्था तथा लखनऊ में सरकारी कार्यालय भवन यथा विधान भवन, लाल बहादुर शास्त्री भवन (एनेक्सी), बापू भवन, योजना भवन तथा जवाहर भवन एवं इंदिरा भवन के रख-रखाव की व्यवस्था सुनिश्चित की जाती है। राज्य से दिल्ली, कोलकाता एवं मुम्बई जाने वाले मा0 मंत्रीगण, मा0 विधायकगण, अधिकारियों एवं अन्य महानुभावों के ठहरने हेतु अतिथि गृह अवस्थित हैं। इसी प्रकार मा0 संासदों तथा अधिकारियों एवं अन्य महानुभावों के लिए लखनऊ में अतिथि गृह अवस्थित हैं। इन अतिथि गृहों की प्रबन्धकीय तथा रख-रखाव की व्यवस्था भी विभाग द्वारा सम्पादित की जाती है। राज्य सरकार के मा0 मंत्रीगण हेतु वाहनों की व्यवस्था भी की जाती है। राज्य अतिथियों एवं भारत सरकार के मा0 मंत्रीगण के प्रदेश (लखनऊ) आगमन पर उनकी सुविधानुसार आवास एवं भोजन की व्यवस्था भी सुनिश्चित की जाती है।
                  </p>
             
            <hr />
      
      </section>
      <!--/#paragraph-->
      <section id="list" class="wrapper list-wrapper">
         <div class="container common-container four_content">
            <h3>अतिथिगृहों, विधायक निवासों, आवासीय काॅंलोनियों एवं अनावासीय भवनों की संख्याः-</h3>
            <div class="list list-circle">
                <div class="view-footer"><br><br>
                    <table>
                       <tr><td>1</td><td>अतिथि गृहों की संख्या</td><td>11</td></tr>
                       <tr><td>2</td><td>विधायक निवासों की संख्या</td><td>10</td></tr>
                       <tr><td>3</td><td>आवासीय काॅंलोनियों की संख्या
                        
                         </td><td>38</td></tr>
                       <tr>
                          <td>4</td>
                          <td>  अनावासीय कार्यालय भवनों की संख्या</td><td>11</td></tr>
                          <tr>
                             <td>5</td>
                             <td> सरकारी वाहनों की संख्या</td><td>246</td></tr>
                          </table>
                 </div>
            </div>
            <hr />
            <hr />
         </div>
      </section>
      <section class="wrapper carousel-wrapper">
         <div class="container common-container four_content carousel-container">
            <div id="flexCarousel" class="flexslider carousel">
               <ul class="slides">
                  <li><a target="_blank" href="http://digitalindia.gov.in/" title="Digital India, External Link that opens in a new window"><img src="assets/images/carousel/digital-india.png" alt="Digital India"></a></li>
                  <li><a target="_blank" href="http://www.makeinindia.com/" title="Make In India, External Link that opens in a new window"> <img src="assets/images/carousel/makeinindia.png" alt="Make In India"></a></li>
                  <li><a target="_blank" href="http://india.gov.in/" title="National Portal of India, External Link that opens in a new window"><img src="assets/images/carousel/india-gov.png" alt="National Portal of India"></a></li>
                  <li><a target="_blank" href="http://goidirectory.nic.in/" title="GOI Web Directory, External Link that opens in a new window"><img src="assets/images/carousel/goidirectory.png" alt="GOI Web Directory"></a></li>
                  <li><a target="_blank" href="https://data.gov.in/" title="Data portal, External Link that opens in a new window" ><img src="assets/images/carousel/data-gov.png" alt="Data portal"></a></li>
                  <li><a target="_blank" href="https://mygov.in/" title="MyGov, External Link that opens in a new window"><img src="assets/images/carousel/mygov.png" alt="MyGov Portal"></a></li>
               </ul>
            </div>
         </div>
      </section>
      <!--/.carousel-wrapper-->
      <footer class="wrapper footer-wrapper">
        <div class="footer-top-wrapper">
           <div class="container common-container four_content footer-top-container">
              <ul>
                 <li><a href="inner.html">Feedback</a></li>
                 <li><a href="inner.html">Website policies</a></li>
                 <li><a href="inner.html">Terms and Conditions </a></li>
                 <li><a href="inner.html">Contact Us</a></li>
                 <li><a href="inner.html">Help</a></li>
                 <li><a href="inner.html">Web Information Manager</a></li>
                 <li><a href="inner.html">Visitor Pass</a></li>
                 <li><a href="inner.html">FAQ</a></li>
                 <li><a href="inner.html">Disclaimer</a></li>
              </ul>
           </div>
        </div>
        <div class="footer-bottom-wrapper">
            <div class="container common-container four_content footer-bottom-container">
               <div class="footer-content clearfix">
                <div class="copyright-content"> 
                    <strong> इस वेब साइट का कंटेंट राज्य संपत्ति विभाग उत्तर प्रदेश कृषि भवन, लखनऊ द्वारा प्रकाशित एवं संचालित किया जाता है।'</strong>
                    <span>इस वेबसाइट के बारे में किसी भी प्रश्न के लिए, कृपया "वेब सूचना प्रबंधक" से संपर्क करें।
                    <!-- <a target="_blank" title="NIC, External Link that opens in a new window" href="http://www.nic.in/"> -->
                    <strong>कापीराइट © राज्य संपत्ति विभाग , उत्तर प्रदेश को सभी अधिकार सुरक्षित हैं।</strong></span> 
                 </div>
                  <!-- <div class="logo-cmf"> <a target="_blank" href="http://cmf.gov.in/" title="External link that opens in new tab, cmf"><img alt="cmf logo" src="assets/images/cmf-logo.png"></a> </div> -->
               </div>
            </div>
         </div>
     </footer>
     <!--/.footer-wrapper-->
     <!-- jQuery v1.11.1 -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js" integrity="sha512-nhY06wKras39lb9lRO76J4397CH1XpRSLfLJSftTeo3+q2vP7PaebILH9TqH+GRpnOhfAGjuYMVmVTOZJ+682w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
     <!-- jQuery Migration v1.4.1 -->
     <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
     <!-- jQuery v3.6.0 -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
     <!-- jQuery Migration v3.4.0 -->
     <script src="https://code.jquery.com/jquery-migrate-3.4.0.min.js"></script>
     
     <script src="assets/js/jquery-accessibleMegaMenu.js"></script>
     <script src="assets/js/framework.js"></script>
     <script src="assets/js/jquery.flexslider.js"></script>
     <script src="assets/js/font-size.js"></script>
     <script src="assets/js/swithcer.js"></script>
     <script src="theme/js/ma5gallery.js"></script>
     <script src="assets/js/megamenu.js"></script>
     <script src="theme/js/easyResponsiveTabs.js"></script>
     <script src="theme/js/custom.js"></script>
  </body>
</html>