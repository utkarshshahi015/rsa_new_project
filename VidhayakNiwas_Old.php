<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">विधायक निवास - 2 पुराना</h2>
               <table width="100%" align="center" border="1" bordercolor="#000000" width="98%">
              <tbody>
                <tr>
                  <td width="167" align="center"><b>नाम </b></td>
                  <td width="195" align="center"><b>पदनाम </b></td>
                  <td width="106" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="158" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
                <tr>
                  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:167px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top"><p class="MsoNormal1">
          श्री फिरोज़ कुमार</p></td>
                  <td style="width:196px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          <p class="MsoNormal1">कार्यवाहक व्यवस्थाधिकारी</p></td>
                  <td style="width:107px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top"><p align="center"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">2274397
                          <o:p></o:p>
                  </span></p></td>
                  <td style="width:159px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          <p align="center">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          9415582392</span><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">
                          <o:p></o:p>
                  </span></p></td>
                </tr>
                <tr>
                  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:167px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top"><p class="MsoNormal1">
          श्री मनोज कुमार तिवारी </p></td>
                  <td style="width:196px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          <p class="MsoNormal1">स्टोर कीपर </p></td>
                  <td style="width:107px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top" align="center"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">2274397
                          <o:p></o:p>
                  </span></td>
                  <td style="width:159px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          <p align="center">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          9454421089</span></p></td>
                </tr>
                <tr>
                  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:167px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          <p class="MsoNormal1">श्री कैसर अब्बास </p></td>
                  <td style="width:196px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          <p class="MsoNormal1">कनिष्ठ सहायक</p></td>
                  <td style="width:107px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top" align="center"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">2274397
                          <o:p></o:p>
                  </span></td>
                  <td style="width:159px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          <p class="MsoNormal1" align="center"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">
                    <o:p>&nbsp;9454421091</o:p>
                          <o:p></o:p>
                    </span></p></td>
                </tr>
                <tr>
                  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:167px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          श्रीमती बैजन्ती पाण्डेय </td>
                  <td style="width:196px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          मुख्यस्वागति </td>
                  <td style="width:107px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top" align="center"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">2274397
                          <o:p></o:p>
                  </span></td>
                  <td style="width:159px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          <p class="MsoNormal1" align="center"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">
                    <o:p>&nbsp;9454421092</o:p>
                          <o:p></o:p>
                    </span></p></td>
                </tr>
                <tr>
                  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:167px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          श्री रामेश्वर प्रसाद </td>
                  <td style="width:196px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          कंप्यूटर ऑपरेटर </td>
                  <td style="width:107px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top" align="center"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">2274397
                          <o:p></o:p>
                  </span></td>
                  <td style="width:159px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top" align="center">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          9454421182</span></td>
                </tr>
                <tr>
                  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:167px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          श्री अनिल कुमार </td>
                  <td style="width:196px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          सहायक स्टोर कीपर </td>
                  <td style="width:107px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top" align="center"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">2274397
                          <o:p></o:p>
                  </span></td>
                  <td style="width:159px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top" align="center">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          9454421106</span></td>
                </tr>
                <tr>
                  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:167px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          श्री आशीष कुमार पंकज&nbsp; </td>
                  <td style="width:196px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          टेलीफोन ऑपरेटर</td>
                  <td style="width:107px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top" align="center"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">2274397
                          <o:p></o:p>
                  </span></td>
                  <td style="width:159px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top" align="center">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          9454421094</span></td>
                </tr>
                <tr>
                  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:167px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          श्री चन्द्रशेखर </td>
                  <td style="width:196px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top">
          कनिष्ठ सहायक</td>
                  <td style="width:107px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top" align="center"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">2274397
                          <o:p></o:p>
                  </span></td>
                  <td style="width:159px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" nowrap="nowrap" valign="top" align="center">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          7376640462</span></td>
                </tr>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>