<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">विधायक निवास - 1 (ब) दारुलशफा</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000">
        <tbody><tr>
          <td width="25%" align="center"><b>नाम </b>
          </td>
          <td width="138" align="center"><b>पदनाम 
          </b></td>
          <td width="132" align="center"><b>कार्यालय नंबर&nbsp;
          </b></td>
          <td width="146" align="center"><b>मोबाइल नंबर</b></td>
        </tr>
        <tr>
          <td nowrap="" valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:192;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          <p class="MsoNormal">श्री रविकांत चौधरी </p></td>
          <td nowrap="" valign="top" style="width:139px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          व्यवस्थाधिकारी </td>
          <td nowrap="" valign="top" style="width:133px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
          <p class="MsoNormal">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          0522&amp;2201066</span></p></td>
          <td nowrap="" valign="top" style="width:147px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">9415002112<br>
&nbsp;</td>
        </tr>
        <tr>
          <td nowrap="" valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:192;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          <p class="MsoNormal">श्री कृपा शंकर चौबे </p></td>
          <td nowrap="" valign="top" style="width:139px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          <p class="MsoNormal">वरिष्ठ स्टोर कीपर </p></td>
          <td nowrap="" valign="top" style="width:133px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
          <p class="MsoNormal">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          0522&amp;2201066</span></p>
          <p class="MsoNormal">&nbsp;</p></td>
          <td nowrap="" valign="top" style="width:147px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
          9454421078<br>
          9415913267</td>
        </tr>
        <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री विकास 
  यादव </font></span></span>
  </td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">सहायक स्टोर 
  कीपर </font></span></span>
  </td>
  <td style="width:98px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
          <p class="MsoNormal" align="right">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          0522&amp;2201066</span></p>
          </td>
  <td style="width:80px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  &nbsp;</td>
              </tr>
        <tr>
          <td nowrap="" valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:192;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          श्री शैलेन्द्र सिंह</td>
          <td nowrap="" valign="top" style="width:139px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">&nbsp;वरिष्ठ 
          सहायक </td>
          <td nowrap="" valign="top" style="width:133px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
          <p class="MsoNormal">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          0522&amp;2201066</span></p>
          <p class="MsoNormal">&nbsp;</p></td>
          <td nowrap="" valign="top" style="width:147px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
          9454421079<br>
          9532967923</td>
        </tr>
        <tr>
          <td nowrap="" valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:192;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          श्री रविन्द्र कुमार </td>
          <td nowrap="" valign="top" style="width:139px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">&nbsp;वरिष्ठ 
          सहायक </td>
          <td nowrap="" valign="top" style="width:133px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
          <p class="MsoNormal">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          0522&amp;2201066</span></p>
          <p class="MsoNormal">&nbsp;</p></td>
          <td nowrap="" valign="top" style="width:147px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
          9454421077<br>
          9452146588</td>
        </tr>
        <tr>
          <td nowrap="" valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:192;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          श्री कमलेन्द्र सिंह </td>
          <td nowrap="" valign="top" style="width:139px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          कनिष्ठ लिपिक </td>
          <td nowrap="" valign="top" style="width:133px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
          <p class="MsoNormal">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          0522&amp;2201066</span></p></td>
          <td nowrap="" valign="top" style="width:147px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
          9454421080<br>
          9919502585</td>
        </tr>
        <tr>
          <td nowrap="" valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:192;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          श्रीमती रूचि </td>
          <td nowrap="" valign="top" style="width:139px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">पूँछ ताछ लिपिक</td>
          <td nowrap="" valign="top" style="width:133px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
          <p class="MsoNormal">
          <span style="font-family: Kruti Dev 010; font-size: 14pt">
          0522&amp;2201066</span></p>
          <p class="MsoNormal">&nbsp;</p></td>
          <td nowrap="" valign="top" style="width:147px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
          9454421081<br>
  8953823111</td>
        </tr>
      </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>