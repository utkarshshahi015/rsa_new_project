<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">योजना कोष्ठक राज्य संपत्ति विभाग</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="178" align="center"><b>पदनाम </b></td>
                  <td width="110" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="102" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
      <td valign="top" style="border-right:.5pt solid windowtext; border-top:.5pt solid windowtext; border-bottom:.5pt solid windowtext; width:202;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in"> 
    श्री संजय दुबे प्रभारी</td>
      <td valign="top" style="border-right:.5pt solid windowtext; border-top:.5pt solid windowtext; border-bottom:.5pt solid windowtext; width:167px;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in"> 
      मुख्य&nbsp;व्यवस्था अधिकारी</td>
      <td valign="top" style="border-right:.5pt solid windowtext; border-top:.5pt solid windowtext; border-bottom:.5pt solid windowtext; width:99px;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" align="center"> 
      <p align="center">
                    <font face="Times New Roman" style="font-size: 13pt">2213441</font></p></td>
      <td valign="top" style="border-right:.5pt solid windowtext; border-top:.5pt solid windowtext; border-bottom:.5pt solid windowtext; width:91px;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" align="center"> 
      <span class="style2"><font color="#000000" style="font-size: 12pt">
      9415091361</font></span></td>
            </tr>
            <tr>
          <td>&nbsp;श्री देवेंद्र 
          प्रसाद पाण्डेय </td>
          <td>कनिष्ठ सहायक </td>
          <td align="center">
                    <font face="Times New Roman" style="font-size: 13pt">2213441</font></td>
          <td width="114" align="center">
                    9415165116</td>
        </tr>
            <tr>
          <td>&nbsp;श्री प्रदीप तिवारी </td>
          <td>लेखाकार </td>
          <td align="center">
                    <font face="Times New Roman" style="font-size: 13pt">2213441</font></td>
          <td width="114" align="center">
                    9454421061</td>
        </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>