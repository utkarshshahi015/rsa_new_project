<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <meta name="format-detection" content="telephone=no" />
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="apple-touch-icon" href="assets/images/favicon/apple-touch-icon.png">
      <link rel="icon" href="assets/images/favicon/favicon.png">
      <title>राज्य संपत्ति विभाग : उत्तर प्रदेश</title>
      <!-- Custom styles for this template -->
      <link href="assets/css/base.css" rel="stylesheet" media="all">
      <link href="assets/css/base-responsive.css" rel="stylesheet" media="all">
      <link href="assets/css/grid.css" rel="stylesheet" media="all">
      <link href="assets/css/font.css" rel="stylesheet" media="all">
      <link href="assets/css/font-awesome.min.css" rel="stylesheet" media="all">
      <link href="assets/css/flexslider.css" rel="stylesheet" media="all">
      <link href="assets/css/print.css" rel="stylesheet" media="print" />
      <link href="assets/css/modules.css" rel="stylesheet" media="all">
      <!-- Theme styles for this template -->
      <link href="assets/css/megamenu.css" rel="stylesheet" media="all" />
      <link href="theme/css/site.css" rel="stylesheet" media="all">
      <link href="theme/css/site-responsive.css" rel="stylesheet" media="all">
      <link href="theme/css/ma5gallery.css" rel="stylesheet" type="text/css">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
      <!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
      <![endif]-->
   </head>
         <body> 
         <header>
            <h1 style="display: none;">Header</h1>
            <div class="region region-header-top">
               <div id="block-cmf-content-header-region-block" class="block block-cmf-content first last odd">
                  <noscript class="no_scr">"JzavaScript is a standard programming language that is included to provide interactive features, Kindly enable Javascript in your browser. For details visit help page"
                  </noscript>
                  <div class="wrapper common-wrapper">
                     <div class="container common-container four_content top-header">
                        <div class="common-left clearfix">
                           <ul>
                              <li class="gov-india"><span class="responsive_go_hindi" lang="hi"><a target="_blank" href="https://india.gov.in/hi" title="भारत सरकार ( बाहरी वेबसाइट जो एक नई विंडो में खुलती है)" role="link">भारत सरकार</a></span></li>
                              <li class="ministry"><span class="li_eng responsive_go_eng"><a target="_blank" href="https://india.gov.in/" title="Government of india,External Link that opens in a new window" role="link">Government of india</a></span></li>
                           </ul>
                        </div>
                        <div class="common-right clearfix">
                           <ul id="header-nav">
                              <li class="ico-skip cf"><a href="#skipCont" title="">Skip to main content</a>
                              </li>
                              <li class="ico-site-search cf">
                                 <a href="javascript:void(0);" id="toggleSearch" title="Site Search">
                                 <img class="top" src="assets/images/ico-site-search.png" alt="Site Search" /></a>
                                 <div class="search-drop both-search">
                                    <div class="google-find">
                                       <form method="get" action="http://www.google.com/search" target="_blank">
                                          <label for="search_key_g" class="notdisplay">Search</label>
                                          <input type="text" name="q" value="" id="search_key_g"/> 
                                          <input type="submit" value="Search" class="submit" /> 
                                          <div >
                                             <input type="radio" name="sitesearch" value="" id="the_web"/> 
                                             <label for="the_web">The Web</label> 
                                             <input type="radio" name="sitesearch" value="india.gov.in" checked id="the_domain"/> <label for="the_domain"> INDIA.GOV.IN</label>
                                          </div>
                                       </form>
                                    </div>
                                    <div class="find">
                                       <form name="searchForm" action="">
                                          <label for="search_key" class="notdisplay">Search</label>
                                          <input type="text" name="search_key" id="search_key" onKeyUp="autoComplete()" autocomplete="off" required />
                                          <input type="submit" value="Search" class="bttn-search"/>
                                       </form>
                                       <div id="auto_suggesion"></div>
                                    </div>
                                 </div>
                              </li>
                              <li class="ico-accessibility cf">
                                 <a href="javascript:void(0);" id="toggleAccessibility" title="Accessibility Dropdown" role="link">
                                 <img class="top" src="assets/images/ico-accessibility.png" alt="Accessibility Dropdown" />
                                 </a>
                                 <ul style="visibility: hidden;">
                                    <li> <a onClick="set_font_size(&#39;increase&#39;)" title="Increase font size" href="javascript:void(0);" role="link">A<sup>+</sup>
                                       </a> 
                                    </li>
                                    <li> <a onClick="set_font_size()" title="Reset font size" href="javascript:void(0);" role="link">A<sup>&nbsp;</sup></a> </li>
                                    <li> <a onClick="set_font_size(&#39;decrease&#39;)" title="Decrease font size" href="javascript:void(0);" role="link">A<sup>-</sup></a> </li>
                                    <li> <a href="javascript:void(0);" class="high-contrast dark" title="High Contrast" role="link">A</a> </li>
                                    <li> <a href="javascript:void(0);" class="high-contrast light" title="Normal Contrast" style="display: none;" role="link">A</a> </li>
                                 </ul>
                              </li>
                              <li class="ico-social cf">
                                 <a href="javascript:void(0);" id="toggleSocial" title="Social Medias">
                                 <img class="top" src="assets/images/ico-social.png" alt="Social Medias" /></a>
                                 <ul>
                                    <li>
                                       <a target="_blank" title="External Link that opens in a new window" href="http://www.facebook.com/">
                                       <img alt="Facebook Page" src="assets/images/ico-facebook.png">
                                       </a>
                                    </li>
                                    <li>
                                       <a target="_blank" title="External Link that opens in a new window" href="http://www.twitter.com/">
                                       <img alt="Twitter Page" src="assets/images/ico-twitter.png">
                                       </a>
                                    </li>
                                    <li>
                                       <a target="_blank" title="External Link that opens in a new window" href="http://www.youtube.com/">
                                       <img alt="youtube Page" src="assets/images/ico-youtube.png">
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li class="ico-sitemap cf"><a href="" title="Sitemap">
                                 <img class="top" src="assets/images/ico-sitemap.png" alt="Sitemap" /></a>
                              </li>
                              <li class="hindi cmf_lan d-hide">
                                 <label class="de-lag">
                                    <span>Language</span>
                                    <select title="Select Language">
                                       <option>English</option>
                                       <option>हिन्दी</option>
                                    </select>
                                 </label>
                              </li>
                              <li class="hindi cmf_lan m-hide">
                                 <a href="javascript:;" title="Select Language">Language</a> 
                                 <ul>
                                    <li><a target="_blank" href="" lang="hi" class="alink" title="Click here for हिन्दी version.">हिन्दी</a></li>
                                 </ul>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <p id="scroll" style="display: none;"><span></span></p>
            </div>
            <!--Top-Header Section end-->
            <section class="wrapper header-wrapper">
                <div class="container common-container four_content  header-container">
                   <h2 class="logo">
                      <a href="home.html" title="Home" rel="home" class="header__logo" id="logo">
                         <img class="national_emblem" src="assets/images/up-logo-hi.png" alt="national emblem" >
                         <p><span>राज्य संपत्ति विभाग  </span>                       
                            <span> उत्तर प्रदेश</span>
                         </p>
                      </a>
                   </h2>
                   <!-- <div class="header-right clearfix">
                      <div class="right-content clearfix">
                         <div class="float-element" style="margin-left:60px;">
                      <button type="button" class="btn btn-primary"><a href="https://estate-up.gov.in/rsav2/applicant" style="color: #fff;">आवेदक लॉगिन</a> </button>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <button type="button" class="btn btn-primary"><a href="https://estate-up.gov.in/rsav2/applicant-register" style="color: #fff;">सरकारी आवास हेतु आवेदन</a></button>
                         </div>
                      </div>
                   </div> -->
                   &nbsp;&nbsp;&nbsp;&nbsp;
                   <div class="header-right clearfix">
                      <div class="right-content clearfix">
                          <div class="float-element">
                              <a class="sw-logo1" target="_blank" href="https://www.skillindia.gov.in/" title="Skill India, External link that open in a new windows">
                                  <img src="assets/images/g20.png" alt="Skill India">
                              </a>
                              <a class="sw-logo" target="_blank" href="https://swachhbharat.mygov.in/" title="Swachh Bharat, External link that open in a new windows">
                                  <img src="assets/images/swach-bharat.png" alt="Swachh Bharat">
                              </a>
                              <a class="sw-logo" target="_blank" href="https://swachhbharat.mygov.in/" title="Swachh Bharat, External link that open in a new windows">
                                  <img src="assets/images/emblem-dark.png" alt="Swachh Bharat">
                              </a>
                             
                          </div> 
                      </div>
                   </div>
              <!-- <div class="header-right clearfix">
                      <div class="right-content clearfix">
                         <div class="float-element" style="margin-left:60px;">
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <button type="button" class="btn btn-primary"><a href="https://estate-up.gov.in/rsav2/applicant-register" style="color: #fff;">सरकारी आवास हेतु आवेदन</a></button>
                         </div>
                      </div>
                   </div> -->
                 
                </div>
             </section>
             <!--/.header-wrapper-->
             <section class="wrapper megamenu-wraper">
            <div class="container common-container four_content">
               <p class="showhide"><em></em><em></em><em></em></p>
               <nav class="main-menu clearfix" id="main_menu" style="">
                  <ul class="nav-menu">
                     <li class="nav-item"> <a href="#" class="home"><i class="fa fa-home"></i></a> </li>
                     <li class="nav-item"> <a href="About.html">हमारे बारे में</a></li>

                     <li class="nav-item"><a href="https://estate-up.gov.in/RajayaSampatti/">   निदेशालय का विवरण</a></li>


                     <li class="nav-item"> <a href="http://estate-up.gov.in/RajayaSampatti/">विभाग के कार्यकलाप</a></li>
            
                    
                     <li class="nav-item"><a href="https://estate-up.gov.in/rsav2/house-allotment-links"> आवास का आवंटन</a>
                        <!-- <div class="sub-nav">
                        <ul class="sub-nav-group">
                           <li><a href="inner.html">Right to Information Act</a></li>
                           <li><a href="inner.html">Other Act And Rules</a></li>
                        </ul>
                        </div> -->
                     </li>
                             
                     <!-- <li class="nav-item"><a href="https://estate-up.gov.in/rsav2/house-allotment-links">Allotment of Accommodation </a></li> -->
            
                  
                     <li class="nav-item"> <a href="http://estate-up.gov.in/RajayaSampatti/">संगठन संरचना</a></li>

                     <li class="nav-item"> <a href="rti.html">आरटीआई</a></li>

                     <li class="nav-item"><a href="https://estate-up.gov.in/rsav2/contact-us">   संपर्क सूत्र</a></li>
                        <li class="nav-item"><a> लॉगिन </a>               
                        <div class="sub-nav">
                        <ul class="sub-nav-group">
                           <li><a href="aanlogin.html">आन-लॉगिन</a></li></label> 
                           <li><a href="https://estate-up.gov.in/rsav2/applicant">आवेदक-लॉगिन</a></li></label>  
                          <li><a href="https://estate-up.gov.in/nideshaalay/">निदेशालय-लॉगिन</a></li></label>  
                           <li><a href="https://estate-up.gov.in/rsav2/superadmin"> विभाग-लॉगिन</a></li>
                           <li><a href="https://estate-up.gov.in/rsav2/engineer">   इंजीनियर-लॉगिन</a></li>
                        </ul>
                        </div>
                     </li> 
                     
                     <li class="nav-item"> <a href="inner.html">गैलरी </a>
                        <div class="sub-nav">
                        <ul class="sub-nav-group">
                           <li><a href="photo.html">फोटो गैलरी</a></li>
                           <li><a href="inner.html">वीडियो गैलरी</a></li>
                        </ul>
                        </div>
                     </li>

                     <li class="nav-item"> <a href="inner.html">अतिथि गृह का विवरण </a>
                        <div class="sub-nav">
                        <ul class="sub-nav-group">
                           <li><a href="assets/doc/guest house name.pdf"> गेस्टहाउस सूची</a></li>
                           <li><a href="assets/doc/rent.pdf">श्रेणी-१ के पात्रता सूची के सन्दर्भ मे</a></li>
                           <li><a href="assets/doc/rent new.pdf">शर्ते/किराया शासनादेश संख्या</a></li>  
                        </ul>
                        </div>
                     </li>


                     <li class="nav-item"><a href="https://ehrms.upsdc.gov.in/">  मानव सम्पदा पोर्टल</a></li>
                  </ul>
                  </nav>
               <nav class="main-menu clearfix" id="overflow_menu">
                  <ul class="nav-menu clearfix">
                  </ul>
               </nav>
            </div>
            <style type="text/css">
               body ~ .sub-nav {
               right: 0;
               }
            </style>
         </section>
         </header>

         <section class="wrapper banner-wrapper">
         <div id="flexSlider" class="flexslider">
            <div class="inner-banner"><img src="theme/images/banner/inner-banner.jpg" alt="Inner Banner of Consumer Affairs"></div>
         </div>
      </section>
         
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js" integrity="sha512-nhY06wKras39lb9lRO76J4397CH1XpRSLfLJSftTeo3+q2vP7PaebILH9TqH+GRpnOhfAGjuYMVmVTOZJ+682w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <!-- jQuery Migration v1.4.1 -->
      <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
      <!-- jQuery v3.6.0 -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <!-- jQuery Migration v3.4.0 -->
      <script src="https://code.jquery.com/jquery-migrate-3.4.0.min.js"></script>
      
      <script src="assets/js/jquery-accessibleMegaMenu.js"></script>
      <script src="assets/js/framework.js"></script>
      <script src="assets/js/jquery.flexslider.js"></script>
      <script src="assets/js/font-size.js"></script>
      <script src="assets/js/swithcer.js"></script>
      <script src="theme/js/ma5gallery.js"></script>
      <script src="assets/js/megamenu.js"></script>
      <script src="theme/js/easyResponsiveTabs.js"></script>
      <script src="theme/js/custom.js"></script>
                </body>
</html>