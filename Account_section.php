<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         <font face="Verdana, Arial, Helvetica, sans-serif">
            <a href="<?php echo url(); ?>" class="style2">
          <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">लेखा अनुभाग</h2>
               <table width="98%" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="32%" align="center"><b>पदनाम </b></td>
                  <td width="17%" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="16%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
              <td>श्री राजेंद्र कुमार </td>
                  <td>अनुसचिव (लेखा)</td>
                  <td align="center">2213442</td>
                  <td align="center">9454411888</td>
                </tr>
            <tr>
              <td>श्री राम खिलावन शर्मा </td>
                  <td>समीक्षा अधिकारी 
          (लेखा) </td>
                  <td align="center">2213442</td>
                  <td align="center">9454413790</td>
                </tr>
            <tr>
              <td>श्री सुरेन्द्र कुमार प्रधान </td>
                  <td>समीक्षा अधिकारी</td>
                  <td align="center">2213442</td>
                  <td align="center">9454419389</td>
                </tr>
            <tr>
              <td>श्री कमर आलम</td>
                  <td>समीक्षा अधिकारी</td>
                  <td align="center">2213442</td>
                  <td align="center">9450660821</td>
                </tr>
            <tr>
              <td>श्री सुजीत कुमार</td>
                  <td>समीक्षा अधिकारी 
          (लेखा) </td>
                  <td align="center">2213442</td>
                  <td align="center">9454419394</td>
                </tr>
            <tr>
              <td>श्री इजलाल मेहँदी </td>
                  <td>वरिष्ठ स्टोर कीपर </td>
                  <td align="center">2213442</td>
                  <td align="center">9454421031</td>
                </tr>
            <tr>
              <td>श्री मो० हसन अंसारी </td>
                  <td>टेलीफ़ोन ऑपरेटर </td>
                  <td align="center">2213442</td>
                  <td align="center">9389106592</td>
                </tr>
      <tr>
              <td>श्री राम अचल यादव </td>
                  <td>टेलीफ़ोन ऑपरेटर</td>
                  <td align="center">2213442</td>
                  <td align="center">9453842379<br>
          9454421032</td>
                </tr>
      <tr>
              <td>श्री दीप नारायण </td>
                  <td>टेलीफ़ोन ऑपरेटर</td>
                  <td align="center">2213442</td>
                  <td align="center">9454421086</td>
                </tr>
      <tr>
              <td>श्री राजेश कुमार मिश्रा</td>
                  <td>कनिष्ठ सहायक </td>
                  <td align="center">2213442</td>
                  <td align="center">9005065283</td>
                </tr>
      <tr>
              <td>श्री ललित कुमार </td>
                  <td>स्वागती </td>
                  <td align="center">2213442</td>
                  <td align="center">9454421158</td>
                </tr>
      <tr>
              <td>श्री नरेश कुमार श्रीवास्तव </td>
                  <td>कनिष्ठ सहायक </td>
                  <td align="center">2213442</td>
                  <td align="center">8957888438</td>
                </tr>
      <tr>
              <td>श्री शकील अहमद </td>
                  <td>कनिष्ठ सहायक </td>
                  <td align="center">2213442</td>
                  <td align="center">9454421149</td>
                </tr>
      <tr>
              <td>श्री राम किशोर </td>
                  <td>सहायक लेखाकार</td>
                  <td align="center">2213442</td>
                  <td align="center">9956747036<br>
          9454421034</td>
                </tr>
      <tr>
              <td>श्री ज़मीरुद्दीन</td>
                  <td>सहायक लेखाकार</td>
                  <td align="center">2213442</td>
                  <td align="center">9956215520</td>
                </tr>
      <tr>
              <td>कु० स्वाति 
        चित्रांशी </td>
                  <td>कनिष्ठ सहायक </td>
                  <td align="center">2213442</td>
                  <td align="center">9792197199</td>
                </tr>
      <tr>
              <td>श्रीमती रचना कुमारी </td>
                  <td>स्वागती </td>
                  <td align="center">2213442</td>
                  <td align="center">9936266646</td>
                </tr>
      <tr>
              <td>श्री नरेंद्र कुमार यादव </td>
                  <td>कनिष्ठ सहायक </td>
                  <td align="center">2213442</td>
                  <td align="center">9454421063</td>
                </tr>
      </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>