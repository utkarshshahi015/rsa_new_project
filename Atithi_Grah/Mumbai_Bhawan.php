<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h4 class="row justify-content-md-center"><u>मुम्बई स्थित अतिथिगृह /उ0प्र0भवन, मुम्बई</u></h4>
              </div>
              <table width="100%" border="2" cellspacing="2" cellpadding="2">
                        <tbody><tr valign="top"> 
                          <td width="27%" height="2" class="style2">
              <font color="#000000">अतिथिगृह में अवस्थान के लिए 
              अर्हता</font></td>
                          <td width="73%" height="2" class="style2"> 
                            <div align="justify"><font color="#000000">
                <span style="font-weight: 400">अधिकारी, 
                मा0सांसद, मा0 विधायक, मा0 भूतपूर्व सांसद, मा0 
                भूतपूर्व विधायक, स्थानीय निकाय के अध्यक्ष, सेवा 
                निवृत अधिकारी, मान्यता प्राप्त पत्रकार, सम्पादक, 
                दिल्ली स्थित केन्द्र सरकार के अधिकारी, अन्य 
                प्रदेशों से आने वाले अधिकारी, विधायक, विधान सभा/परिषद 
                की मा0 समितियों में मा0 सदस्य एवं सर्वजनिक 
                उपक्रमों/निगमों के अध्यक्ष/सदस्य,<span lang="en-us">
                </span>कन्द्र सरकार 
                के अधिकारी जो दिल्ली से बाहर तैनात हो तथा अन्य 
                व्यक्ति।</span></font></div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" class="style2"><font color="#000000">
              पता</font></td>
                          <td width="69%" class="style2 style8 style10"> 
              <font color="#000000" size="3">
              <span style="font-weight: 400">यू0 पी0 भवन नवी 
              मुम्बई </span></font> </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" class="style2"><font color="#000000">
              फोन नम्बर</font></td>
                          <td width="69%" class="style2"><p>
              <font color="#000000">
              <span style="font-weight: 400">022.27811861</span></font></p></td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" class="style2"><font color="#000000">
              कक्षों की संख्या</font></td>
                          <td width="69%" class="style2">
              <font color="#000000">
              <span style="font-weight: 400">15</span></font></td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" class="style2"><font color="#000000">
              अधिकारी/कर्मचारियों के नाम एवं फ़ोन नंबर </font></td>
                          <td width="69%" class="style2">
            <table width="636" border="1" align="center" bordercolor="#000000" height="82">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="193" align="center"><b>पदनाम </b></td>
                  <td width="134" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="21%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:193;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:11; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री राम सागर तिवारी</td>
        <td bgcolor="#FFFFFF" style="width:189;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:11">
        व्यवस्था अधिकारी</td>
        <td bgcolor="#FFFFFF" style="width:130;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:11">
        022-27811861</td>
        <td bgcolor="#FFFFFF" style="width:106;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:11">
        09452269851, 08108510333</td>
            </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:193;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:35; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री माता प्रसाद सिंह</td>
        <td bgcolor="#FFFFFF" style="width:189;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        वरिष्ठ सहायक</td>
        <td bgcolor="#FFFFFF" style="width:130;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        022-27811861</td>
        <td bgcolor="#FFFFFF" style="width:106;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        -</td>
            </tr>
            </tbody></table>
                  </td>
                        </tr>
                        <tr valign="top"> 
                          <td height="108" colspan="2" class="style2"> 
                            <div align="justify">
                              <p><span style="font-weight: 400">
                <font face="Kruti Dev 010" color="#000000">
                <span lang="en-us">शर्ते/किराया शासनादेश संख्या 
                - </span></font><a href="../assets/doc/room_rent.pdf" target="_blank">
                <font face="Kruti Dev 010" color="#000000">
                एम-2783 32-3-2013 - 2एन0टी92<span lang="en-us">
                </span>&nbsp;दिनांक 29 मई 
                2013</font></a></span></p>
                              <p><span style="font-weight: 400">
                <font face="Kruti Dev 010">
                <font color="#000000"><span lang="en-us">भोजन की 
                दरें -प्रतिथाल शासनादेश संख्या - </span></font>
                <a href="../assets/doc/Bhojan_dar.pdf" target="_blank">
                <font color="#000000">एम-276532-3-2013-57/81 दिनांक 20 -05-2013</font></a></font></span></p>
                            </div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td height="7" rowspan="2" class="style2">
              <font color="#000000">अतिथिगृह आवंटन हेतु सम्पर्क 
              सूत्र</font></td>
                          <td width="69%" height="3" class="style2">
              <font color="#000000">राज्य सम्पत्ति अधिकारी<span style="font-weight: 400"><br>
              नवीन भवन,कक्ष सं0-22,उ0प्र0सचिवालय<br>
              फोन नम्बर-0522-2238203, फैक्स नम्बर-2228385</span></font></td>
                        </tr>
                        <tr valign="top"> 
                          <td width="69%" height="2" class="style2">
              <font color="#000000">व्यवस्थाधिकारी<span style="font-weight: 400"><br>
              फोन नम्बर-022-27811861</span></font></td>
                        </tr>
                      </tbody></table>
               
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>