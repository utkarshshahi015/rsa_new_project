<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h4 class="row justify-content-md-center"><u>विशिष्ट अतिथिगृह- डालीबाग- लखनऊ
</u></h4>
              </div>
              
               <table width="100%" border="2" cellspacing="2" cellpadding="2">
                        <tbody><tr valign="top"> 
                          <td width="27%" height="2"><b>अतिथिगृह में अवस्थान के 
              लिए अर्हता<br>
&nbsp;</b></td>
                          <td width="64%" height="2"> 
                            <div align="Justify">अधिकारी, मा0सांसद, मा0 विधायक, 
                मा0 भूतपूर्व सांसद, मा0 भूतपूर्व विधायक, स्थानीय 
                निकाय के अध्यक्ष, सेवा निवृत अधिकारी, मान्यता 
                प्राप्त पत्रकार, सम्पादक, दिल्ली स्थित केन्द्र 
                सरकार के अधिकारी, अन्य प्रदेशों से आने वाले 
                अधिकारी, विधायक, विधान सभा/परिषद की मा0 समितियों 
                में मा0 सदस्य एवं सर्वजनिक उपक्रमों/निगमों के 
                अध्यक्ष/सदस्य,<span lang="en-us">
                </span>केन्द्र सरकार के अधिकारी जो दिल्ली 
                से बाहर तैनात हो तथा अन्य व्यक्ति।</div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%"><b>पता</b></td>
                          <td width="64%">डालीबाग, लखनऊ।</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%"><b>फोन नम्बर</b></td>
                          <td width="64%">0522-2208608, 2208591, 2208620</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%"><b>कक्षों की संख्या</b></td>
                          <td width="64%">55(सभी कक्ष वातानुकूलित हैं) </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" height="24"><b>अधिकारी/कर्मचारियों के 
              नाम एवं फ़ोन नंबर </b></td>
                          <td width="64%" height="24">
      <table border="1" width="99%">
        <tbody><tr>
                  <td width="29%" align="center"><b>नाम </b></td>
                  <td width="204" align="center"><b>पदनाम </b></td>
                  <td width="108" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="20%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री राजीव कुमार </td>
                  <td width="204" align="center">व्यवस्थाधिकारी </td>
                  <td width="108" align="center"> <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2208608, 522, 516,&nbsp; 620</font></span></font><font face="Times New Roman" style="font-size: 13pt">
                    </font> <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">, 591 (fax)</font></span></font></td>
                  <td width="20%" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt">
                    9450910134</span></td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री संजीव कौल </td>
                  <td width="204" align="center">व्यवस्थापक </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421164</td>
                </tr>
        <tr>
          <td align="center">श्री चंद्रमौलि त्रिपाठी </td>
          <td align="center">मुख्यस्वागति </td>
          <td align="center">
                    &nbsp;</td>
          <td width="179" align="center">
                    9454421020</td>
        </tr>
        <tr>
                  <td width="29%" align="center">श्री दूधनाथ प्रसाद </td>
                  <td width="204" align="center">मुख्य स्वागती </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421165</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री देशराज </td>
                  <td width="204" align="center">मुख्य स्वागती </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421168</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री रेहान हैदर रिज़वी </td>
                  <td width="204" align="center">वरिष्ठ स्वागती </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421167</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री मुकेश चन्द्र श्रीवास्तव </td>
                  <td width="204" align="center">स्टोर कीपर </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421170</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री चन्द्रपाल सिंह व्यास </td>
                  <td width="204" align="center">वरिष्ठ सहायक </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421171</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री गुलाम अब्बास </td>
                  <td width="204" align="center">काउंटर क्लर्क कम स्टोर कीपर </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421172</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री लाल बहादुर </td>
                  <td width="204" align="center">कनिष्ठ सहायक </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421173</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्रीमती रंजना उपाध्याय </td>
                  <td width="204" align="center">कनिष्ठ सहायक </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421174</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री अलोक पाण्डेय</td>
                  <td width="204" align="center">सहायक स्टोर कीपर </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421175</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्रीमती किरण कुमारी </td>
                  <td width="204" align="center">सहायक स्टोर कीपर </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421099</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्रीमती सब्बी खातून </td>
                  <td width="204" align="center">सहायक स्टोर कीपर </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421178</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री नगीना सिंह </td>
                  <td width="204" align="center">टेलीफ़ोन ऑपरेटर</td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421180</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री ओम प्रकाश साहू </td>
                  <td width="204" align="center">टेलीफ़ोन ऑपरेटर</td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421181</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री पंकज सिंह </td>
                  <td width="204" align="center">टेलीफ़ोन ऑपरेटर</td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">9454421047</td>
                </tr>
        <tr>
                  <td width="29%" align="center">श्री राज कुमार त्रिपाठी </td>
                  <td width="204" align="center">लिफ्टमैन </td>
                  <td width="108" align="center">2208608</td>
                  <td width="20%" align="center">-</td>
                </tr>
        </tbody></table>
              </td>
                        </tr>
                        <tr valign="top"> 
                          <td height="61" colspan="2"> 
                            <div align="justify">
                            <p><font face="Kruti Dev 010"><span lang="en-us">
              शर्ते किराया शासनादेश संख्या - </span></font>
              <a href="../assets/doc/ratenew2.pdf" target="_blank">
              <font face="Kruti Dev 010">एम<span style="font-size: 15pt" lang="en-us">@</span><span style="font-size: 15pt">6924 
              32<span lang="en-us">@</span>3<span lang="en-us">@</span>2010<span lang="en-us">@</span>2</span>एन0टी<span style="font-size: 15pt" lang="en-us">0@</span><span style="font-size: 15pt">92,</span>
              <span lang="en-us">@</span>दिनांक
              <span style="font-size: 15pt">30</span> जून<span style="font-size: 15pt">, 2010</span></font></a></p>
                            <p><font face="Kruti Dev 010"><span lang="en-us">
              भोजन की दरें प्रतिथाल शासनादेश संख्या - </span>
              <a href="../assets/doc/ratenew4.pdf" target="_blank">
              एम<span style="font-size: 15pt"><span lang="en-us">@</span>2768<span lang="en-us">@</span>32<span lang="en-us">@</span>3<span lang="en-us">@</span>2013<span lang="en-us">@</span>5781<span lang="en-us">@</span></span>दिनांक
              <span style="font-size: 15pt">20-05-2013</span></a></font></p>
                            <p>श्रेणी - १ &nbsp;के &nbsp;पात्रता सूची के सन्दर्भ मे ! - <a href="../assets/doc/ratenew1.pdf" target="_blank">संख्या एम् - 
                              4615/32-3-2012 (एन0 टी0) / 92 दिनाक 
                              18-10- 2012</a><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span lang="en-us">&nbsp;
              </span>&nbsp;&nbsp; <a href="../assets/doc/ratenew1.pdf" target="_blank">संख्या एम् - 
                              4543/32-3-2012 (एन0 टी0) / 92 दिनाक 
                            10-09- 2012</a></p>
                            </div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td rowspan="2" height="21"><b>अतिथिगृह आवंटन हेतु 
              सम्पर्क सूत्र</b></td>
                          <td width="64%" height="11"><b>राज्य सम्पत्ति अधिकारी</b><br>
              नवीन भवन,कक्ष सं0-22, उ0प्र0सचिवालय<br>
              फोन नम्बर 2238203, फैक्स नम्बर-2228385</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="64%" height="8"><b>व्यवस्थाधिकारी</b><br>
              फोन नम्बर-0522-2208608, 2208591, 2208620<br>
              फैक्स नम्बर-2208591</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" height="3"><b>राज्य अतिथि के लिए</b></td>
                          <td width="64%" height="3">सचिव, प्रोटोकाल,शास्त्री 
              भवन, कक्ष संख्या-512, उ0प्र0शासन। <br>
              फोन नम्बर-2238316 फैक्स नम्बर 2238258 </td>
                        </tr>
                      </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>