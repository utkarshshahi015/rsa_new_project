<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h4 class="row justify-content-md-center"><u>नवनिर्मित राज्य अतिथि गृह, विक्रमादित्य मार्ग, लखनऊ
</u></h4>
              </div>
              
              <table width="100%" border="2" cellspacing="2" cellpadding="2">
                        <tbody><tr valign="top"> 
                          <td width="27%" height="2">अतिथिगृह में अवस्थान के लिए 
              अर्हता</td>
                          <td width="63%" height="2"> 
                            <div align="Justify">अधिकारी, मा0सांसद, मा0 विधायक, 
                मा0 भूतपूर्व सांसद, मा0 भूतपूर्व विधायक, स्थानीय 
                निकाय के अध्यक्ष, सेवा निवृत अधिकारी, मान्यता 
                प्राप्त पत्रकार, सम्पादक, दिल्ली स्थित केन्द्र 
                सरकार के अधिकारी, अन्य प्रदेशों से आने वाले 
                अधिकारी, विधायक, विधान सभा/परिषद की मा0 समितियों 
                में मा0 सदस्य एवं सर्वजनिक उपक्रमों/निगमों के 
                अध्यक्ष/सदस्य, केन्द्र सरकार के अधिकारी जो दिल्ली 
                से बाहर तैनात हो तथा अन्य व्यक्ति।</div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%">पता</td>
                          <td width="63%">3-विक्रमादित्य मार्ग, लखनऊ, अध्यक्ष 
              राजस्व परिषद के मकान के बाद।</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%">फोन नम्बर</td>
                          <td width="63%">0522-2235213</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%">कक्षों की संख्या</td>
                          <td width="63%">15 (सभी कक्ष वातानुकूलित हैं) </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" height="24">अधिकारी/कर्मचारियों के नाम 
              एवं फ़ोन नंबर </td>
                          <td width="63%" height="24">
            <table width="566" border="1" align="center" bordercolor="#000000" height="15">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="172" align="center"><b>पदनाम </b></td>
                  <td width="114" align="center"><b>कार्यालय नंबर</b></td>
                  <td width="105" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री महेंद्र गुप्ता </td>
                  <td width="172" align="center">कार्यवाहक व्यवस्थाधिकारी </td>
                  <td width="114" align="center">2235213</td>
                  <td width="105" align="center">9454421185</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री फ़िरोज़ अहमद </td>
                  <td width="172" align="center">स्टोर कीपर </td>
                  <td width="114" align="center">2235213</td>
                  <td width="105" align="center">9454421157</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री भगवत सिंह </td>
                  <td width="172" align="center">लेखाकार </td>
                  <td width="114" align="center">2235213</td>
                  <td width="105" align="center">9454421169</td>
                </tr>
            <tr>
              <td height="18" align="center">श्री मुकेश कुमार </td>
                  <td height="18" align="center">सहायक स्टोर कीपर </td>
                  <td height="18" align="center">2235213</td>
                  <td height="18"><p align="center">
          <font face="Times New Roman">9936245335</font></p></td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री राम नरेश पाल </td>
                  <td width="172" align="center">टेलीफोन ऑपरेटर </td>
                  <td width="114" align="center">2235213</td>
                  <td width="105" align="center">9454421160</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री वेद प्रकाश शुक्ला </td>
                  <td width="172" align="center">टेलीफोन ऑपरेटर </td>
                  <td width="114" align="center">2235213</td>
                  <td width="105" align="center">9454421159</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री सुमित सिंह </td>
                  <td width="172" align="center">टेलीफोन ऑपरेटर </td>
                  <td width="114" align="center">2235213</td>
                  <td width="105" align="center">9454421093</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री सौरभ पांडेय </td>
                  <td width="172" align="center">कनिष्ठ सहायक </td>
                  <td width="114" align="center">2235213</td>
                  <td width="105" align="center">9454421088</td>
                </tr>
            </tbody></table>
                    </td>
                        </tr>
                        <tr valign="top"> 
                          <td colspan="2"> 
                            <div align="justify">
                              <p><font face="Kruti Dev 010"><span lang="en-us">
              शर्ते/किराया शासनादेश संख्या - </span></font>
              <a href="../assets/doc/ratenew2.pdf" target="_blank">
              <font face="Kruti Dev 010">एम-6924 32-3-2010 - 
              2एन0टी92 दिनांक 30 जून 2010</font></a></p>
                              <p><font face="Kruti Dev 010"><span lang="en-us">
              भोजन की दरें प्रतिथाल शासनादेश संख्या -</span></font><font face="Kruti Dev 010" size="4"><span lang="en-us">
                </span></font>
                <a href="../assets/doc/2767.pdf" target="_blank">
                <font face="Kruti Dev 010">
                एम-276732-3-2013-5781 दिनांक 20-05-2013</font></a></p>
                              <p>श्रेणी - १ &nbsp;के &nbsp;पात्रता सूची के सन्दर्भ मे ! - <a href="../assets/doc/ratenew1.pdf" target="_blank">संख्या एम् - 
                              4615/32-3-2012 (एन0 टी0) / 92 दिनाक 
                              18-10- 2012</a><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span lang="en-us">&nbsp;
              </span>&nbsp;&nbsp; <a href="../assets/doc/ratenew1.pdf" target="_blank">संख्या एम् - 
                              4543/32-3-2012 (एन0 टी0) / 92 दिनाक 
                            10-09- 2012</a></p>
                            </div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td rowspan="2" height="21"><b>अतिथिगृह आवंटन हेतु 
              सम्पर्क सूत्र</b></td>
                          <td width="63%" height="11"><b>राज्य सम्पत्ति अधिकारी</b><br>
              नवीन भवन,कक्ष सं0-22, उ0प्र0 सचिवालय।<br>
              फोन नम्बर-0522-2238203, फैक्स नम्बर-2228385</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="63%" height="45"><b>व्यवस्थाधिकारी</b><br>
              फोन नम्बर-0522-2235213, मो0नं0-9335566880</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" height="3"><b>राज्य अतिथि के लिए</b></td>
                          <td width="63%" height="3">सचिव, प्रोटोकाल, शास्त्री 
              भवन, कक्ष संख्या-512, उ0प्र0शासन। <br>
              फोन नम्बर-2238316 फैक्स नम्बर 2238258 </td>
                        </tr>
                      </tbody></table>
               
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>