<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h4 class="row justify-content-md-center"><u>अतिविशिष्ट अतिथिगृह महात्मा गांधी मार्ग लखनऊ।
</u></h4>
              </div>
              
               <table width="100%" border="2" align="center" cellpadding="2" cellspacing="2">
                      <tbody><tr valign="top"> 
                        <td colspan="2"> 
                          <div align="center">अतिविशिष्ट अतिथिगृह, लखनऊ में 36 
              कक्ष हैं तथा सभी कक्ष वातानुकूलित हैं। उक्त अतिथिगृह 
              मा0 सांसदों/मा0मंत्रीगण भारत सरकार के अधिकारियों तथा 
              प्रोटोकाल द्वारा घोषित मा0 राज्य अतिथियों के 
              उपयोगार्थ है।</div>                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td width="26%"><b>पता</b></td>
                        <td width="72%">महात्मागांधी मार्ग, लारेटो स्कूल के ठीक 
            सामने, लखनऊ।</td>
                      </tr>
                      <tr valign="top"> 
                        <td width="26%"><b>फोन नम्बर</b></td>
                        <td width="72%">0522-2610072, 2610076, 2235893, 2235159, 
            2235867</td>
                      </tr>
                      <tr valign="top"> 
                        <td width="26%"><b>फैक्स नम्बर </b></td>
                        <td width="72%">2235159 </td>
                      </tr>
                      <tr valign="top"> 
                        <td width="26%"><b>कक्षों की संख्या</b></td>
                        <td width="72%">36</td>
                      </tr>
                      <tr valign="top"> 
                        <td width="26%"><b>व्यवस्थाधिकारी का नाम/पता</b></td>
                        <td width="72%">श्री दिलीप कुमार सिंह ,69/70 बी-ब्लाक, 
            दारूलशफा, लखनऊ। फोन नम्बर-26814843, मो0 न0-9415911291</td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="2"> 
                          <div align="justify">
                            <p><font face="Kruti Dev 010"><span lang="en-us">
              शर्ते/किराया शासनादेश संख्या - </span></font>
              <a href="../assets/doc/ratenew.pdf" target="_blank">
              <font face="Kruti Dev 010">एम-6924<span lang="en-us">
              </span>&nbsp;32-3-2010 - 
              2एन0टी<span lang="en-us">
              </span>92<span lang="en-us"> </span>&nbsp;दिनांक 30 जून<span lang="en-us">
              </span>&nbsp;2010</font></a></p>
                            <p><font face="Kruti Dev 010"><span lang="en-us">
              भोजन की दरें प्रतिथाल शासनादेश संख्या - </span>
              <a href="../assets/doc/guesthouse.pdf" target="_blank">
              एम-276832-3-2013-5781 दिनांक 20-05-2013</a></font></p>
                            <p>श्रेणी - १ &nbsp;के &nbsp;पात्रता सूची के सन्दर्भ मे ! - <a href="../assets/doc/ratenew1.pdf" target="_blank">संख्या एम् - 
                              4615/32-3-2012 (एन0 टी0) / 92 दिनाक 
                              18-10- 2012</a><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span lang="en-us">&nbsp;
              </span>&nbsp;&nbsp; <a href="../assets/doc/ratenew1.pdf" target="_blank">संख्या एम् - 
                              4543/32-3-2012 (एन0 टी0) / 92 दिनाक 
                            10-09- 2012</a></p>
                          </div>                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td width="26%" height="11"><b>आवंटन प्रक्रिया</b></td>
                        <td width="72%" height="11"> 
                          <div align="justify">शासनादेश 
              संख्या-एम-9362/32-3-1998, दिनांक 31.3.1998 में दी गई 
              व्यवस्थानुसार तथा प्रोटोकाल द्वारा घोषित राज्यअतिथि 
              एवं राज्य सम्पत्ति विभाग से सम्पर्क करने पर पात्र 
              महानुभावों को कक्ष आवंटित किया जाता है। </div>                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td rowspan="2"><b>अतिथिगृह आवंटन हेतु सम्पर्क सूत्र</b></td>
                        <td width="72%"><b>राज्य सम्पत्ति अधिकारी</b><br>
            नवीन भवन,कक्ष सं0-22,उ0प्र0सचिवालय फोन नम्बर 2238203, 
            फैक्स नम्बर-2228385</td>
                      </tr>
                      <tr valign="top"> 
                        <td width="72%" height="26"><b>व्यवस्थाधिकारी</b><br>
            फोन नम्बर-0522-2610072, 2610076, 2235893, 2235159<br>
            फैक्स नम्बर-2235159</td>
                      </tr>
                      <tr valign="top"> 
                        <td width="26%"><b>राज्य अतिथि के लिए</b></td>
                        <td width="72%">सचिव, प्रोटोकाल, शास्त्री भवन, कक्ष 
            संख्या-512, उ0प्र0शासन। <br>
            फोन नम्बर-2238316 फैक्स नम्बर 2238258 </td>
                      </tr>
                    </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>