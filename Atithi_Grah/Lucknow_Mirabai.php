<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h4 class="row justify-content-md-center"><u>राज्य अतिथिगृह मीराबाई मार्ग लखनऊ
</u></h4>
              </div>
              
               <table width="82%" border="2" cellspacing="2" cellpadding="2" align="center">
                        <tbody><tr valign="top"> 
                          <td width="27%" height="2" class="style3"><b>अतिथि गृह 
              में अवस्थान के लिए अर्ह</b></td>
                          <td width="69%" height="2" class="style3"> 
                            <div align="justify">अधिकारी, मा0सांसद, मा0 विधायक, 
                मा0 भूतपूर्व सांसद, मा0 भूतपूर्व विधायक, स्थानीय 
                निकाय के अध्यक्ष, सेवा निवृत अधिकारी, मान्यता 
                प्राप्त पत्रकार, सम्पादक, दिल्ली स्थित केन्द्र 
                सरकार के अधिकारी, अन्य प्रदेशों से आने वाले 
                अधिकारी, विधायक, विधान सभा/परिषद की मा0 समितियों 
                में मा0 सदस्य एवं सर्वजनिक उपक्रमों/निगमों के 
                अध्यक्ष/सदस्य, केन्द्र सरकार के अधिकारी जो दिल्ली 
                से बाहर तैनात हो तथा अन्य व्यक्ति।</div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="27%" class="style3"><b>पता</b></td>
                          <td width="69%" class="style3">मीराबाई मार्ग, 
              इन्दिराभवन के ठीक पीछे, लखनऊ।</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="27%" height="26" class="style3"><b>फोन 
              नम्बर</b></td>
                          <td width="69%" height="26" class="style3"> 
                            <p>0522-2287318, 2287488, 2288634 (पी0बी0एक्स0)</p>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="27%" class="style3"><b>कक्षों की संख्या</b></td>
                          <td width="69%" class="style3">61 (सभी कक्ष 
              वातानुकूलित हैं)</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="27%" height="42" class="style3"><b>अधिकारी/कर्मचारियों 
              के नाम एवं फ़ोन नंबर </b></td>
                          <td width="69%" height="42" class="style3"> 
            <table width="646" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="193" align="center"><b>पदनाम </b></td>
                  <td width="134" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="21%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री रवीन्द्र प्रताप सिंह</td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <p class="MsoNormal" align="center">कार्यवाहक व्यवस्थाधिकारी</p>
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  <p class="MsoNormal" align="center" style="text-align:center"> <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318, 2287488, 2288634</font></span></font><font face="Times New Roman" style="font-size: 13pt">
                    </font> </p>
  </td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  <p class="MsoNormal" align="center" style="text-align:center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt">
          9415393803<br>
          9454421226</span></p>
  <p class="MsoNormal" align="center" style="text-align:center"> 
                    <font face="Times New Roman" style="font-size: 13pt">
                    &nbsp;</font></p>
  </td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री रामशंकर सिंह</td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <p align="center">स्टोर कीपर
  </p></td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  &nbsp;</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री राम कृष्ण परमहंस </td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  पूछताछ लिपिक
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  9454421140</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री लालजी दुबे </td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  टेलीफ़ोन ऑपरेटर.
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  &nbsp;</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री रमेश चन्द्र </td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  कनिष्ठ लिपिक
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  9454421143</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री मुमताज़ अहमद </td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  पूछताछ लिपिक
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  &nbsp;</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री हसन अब्दी </td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  कनिष्ठ लिपिक
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  9454421147</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री सोबरन प्रसाद </td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  कनिष्ठ लिपिक
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  9454421142</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री जीतेन्द्र कुमार वर्मा </td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  कनिष्ठ लिपिक .
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  9454421150</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री विजय कुमार आनंद</td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  टेलीफ़ोन ऑपरेटर.
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  9454421146</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री पी0 के0 सिंह </td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  टेलीफ़ोन ऑपरेटर.
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  9454421136</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री कमल किशोर </td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  कनिष्ठ लिपिक .
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  &nbsp;</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री संतोष यादव </td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  सहायक स्टोर कीपर
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  &nbsp;</td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  श्री विवेक कुमार गुप्ता </td>
  <td valign="top" style="width:181;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  सहायक स्टोर कीपर
  </td>
  <td valign="top" style="width:120;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    <font style="font-size: 13pt">2287318</font></span></font></td>
  <td valign="top" style="width:152;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" align="center">
  9454421151</td>
            </tr>
            </tbody></table>
              </td>
                        </tr>
                        <tr valign="top"> 
                          <td colspan="2" class="style3"> 
                            <div align="justify">
                            <p><font face="Kruti Dev 010"><span lang="en-us">
              शर्ते किराया शासनादेश संख्या - </span></font>
              <a href="../assets/doc/ratenew2.pdf" target="_blank">
              <font face="Kruti Dev 010">एम-6924 32-3-2010 - 
              2एन0टी92 दिनांक 30 जून 2010</font></a></p>
                            <p><font face="Kruti Dev 010"><span lang="en-us">
              भोजन की दरें प्रतिथाल शासनादेश संख्या - </span>
              <a href="../assets/doc/ratenew4.pdf" target="_blank">
              एम-2768<span lang="en-us"> </span>32-3-2013-5781 दिनांक 20-05-2013</a></font></p>
                            <p>श्रेणी - १ के पात्रता सूची के सन्दर्भ मे ! - <a href="../assets/doc/ratenew1.pdf" target="_blank">संख्या एम् - 
                              4615/32-3-2012 (एन0 टी0) / 92 दिनाक 
                              18-10- 2012</a><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span lang="en-us">&nbsp;
              </span>&nbsp;&nbsp;<span lang="en-us">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              </span>&nbsp;<a href="../assets/doc/ratenew1.pdf" target="_blank">संख्या एम् - 
                              4543/32-3-2012 (एन0 टी0) / 92 दिनाक 
                            10-09- 2012</a></p>
                            </div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="27%" height="11" class="style3"><b>आवंटन 
              प्रक्रिया<br>
&nbsp;</b></td>
                          <td width="69%" height="11" class="style3"> 
                            <div align="justify">शासनादेश 
                संख्या-एम-9362/32-3-1998, दिनांक 31.3.1998 में 
                दी गई व्यवस्थानुसार तथा प्रोटोकाल द्वारा घोषित 
                राज्यअतिथि एवं राज्य सम्पत्ति विभाग से सम्पर्क 
                करने पर पात्र महानुभावों को कक्ष आवंटित किया जाता 
                है। </div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td rowspan="2" class="style3"><b>अतिथिगृह आवंटन हेतु 
              सम्पर्क सूत्र</b></td>
                          <td width="69%" class="style3"><b>राज्य सम्पत्ति 
              अधिकारी</b><br>
              नवीन भवन, कक्ष सं0-22, उ0प्र0 सचिवालय फोन नम्बर 
              2238203, फैक्स नम्बर-2228385</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="69%" height="26" class="style3"><b>
              व्यवस्थाधिकारी</b><br>
              फोन नम्बर-0522-2287318, 2287488<br>
              फैक्स नम्बर-2287318</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="27%" height="2" class="style3"><b>राज्य 
              अतिथि के लिए</b></td>
                          <td width="69%" height="2" class="style3">सचिव, 
              प्रोटोकाल,शास्त्री भवन, कक्ष संख्या-512, उ0प्र0शासन।
              <br>
              फोन नम्बर-2238316 फैक्स नम्बर 2238258 </td>
                        </tr>
                      </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>