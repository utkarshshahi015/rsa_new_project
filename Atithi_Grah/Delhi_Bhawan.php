<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h4 class="row justify-content-md-center"><u>नई दिल्ली स्थित अतिथिगृह /उ0प्र0भवन, नई दिल्ली</u></h4>
              </div>
              <table width="100%" border="2" cellspacing="2" cellpadding="2">
                        <tbody><tr valign="top"> 
                          <td width="27%" height="2" class="style2">
              <font color="#000000">अतिथिगृह में अवस्थान के लिए 
              अर्हता</font></td>
                          <td width="73%" height="2" class="style2"> 
                            <div align="justify"><span style="font-weight: 400">
                <font color="#000000">अधिकारी, मा0सांसद, मा0 
                विधायक, मा0 भूतपूर्व सांसद, मा0 भूतपूर्व विधायक, 
                स्थानीय निकाय के अध्यक्ष, सेवा निवृत अधिकारी, 
                मान्यता प्राप्त पत्रकार, सम्पादक, दिल्ली स्थित 
                केन्द्र सरकार के अधिकारी, अन्य प्रदेशों से आने 
                वाले अधिकारी, विधायक, विधान सभा/परिषद की मा0 
                समितियों में मा0 सदस्य एवं सर्वजनिक उपक्रमों/निगमों 
                के अध्यक्ष/सदस्य, ेकन्द्र सरकार के अधिकारी जो 
                दिल्ली से बाहर तैनात हो तथा अन्य व्यक्ति।</font></span></div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" class="style2"><font color="#000000">
              पता</font></td>
                          <td width="69%" class="style2">
              <span style="font-weight: 400">
              <font color="#000000">सरदार पटेल मार्ग, नई दिल्ली।</font></span></td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" class="style2"><font color="#000000">
              फोन नम्बर</font></td>
                          <td width="69%" class="style2">
              <span style="font-weight: 400">
              <font color="#000000">011-26110151 से 55 तक</font></span></td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" class="style2"><font color="#000000">
              कक्षों की संख्या</font></td>
                          <td width="69%" class="style2">
              <span style="font-weight: 400">
              <font color="#000000">77 (71 कक्ष सिंगल तथा 06 कक्ष 
              डबल हैं तथा सभी कक्ष वातानूकूलित हैं।)</font></span></td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" class="style2"><font color="#000000">
              व्यवस्थाधिकारी का नाम/पता</font></td>
                          <td width="69%" class="style2">
              <span style="font-weight: 400">
              <font color="#000000">1-श्री राजेश चौबे व्यवस्था 
              अधिकारी उत्तर प्रदेश भवन नई दिल्ली 
              मोबाइल-09868111044 </font></span></td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" height="87" class="style2">
              <font color="#000000">अधिकारी/कर्मचारियों के नाम एवं 
              फ़ोन नंबर </font></td>
                          <td width="69%" height="87" class="style2">
            <table width="612" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="154" align="center"><b>नाम </b></td>
                  <td width="170" align="center"><b>पदनाम </b></td>
                  <td width="131" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="130" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री राजेश कुमार चौबे </td>
                  <td width="170" align="center">व्यवस्थापक प्रभारी</td>
                  <td width="131" align="center"> 
                    <font face="Kruti Dev 010" style="font-size: 13pt">
                    <span style="font-family: Times New Roman"> 011-26110151-55, 26114775(fax)</span></font><font face="Times New Roman" style="font-size: 13pt">
                    </font> </td>
                  <td width="130" align="center"> 
                    <font face="Kruti Dev 010" style="font-size: 13pt">
                    <span style="font-family: Times New Roman">09868111044, 
          9312311440</span></font></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्रीमती दीपिका कपूर</td>
                  <td width="170" align="center">विशेष कार्याधिकारी </td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center"><font face="Kruti Dev 010">
          <span style="font-family: Times New Roman; font-size: 13pt">
          9811842312</span></font></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री राम प्रकाश</td>
                  <td width="170" align="center">व्यवस्थापक </td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center">
              <span style="font-weight: 400">
              <font color="#000000">09868354040</font></span></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री जयवीर सिंह</td>
                  <td width="170" align="center">&nbsp;टेलीफोन ऑपेरटर </td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center"><font face="Kruti Dev 010">
          <span style="font-family: Times New Roman; font-size: 13pt">
          8802571409</span></font></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री पारस नाथ</td>
                  <td width="170" align="center">स्वागती</td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center"><font face="Kruti Dev 010">
          <span style="font-family: Times New Roman; font-size: 13pt">
          9868377611</span></font></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री उदयभान सिंह</td>
                  <td width="170" align="center">कनिष्ठ सहायक</td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center"><font face="Kruti Dev 010">
          <span style="font-family: Times New Roman; font-size: 13pt">
          9968328988</span></font></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री लक्ष्मण प्रसाद</td>
                  <td width="170" align="center">&nbsp;स्टोर कीपर </td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center"><font face="Kruti Dev 010">
          <span style="font-family: Times New Roman; font-size: 13pt">
          9958826211</span></font></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री रक्षपाल सिंह</td>
                  <td width="170" align="center">&nbsp;टेलीफोन ऑपेरटर </td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center"><font face="Kruti Dev 010">
          <span style="font-family: Times New Roman; font-size: 13pt">
          9871365669</span></font></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री जावेद वारिश</td>
                  <td width="170" align="center">कनिष्ठ सहायक </td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center"><font face="Kruti Dev 010">
          <span style="font-family: Times New Roman; font-size: 13pt">
          9871750431</span></font></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री राकेश कुमार सिंह </td>
                  <td width="170" align="center">कनिष्ठ सहायक</td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center"><font face="Kruti Dev 010">
          <span style="font-family: Times New Roman; font-size: 13pt">
          9968572323</span></font></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री दीपक </td>
                  <td width="170" align="center">कनिष्ठ सहायक </td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center"><font face="Kruti Dev 010">
          <span style="font-family: Times New Roman; font-size: 13pt">
          9999881834</span></font></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री राम बहाल यादव </td>
                  <td width="170" align="center">टेलीफोन ऑपेरटर</td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center"><font face="Kruti Dev 010">
          <span style="font-family: Times New Roman; font-size: 13pt">
          9871295109</span></font></td>
                </tr>
      <tr>
                  <td width="154" align="center">श्री ओमप्रकाश सिंह </td>
                  <td width="170" align="center">वरिष्ठ सहायक</td>
                  <td width="131" align="center">&nbsp;</td>
                  <td width="130" align="center"><font face="Kruti Dev 010">
          <span style="font-family: Times New Roman; font-size: 13pt">
          9958160351</span></font></td>
                </tr>
            </tbody></table>
            &nbsp;</td>
                        </tr>
                        <tr valign="top"> 
                          <td height="64" colspan="2" class="style2"> 
                            <div align="justify">
                              <p><span style="font-weight: 400">
                <font face="Kruti Dev 010">
                <a href="../assets/doc/ratenew2.pdf" target="_blank">
                <span style="text-decoration: none">
                <font color="#000000">
                शर्ते/किराया 
                शासनादेश संख्या -</font>एम</span><span style="font-size: 15pt">-6924 
                32-3-2010 - 2</span>एन0टी<span style="font-size: 15pt">92</span> दिनांक
                <span style="font-size: 15pt">30</span> जून
                <span style="font-size: 15pt">2010</span></a></font></span></p>
                              <p><span style="font-weight: 400">
                <font color="#000000">
                <span style="text-decoration: none">श्रेणी . १ 
                के पात्रता सूची के सन्दर्भ मे ! . <br>
                </span></font><a href="../assets/doc/ratenew1.pdf" target="_blank">
                <font color="#000000">
                <span style="text-decoration: none">संख्या एम्<span lang="en-us">
                </span></span>4615ध्32.3.2012 ;एन0 टी0द्ध ध् 92 
                दिनाक 18.10. 2012</font></a><font color="#000000"><br>
                <a href="../assets/doc/ratenew1.pdf" target="_blank">संख्या एम् . 
                4543<span lang="en-us">/</span>32.3.2012 ;एन0 
                टी0 92 दिनाक 10.09. 
                2012</a><br>
                              </font><font color="#000000" face="Kruti Dev 010">
                <a href="../assets/doc/2444.pdf" target="_blank">
                एम<span style="font-size: 15pt">-2444<span lang="en-us">
                </span>32-3-1997,</span> दिनांक
                <span style="font-size: 15pt">14</span> मई
                <span style="font-size: 15pt">1997</span></a><span style="font-size: 15pt">
                </span> </font>
                </span></p>
                              <p><span style="font-weight: 400">
                <font face="Kruti Dev 010">
                <a href="../assets/doc/2766.pdf" target="_blank">
                <font color="#000000">भोजन की दरें प्रतिथाल 
                शासनादेश</font> संख्या-एम<span style="font-size: 15pt">-2766<span lang="en-us">@</span>32<span lang="en-us">@</span>3<span lang="en-us">@</span>2013-57<span lang="en-us">-</span>81</span> दिनांक<span style="font-size: 15pt"> 
                20-05-2013</span></a></font></span></p>
                            </div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td height="7" rowspan="2" class="style2">
              <span style="font-weight: 400">
              <font color="#000000">अतिथिगृह आवंटन हेतु सम्पर्क 
              सूत्र</font></span></td>
                          <td width="69%" height="3" class="style2">
              <span style="font-weight: 400">
              <font color="#000000">राज्य सम्पत्ति अधिकारी<br>
              नवीन भवन,कक्ष सं0-22,उ0प्र0सचिवालय<br>
              फोन नम्बर-0522-2238203, फैक्स नम्बर-2228385</font></span></td>
                        </tr>
                        <tr valign="top"> 
                          <td width="69%" height="2" class="style2">
              <span style="font-weight: 400">
              <font color="#000000">व्यवस्थाधिकारी<br>
              फोन नम्बर- 011-26110151 से 55 तक<br>
              फैक्स नम्बर-011-26110847, 26114775</font></span></td>
                        </tr>
                      </tbody></table>
               
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>