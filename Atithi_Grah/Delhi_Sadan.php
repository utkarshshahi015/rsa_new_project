<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h5 class="row justify-content-md-center"><u>नई दिल्ली स्थित अतिथिगृह उ0प्र0सदन<span lang="en-us">
            </span>नई दिल्ली<br><br></u></h5>
              <div class="col-md-6 offset-md-3">
                <span style="font-weight: 400" class="row justify-content-md-center">उ0प्र0सदन में कुल
            52 कक्ष हैं। विशिष्ट प्रकृति का है। <br>
            गैर सरकारी व्यक्तियों के लिए अनुमति नहीं है। </span>
              </div>
              
              <table width="100%" border="2" cellspacing="2" cellpadding="2">
                        <tbody><tr valign="top"> 
                          <td colspan="2" height="21"> 
                            <p align="justify"><b><u>श्री राज्यपाल/मा0मुख्यमंत्री 
              ब्लाक</u><span lang="en-us"> -</span></b><span lang="en-us">
              </span>यह ब्लाक महामहिम राज्यपाल जी एवं 
              मा0मुख्यमंत्री जी के लिए आरक्षित हैं।<br>
              <br>
              <b><u>मंत्री ब्लाक</u><span lang="en-us"> - </span>
              </b>यह ब्लाक मा0अध्यक्ष/उपाध्यक्ष विधानसभा 
              मा0सभापति/उपसभापति विधानपरिषद मा0 कैबिनेट/उपमंत्रीगण/मुख्य/न्यायाधीश 
              के लिये आरक्षित है। उक्त ब्लाक में कुल 17 डबल कक्ष 
              हैं तथा 04 सिंगल कक्ष हैं।<br>
              <br>
              <b><u>सचिव ब्लाक</u><span lang="en-us"> - </span>
              </b>इस ब्लाक में उ0प्र0शासन के मुख्यसचिव/प्रमुख 
              सचिव/सचिव/महानिदेशक/ विभिन्न आयोगों/निगमों के 
              अध्यक्ष एवं सदस्यगण ठहरने के लिये पात्र होंगे। उक्त 
              ब्लाक में 03 डबल कक्ष 25 सिंगल कक्ष तथा 10 बेड की 
              डारमेटरी है।</p>
                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%"><b>पता</b></td>
                          <td width="69%">चाणक्यपुरी, नई दिल्ली।</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%"><b>फोन नम्बर</b></td>
                          <td width="69%">011-24678735 से 46 तक, फैक्स 
              नं-24678750</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%"><b>कक्षों की संख्या</b></td>
                          <td width="69%">52 (सभी कक्ष वातानुकूलित हैं)</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" height="42"><b>व्यवस्थाधिकारी का नाम/पता</b></td>
                          <td width="69%" height="42"><p>श्री राजीव तिवारी<span lang="en-us">,</span> 
              व्यवस्था अधिकारी उत्तर प्रदेश सदनए चाद्क्यपुरीए नई 
              दिल्ली<br>
              फोन नम्बर-011-24678735-42 मोबाइल-<span class="style58"><span class="style45"><span class="style65"><font face="Kruti Dev 010"><span style="font-family: Times New Roman"><font style="font-size: 13pt">09810363166</font></span></font><font face="Times New Roman" style="font-size: 13pt">,&nbsp;</font></span></span></span><font face="Kruti Dev 010" style="font-size: 13pt"><span style="font-family: Times New Roman">9335566880</span></font><span class="style58"><span class="style45"><span class="style65"><font face="Times New Roman" style="font-size: 13pt"> </font> </span></span></span></p>                            </td>
                        </tr>
                        <tr valign="top"> 
                          <td width="31%" height="148"><b>अधिकारी/कर्मचारियों के 
              नाम एवं फ़ोन नंबर </b></td>
                          <td width="69%" height="148"> 
            <table width="597" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="154" align="center"><b>नाम </b></td>
                  <td width="157" align="center"><b>पदनाम </b></td>
                  <td width="130" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="130" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:155px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">श्री राजीव तिवारी</td>
        <td bgcolor="#FFFFFF" style="width:160px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:16">
        <font face="Kruti Dev 010" style="font-size: 16pt">
        <span style="font-family:&quot;Kruti Dev 010&quot;">O;oLFkkf/kdkjh</span></font></td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:16" align="center">
                    <font face="Kruti Dev 010" style="font-size: 13pt">
                    <span style="font-family: Times New Roman"> 011-24678754, 
          011-24678735, 
                    24678750 (fax)</span></font></td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:16" align="center">
        <span class="style58"><span class="style45"><span class="style65"><font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">09810363166</font></span></font><font face="Times New Roman" style="font-size: 13pt">,&nbsp;</font></span></span></span><font face="Kruti Dev 010" style="font-size: 13pt"><span style="font-family: Times New Roman"><br>
    09335566880</span></font><span class="style58"><span class="style45"><span class="style65"><font face="Times New Roman" style="font-size: 13pt"> </font> </span></span></span></td>
            </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:155px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:35; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री श्याम सुन्दर यादव </td>
        <td bgcolor="#FFFFFF" style="width:160px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        व्यवस्थापक </td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        &nbsp;</td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        <span class="style65">
    <span style="font-family: Times New Roman; font-size: 13pt">9818337439</span></span></td>
            </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:155px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:35; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री बिजेन्द्र सिंह </td>
        <td bgcolor="#FFFFFF" style="width:160px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        वरिष्ठ सहायक </td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        &nbsp;</td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        9868633850</td>
            </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:155px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:35; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री द्वारका नाथ मिश्र </td>
        <td bgcolor="#FFFFFF" style="width:160px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        प्रधान सहायक</td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        &nbsp;</td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        <span class="style65">
    <span style="font-family: Times New Roman; font-size: 13pt">9968900779</span></span></td>
            </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:155px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:35; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री विनोद कुमार गुप्ता </td>
        <td bgcolor="#FFFFFF" style="width:160px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        लेखाकार </td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        &nbsp;</td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        <span class="style65">
    <span style="font-family: Times New Roman; font-size: 13pt">8800234600</span></span></td>
            </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:155px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:35; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री नत्थू सिंह </td>
        <td bgcolor="#FFFFFF" style="width:160px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        सहायक प्रोटोकाल </td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        &nbsp;</td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        <span class="style65">
    <span style="font-family: Times New Roman; font-size: 13pt">9871261421</span></span></td>
            </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:155px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:35; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री उपेंद्र यादव </td>
        <td bgcolor="#FFFFFF" style="width:160px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        स्टोर कीपर </td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        &nbsp;</td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        <span class="style65">
    <span style="font-family: Times New Roman; font-size: 13pt">9312732077</span></span></td>
            </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:155px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:35; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री शारदा प्रसाद यादव </td>
        <td bgcolor="#FFFFFF" style="width:160px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        कंप्यूटर ऑपेरटर </td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        &nbsp;</td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        <span class="style65">
    <span style="font-family: Times New Roman; font-size: 13pt">9213893650</span></span></td>
            </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:155px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:35; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री कुरन सिंह </td>
        <td bgcolor="#FFFFFF" style="width:160px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        कंप्यूटर ऑपेरटर </td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        &nbsp;</td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        <span class="style65">
    <span style="font-family: Times New Roman; font-size: 13pt">9968475443</span></span></td>
            </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:155px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:35; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री विन्दादीन</td>
        <td bgcolor="#FFFFFF" style="width:160px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        स्वागती </td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        &nbsp;</td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        <span class="style65">
    <span style="font-family: Times New Roman; font-size: 13pt">9968041141</span></span></td>
            </tr>
            <tr>
        <td bgcolor="#FFFFFF" style="width:155px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:35; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
        श्री महेश चंद्र शर्मा</td>
        <td bgcolor="#FFFFFF" style="width:160px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35">
        कनिष्ठ सहायक </td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        &nbsp;</td>
        <td bgcolor="#FFFFFF" style="width:133px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:35" align="center">
        <span class="style65">
    <span style="font-family: Times New Roman; font-size: 13pt">9560756189</span></span></td>
            </tr>
            </tbody></table>
              </td>
                        </tr>
                        <tr valign="top"> 
                          <td colspan="2" height="64"> 
                            <div align="justify">
                              <p><span style="font-weight: 400">
                <font face="Kruti Dev 010">
                <a href="../assets/doc/ratenew2.pdf" target="_blank">
                <font color="#000000">
                <span style="text-decoration: none">शर्तेकिराया 
                शासनादेश संख्या -एम<span lang="en-us">
                </span></span><span style="font-size: 15pt">-6924 32-3-2010  
                2</span>एन0टी<span style="font-size: 15pt" lang="en-us">
                </span><span style="font-size: 15pt">92</span> दिनांक
                <span style="font-size: 15pt">30</span> जून<span style="font-size: 15pt"> 2010</span></font></a></font></span></p>
                              <p><span style="font-weight: 400">
                <font color="#000000">
                <span style="text-decoration: none">श्रेणी . १ 
                के पात्रता सूची के सन्दर्भ मे ! . <br>
                </span></font><a href="../assets/doc/ratenew1.pdf" target="_blank">
                <font color="#000000">
                <span style="text-decoration: none">संख्या एम्<span lang="en-us">
                </span></span>4615<span lang="en-us"> </span>32.3.2012 ;एन0 टी0 92 दिनाक 18.10. 2012</font></a><font color="#000000"><br>
                <a href="../assets/doc/ratenew1.pdf" target="_blank">संख्या एम् . 
                4543<span lang="en-us">/</span>32.3.2012 ;एन0 टी0<span lang="en-us">/</span>92 दिनाक 10.09. 
                2012</a><br>
                              </font><font color="#000000" face="Kruti Dev 010">
                <a href="../assets/doc/2444.pdf" target="_blank">
                एम-<span style="font-size: 15pt">2444/32-3-1997<span lang="en-us">@</span>
                </span>दिनांक
                <span style="font-size: 15pt">14</span> मई<span style="font-size: 15pt"> 1997</span></a><span style="font-size: 15pt">
                </span> </font>
                </span></p>
                              <p align="left"><span style="font-weight: 400">
                <font face="Kruti Dev 010">
                <a href="../assets/doc/2766.pdf" target="_blank">
                <font color="#000000">भोजन की दरें प्रतिथाल 
                शासनादेश संख्या-एम-<span style="font-size: 15pt">2766<span lang="en-us">
                </span>32-3-2013-57<span lang="en-us"> </span>81</span> दिनांक<span style="font-size: 15pt"> 
                20-05-2013</span></font></a></font></span></p>
                            </div>                          </td>
                        </tr>
                        <tr valign="top"> 
                          <td rowspan="2" height="6"><b>अतिथिगृह आवंटन हेतु सम्पर्क 
              सूत्र</b></td>
                          <td width="69%" height="2">राज्य सम्पत्ति अधिकारी<br>
              नवीन भवन,कक्ष सं0-22,उ0 प्र0 सचिवालय<br>
              फोन नम्बर-0522-2238203, फैक्स नम्बर-2228385</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="69%" height="2">व्यवस्थाधिकारी<br>
              फोन नम्बर- 011-24678735 से 46 तक <br>
              फैक्स नम्बर-011-24678750</td>
                        </tr>
                      </tbody></table>
               
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>