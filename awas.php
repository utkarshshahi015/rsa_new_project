<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">आवास/नियमावली एवं महत्वपूर्ण शासनादेश</h2>
               <table width="100%" border="2" cellspacing="2" cellpadding="2" align="center">
                <tbody><tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">उत्तर प्रदेश 
          मंत्री और राज्य विधान मण्डल अधिकारी निवास स्थान<span lang="en-us">
          </span>
                    </font>
          <font face="Kruti Dev 010">
          <a target="_blank" href="assets/doc/awas1.pdf">नियमावली-<font style="font-size: 15pt">1959</font></a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <a href="#"><span lang="en-us">
          <font face="Kruti Dev 010" color="#000000">
          <span style="text-decoration: none">आवंटन नियमावली </span>
          </font></span></a><font face="Kruti Dev 010">
          <a target="_blank" href="assets/doc/awas22.pdf">
          <span style="font-size: 15pt">1980</span> संख्या आर<span style="font-size: 15pt">-5371<span lang="en-us">-</span>32-80-100(122-70</span> दिनांक
          <span style="font-size: 15pt">29<span lang="en-us">-</span>11<span lang="en-us">-</span>1980</span></a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font face="Kruti Dev 010" color="#000000">मध्य वर्ग गृह 
          निर्माण योजना के अन्तर्गत बने क्वाटरों के आवंटन सम्बन्धी<span lang="en-us">
          </span>नियम</font><font face="Kruti Dev 010"><font color="#000000"> </font> 
          <a href="#"><font color="#FF0000">a</font></a></font><font face="Kruti Dev 010"><a target="_blank" href="assets/doc/awas2.pdf">सं-आर-236/रा.सं.वि.1962 
          दि.-01.11.1962</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">
          <span lang="en-us">उत्तर प्रदेश राज्य विधान मण्डल के सदस्यों 
          के निवास स्थान संबंधी </span>
                    </font>
          <font face="Kruti Dev 010">
          <a target="_blank" href="#">नियमावली-1963</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">राजनैतिक दलों को 
          पार्टी के प्रयोगार्थ लखनऊ स्थित राजकिय आवासों का आवंटन 
          सम्बन्धी </font> 
          <font face="Kruti Dev 010">
          <a target="_blank" href="#">शासनादेश 
          दिनांक-22.06.1982</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font face="Kruti Dev 010">
                  <font color="#000000">मान्यता प्राप्त पत्रकारों को आवास आवंटन 
          सम्बन्धी नियमावली<span lang="en-us"> </span>
                    </font><a href="#"><font color="#FF0000">a</font></a></font><font face="Kruti Dev 010"><a target="_blank" href="#">सं-आर-1408/32-2-85-211/77टी0सी0 
          दि.-21.05.1985</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" height="2" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" height="2" class="style2" align="justify">
          <span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">लखनऊ स्थित राजकीय 
          आवासों के लिए वर्तमान किराया प्रणाली के स्थान पर फ्लैट रेन्ट 
          एवं अवैध अध्यासन के लिए क्षतिपूति का पुनर्निधारण विषयक 
          शासनादेश </font><font face="Kruti Dev 010">
                  <font color="#000000">&nbsp;</font><a href="#"><font color="#FF0000">a-</font></a></font><font face="Kruti Dev 010"><a target="_blank" href="#">सं.-आर-8004/32-2-88-10-87 
          दि.-24.12.1988</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">लखनऊ स्थित राजकीय 
          आवासों के लिए वर्तमान किराया प्रणाली के स्थान पर फ्लैट रेन्ट 
          एवं अवैध अध्यासन के लिए क्षतिपूति का पुनर्निधारण विषयक 
          शासनादेश </font> 
          <font face="Kruti Dev 010">
          <a target="_blank" href="#">सं.-बार-6888/32-2-89-10/89 दि.-03.11.1989</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">बटलर पैलेस के 
          आवासों हेतु फ्लैट रेन्ट एवं अवैध अध्यासन के लिए क्षतिपूति का 
          पुनर्निधारण विषयक शासनादेश <span lang="en-us">&nbsp;</span></font><font face="Kruti Dev 010"><a target="_blank" href="#">सं.-आर-5673/32-2-90-10/87 
          दि.-31.08.1990</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">राजकीय कर्मियों 
          को सेवानिवृर्ति/मृत्यु एवं स्थानांतरण की स्थिति में राज्य 
          सम्पत्ति विभाग के आवासों के अपरिहार्य अध्यासन के लिए किराया 
          का निर्धारण विषयक शासनादेश </font>
          <font face="Kruti Dev 010">
          <a target="_blank" href="#">सं.आर-02/32-2-92-4-60-85 दिनांक-02.01.1992</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">राज्य सम्पत्ति 
          विभाग के आवासों के किराये पुनर्निधारण विषयक शासनादेश<span lang="en-us">
          </span>
                    </font>
          <font face="Kruti Dev 010">
          <a target="_blank" href="assets/doc/awas10.pdf">सं. 
          आर-5166/32-2-98 दिनांक-01.08.1998</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">पुलिस रेडियों 
          कर्मचारियों का आवास किराया फ्लेट रेंट की दो गुनी दर के स्थान 
          पर सामान्य दर पर लिये जाने के सम्बन्ध में शासनादेश<span lang="en-us">
          </span>
                    </font>
          <font face="Kruti Dev 010">
          <a target="_blank" href="#">सं.-आर-871/32-2-99 दिनांक-17.01.1999</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">भूतपूर्व 
          मुख्यमंत्री निवास स्थान आवंटन </font>
          <font face="Kruti Dev 010">
          <a target="_blank" href="#">नियमावली-1997 (असांविधिक) 
          एवं (प्रथम संशोधन) नियमावली, 2001 (असांविधिक)</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" height="2" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" height="2" class="style2" align="justify">
          <span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">भूतपूर्व 
          मुख्यमंत्री निवास स्थान आवंटन (तृतीय संशोधन) नियमावली-2007 (असांविधिक)<br>
                    </font>
                    <a target="_blank" href="#">
          <font face="Kruti Dev 010">संख्या 
          आर-5521/32-2-2007-14/95 दिनांक 25.09.2007</font></a></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" height="2" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" height="2" class="style2" align="justify">
          <span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">गैर सरकारी व्यक्ति/संस्थाओं/राजनैतिक 
          दलों/पूर्व विधायकों/पूर्व मंत्री इत्यादि के किराया निर्धारण 
          सम्बन्धी कार्यालय ज्ञाप <br>
                    </font>
          <font face="Kruti Dev 010">
          <a target="_blank" href="#">संख्या 
          आर-5336/32-2-2001-29-2001 दिनांक-04.12.2001</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">राजकीय कर्मियों 
          के सेवानिवृत्र्ति/मृत्यु एवं स्थानांतरण की स्थिति में राज्य 
          सम्पत्ति विभाग के विभिन्न श्रेणी के आवासों के अपरिहार्य 
          अध्यासन के क्षर्तिपूर्ति किराये को दो गुना किये जाने सम्बन्धी 
          शासनादेश<span lang="en-us"> </span>
                    </font>
          <font face="Kruti Dev 010">
          <a target="_blank" href="#">सं0 
          आर-7003/32-2-2001-27-2001 दिनांक-11.01.2002</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">राज्य सम्पत्ति 
          आवास को क्रमिर्यो के वेतनमान के अनुसार श्रेणीवार आवास की 
          पात्रता मे विभाजन <br>
                    </font>
                    <a href="#">
          <font face="Kruti Dev 010">संख्या 
          आर-2599/32-2-2002-27-01 दिनांक-26.06.2002</font></a></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font face="Kruti Dev 010">
                  <font color="#000000">लखनऊ स्थित राज्य सम्पत्ति विभाग के 
          नियन्त्रणाधीन आवासों का किराया निर्धारण विषयक शासनादेश <br>
                    </font><font color="#FF0000">a</font></font><font face="Kruti Dev 010" color="#0000FF">संख्या-आर 
          4528/32-2-02-21/01 दिनांक 20-2-2003</font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" height="2" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" height="2" class="style2" align="justify">
          <span style="font-weight: 400"><font face="Kruti Dev 010"> 
                  <font color="#000000">सुविख्यात व्यक्ति के ट्रस्ट के उपयोगार्थ 
          राज्य सम्पत्ति विभाग के प्रशासनिक नियंत्रण मे लखनऊ स्थित भवनो 
          की आवंटन नियमावली, 2003 <br>
                    </font>
                    <a href="#"><font color="#FF0000">a</font></a></font><a href="#"><font face="Kruti Dev 010">संख्या 
          आर-5489/32-2-2003-28/2001 दिनांक-31.12.2003</font></a></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font face="Kruti Dev 010">
                  <font color="#000000">लखनऊ सिथत राजकिय आवासों के लिये वर्तमान 
          किराया प्रणाली के स्थान पर फ्लैट रेन्ट एवं अवैध अध्यासन के 
          लिए क्षर्तिपूर्ति का पुनर्निधारण विषयक शासनादेश <br>
                    </font>
                    <a href="Awas/19.htm"><font color="#FF0000">a</font></a></font><a href="Awas/19.htm"><font face="Kruti Dev 010">संख्या 
          आर-5497/32-2-2004-4-4-2004 टी0सी0 दिनांक -14.12.2004</font></a></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font face="Kruti Dev 010"> 
                  <font color="#000000">गैर सरकारी संस्थाओं/ट्रस्ट, गैर सरकारी 
          व्यक्तियों व कर्मचारी संघों के उपयोगार्थ राज्य सम्पत्ति 
          विभाग के प्रशासनिक नियंत्रण में लखनऊ स्थित भवनो का आवंटन 
          सम्बन्धी शसनादेश <br>
                    </font>
                    <a href="Awas/20.htm"><font color="#FF0000">a</font></a></font><a href="Awas/20.htm"><font face="Kruti Dev 010">संख्या 
          आर-2839/32-2-2005-27/2004 टी.सी. दिनांक-04.07.2005</font></a></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify"><span style="font-weight: 400">
          <font face="Kruti Dev 010"> 
                    <font color="#000000">विधायक निवासों में भेजन/नाश्ते की दर 
          का निर्धारण शासनादेश शसनादेश&nbsp; 
                    <br>
                        </font>
                        <a href="Awas/21.htm">संख्या 
          एम-6031ए/32-3-2005-57-81 दिनांक-07.09.2005</a></font></span></td>
                </tr>
                <tr valign="top"> 
                  <td width="7%" class="style2"> 
                    <div align="center">
                      </div>                  </td>
                  <td width="80%" class="style2" align="justify">
          <font face="Kruti Dev 010">
                  <span style="font-weight: 400">
                  <font color="#000000">आवास आवंटन के लिए प्रतिक्षा सूची के लिए 
          आवेदन पत्र का प्रारूप।<br>
                    d- </font> <font color="#FF0000">
          <a href="../AwaskaAwantan/Niyantran/Sarkari_Awantan4.htm">
          चतुर्थ श्रेणी कर्मचारी हेतु</a></font><font color="#0000FF"><br>
          [k- </font><font color="#FF0000">
          <a href="../AwaskaAwantan/Niyantran/Sarkari_Awantan3.htm">
          तृतीय श्रेणी कर्मचारि हेतु</a></font><b><font color="#0000FF"><br>
          x-</font></b><font color="#0000FF"> </font> 
          <a href="../AwaskaAwantan/Niyantran/Sarkari_Awantan_Raj.htm">
                  राजपत्रित अधिकारी हेतु</a><font color="#0000FF"><br>
                    <b>?k-</b> </font> 
          <a href="../AwaskaAwantan/Niyantran/Sarkari_Awantanall.htm">
                  सरकारी अधिकारी/कर्मचारियों के आवास 
          परिर्वतन हेतु </a></span></font></td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>