<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">राज्य संपत्ति विभाग अनुभाग - 3</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000" height="155">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="210" align="center"><b>पदनाम </b></td>
                  <td width="110" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="102" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="28" nowrap="">
  श्री गुरु प्रसाद तिवारी</td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="28" nowrap="">
  <p><span class="style131">अनुभाग अधिकारी<span style="mso-bidi-font-size:16.0pt;color:black"> </span>
  </span>अनुभाग - 3 <o:p></o:p></p>
  </td>
  <td style="width:99px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="28" nowrap="">
  <p align="center">2213439, 2238385 (fax)</p></td>
  <td style="width:91px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="28" nowrap="">
          &nbsp;9454411527</td>
            </tr>
            <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p class="MsoNormal"><span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री 
  रामचन्द्र </font></span> </span></p>
  </td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p class="MsoNormal"><span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">समीक्षा 
  अधिकारी </font></span> </span></p>
  </td>
  <td style="width:99px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p align="center">2213439</p></td>
  <td style="width:91px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p><font color="#000000"><span style="font-weight: 400">9454419393</span></font></p>
  </td>
            </tr>
            <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री विनय 
  कुमार चटर्जी </font></span></span>
  </td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">समीक्षा 
  अधिकारी </font></span> </span>
  </td>
  <td style="width:99px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p align="center">2213439</p></td>
  <td style="width:91px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  &nbsp;</td>
            </tr>
            <tr>
          <td>&nbsp;श्रीमती ताज रिज़वी </td>
          <td>&nbsp;प्रधान सहायक </td>
          <td align="center">
                    <font face="Times New Roman" style="font-size: 13pt">2213439</font></td>
          <td width="102" align="center">
                    9415195351</td>
        </tr>
            <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री अंबरीष 
  कुमार श्रीवास्तव </font></span></span>
  </td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">लेखा लिपिक 
  @ मोटर सहायक </font></span></span>
  </td>
  <td style="width:99px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p align="center">2213439</p></td>
  <td style="width:91px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  &nbsp;</td>
            </tr>
      <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री कृष्ण 
  कुमार वर्मा</font></span></span></td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">कंप्यूटर 
  ऑपेरटर </font></span></span>
  </td>
  <td style="width:99px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p align="center">2213439</p></td>
  <td style="width:91px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  &nbsp;</td>
            </tr>
      <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री लोक 
  नाथ </font></span></span>
  </td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">वरिष्ठ 
  सहायक </font></span></span>
  </td>
  <td style="width:99px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p align="center">2213439</p></td>
  <td style="width:91px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  &nbsp;</td>
            </tr>
      <tr>
                  <td width="25%" align="left">श्रीमती आरती शर्मा </td>
                  <td width="184" align="left">कनिष्ठ सहायक </td>
                  <td width="110" align="center"> 
                    2213439</td>
                  <td width="102" align="center">9454421023</td>
                </tr>
      <tr>
                  <td width="25%" align="left">श्री विनोद कुमार मिश्रा </td>
                  <td width="184" align="left">कनिष्ठ सहायक </td>
                  <td width="110" align="center">2213439</td>
                  <td width="102" align="center">9454421052</td>
                </tr>
      <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  श्री मती पूनम श्रीवास्तव
  </td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  कनिष्ठ सहायक </td>
  <td style="width:99px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p align="center">2213439</p></td>
  <td style="width:91px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  &nbsp;</td>
            </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>