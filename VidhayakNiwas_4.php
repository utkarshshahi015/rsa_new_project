<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">विधायक निवास - ४ रोयल होटल</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="180" align="center"><b>नाम </b></td>
                  <td width="195" align="center"><b>पदनाम </b></td>
                  <td width="120" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="109" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
                  <td width="180" align="center">श्री रजा&nbsp;आब्दी</td>
                  <td width="195" align="center">प्रभारी </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9415544026<br>
          9454421107</td>
                </tr>
            <tr>
                  <td width="180" align="center">श्री विनोद कुमार मिश्र </td>
                  <td width="195" align="center">ज्येष्ठ लिपिक </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421196</td>
                </tr>
            <tr>
                  <td width="180" align="center">श्री विनोद प्रसाद </td>
                  <td width="195" align="center">&nbsp;कंप्यूटर ऑपेरटर </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421022</td>
                </tr>
            <tr>
                  <td width="180" align="center">सुश्री कनक सक्सेना </td>
                  <td width="195" align="center">कनिष्ठ सहायक </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421114</td>
                </tr>
            <tr>
                  <td width="180" align="center">श्री अविनाश यादव </td>
                  <td width="195" align="center">सहायक स्टोर कीपर </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421118</td>
                </tr>
            <tr>
                  <td width="180" align="center">श्री गोरखनाथ यादव </td>
                  <td width="195" align="center">टेलीफ़ोन ऑपरेटर </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421116</td>
                </tr>
            <tr>
                  <td width="180" align="center">श्री दिनेश बहादुर सिंह </td>
                  <td width="195" align="center">कंप्यूटर ऑपेरटर </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421115</td>
                </tr>
            <tr>
                  <td width="180" align="center" height="24">श्री अशोक कुमार 
          पाण्डेय </td>
                  <td width="195" align="center" height="24">&nbsp;वरिष्ठ स्टोर 
          कीपर </td>
                  <td width="120" align="center" height="24">2236016</td>
                  <td width="109" align="center" height="24">9454421109</td>
                </tr>
            <tr>
                  <td width="180" align="center">श्री वेद प्रकाश यादव </td>
                  <td width="195" align="center">सहायक स्टोर कीपर </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421119</td>
                </tr>
            <tr>
                  <td width="180" align="center">सुश्री दीपापन्त </td>
                  <td width="195" align="center">मुख्यस्वागति </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421062</td>
                </tr>
            <tr>
                  <td width="180" align="center">सुश्री संगीता स्रोतिया </td>
                  <td width="195" align="center">कनिष्ठ सहायक </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421113 </td>
                </tr>
            <tr>
                  <td width="180" align="center">श्री मती नसीम बानो </td>
                  <td width="195" align="center">कंप्यूटर ऑपेरटर </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421098</td>
                </tr>
            <tr>
                  <td width="180" align="center">श्री हरपाल सिंह </td>
                  <td width="195" align="center">टेलीफ़ोन ऑपरेटर </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421117</td>
                </tr>
            <tr>
                  <td width="180" align="center">श्री राजेश सिंह</td>
                  <td width="195" align="center">कनिष्ठ सहायक </td>
                  <td width="120" align="center">2236016</td>
                  <td width="109" align="center">9454421027</td>
                </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>