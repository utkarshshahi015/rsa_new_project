<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">सहायक राज्य संपत्ति अधिकारी कैंप कार्यालय</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000" height="31">
            <tbody><tr>
                  <td width="164" align="center"><b>नाम </b></td>
                  <td width="220" align="center"><b>पदनाम </b></td>
                  <td width="134" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="21%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
                  <td width="164" align="center">श्री अशोक कुमार सिंह </td>
                  <td width="220" align="center">संयुक्त सचिव </td>
                  <td width="134" align="center">2213573<br>
          2238231</td>
                  <td width="21%" align="center">9454411422</td>
                </tr>
            <tr>
                  <td width="164" align="center">श्री उमेश चतुर्वेदी </td>
                  <td width="220" align="center">मुख्य कंप्यूटर ऑपेरटर </td>
                  <td width="134" align="center">2213573</td>
                  <td width="21%" align="center">9454421044</td>
                </tr>
            <tr>
                  <td width="164" align="center">श्रीमती प्रीती मौर्या </td>
                  <td width="220" align="center">काउंटर क्लर्क कम स्टोर कीपर </td>
                  <td width="134" align="center">2213573</td>
                  <td width="21%" align="center">9454421017</td>
                </tr>
            <tr>
                  <td width="164" align="center">श्री एहशान उल्लाह </td>
                  <td width="220" align="center">अनुसेवक </td>
                  <td width="134" align="center">2213573</td>
                  <td width="21%" align="center">9389016523</td>
                </tr>
            <tr>
                  <td width="164" align="center">श्री केशव देव यादव </td>
                  <td width="220" align="center">चौकीदार </td>
                  <td width="134" align="center">2213573</td>
                  <td width="21%" align="center">9453843991</td>
                </tr>
            <tr>
                  <td width="164" align="center">श्री हेमंत कुमार त्रिपाठी </td>
                  <td width="220" align="center">चपरासी </td>
                  <td width="134" align="center">2213573</td>
                  <td width="21%" align="center">9451115094</td>
                </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>