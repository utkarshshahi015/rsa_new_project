<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">विधायक निवास - 5</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000" height="40">
            <tbody><tr>
                  <td width="156" align="center"><b>नाम </b></td>
                  <td width="230" align="center"><b>पदनाम </b></td>
                  <td width="112" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="103" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
                  <td width="156" align="left">श्रीमती किरन सिंह </td>
                  <td width="230" align="left">प्रभारी </td>
                  <td width="112" align="center">2205220</td>
                  <td width="103" align="center">9454421183</td>
                </tr>
            <tr>
                  <td width="156" align="left">श्रीमती प्रतिमा सिंह </td>
                  <td width="230" align="left">कार्यालय क्लर्क कम स्टोर कीपर</td>
                  <td width="112" align="center">2205220</td>
                  <td width="103" align="center">8004843644</td>
                </tr>
            <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="" align="left">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री 
  राजेंद्र प्रसाद </font></span></span>
  </td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="" align="left">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">स्टोर कीपर </font></span></span>
  </td>
  <td style="width:98px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p align="center">2205220</p></td>
  <td style="width:80px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  -</td>
            </tr>
            <tr>
                  <td width="156" align="left">मो0 रईस </td>
                  <td width="230" align="left">कंप्यूटर ऑपरेटर. </td>
                  <td width="112" align="center">2205220</td>
                  <td width="103" align="center">9454421124</td>
                </tr>
            <tr>
                  <td width="156" align="left">श्री सालिगराम </td>
                  <td width="230" align="left">कंप्यूटर ऑपरेटर. </td>
                  <td width="112" align="center">2205220</td>
                  <td width="103" align="center">9454421121</td>
                </tr>
            <tr>
                  <td width="156" align="left">श्री घनश्याम यादव </td>
                  <td width="230" align="left">कंप्यूटर ऑपरेटर. </td>
                  <td width="112" align="center">2205220</td>
                  <td width="103" align="center">9454421123</td>
                </tr>
            <tr>
                  <td width="156" align="left">श्री कमलेश कुमार </td>
                  <td width="230" align="left">कनिष्ठ सहायक </td>
                  <td width="112" align="center">2205220</td>
                  <td width="103" align="center">8423632777</td>
                </tr>
            <tr>
                  <td width="156" align="left">श्री आरिफ़ हुसैन</td>
                  <td width="230" align="left">कनिष्ठ स्वागती</td>
                  <td width="112" align="center">2205220</td>
                  <td width="103" align="center">9454421131</td>
                </tr>
            <tr>
                  <td width="156" align="left">श्री धनुषधारी </td>
                  <td width="230" align="left">चौकीदार </td>
                  <td width="112" align="center">2205220</td>
                  <td width="103" align="center">&nbsp;</td>
                </tr>
            <tr>
                  <td width="156" align="left">श्री बनवारी लाल</td>
                  <td width="230" align="left">सफाईकर्मी </td>
                  <td width="112" align="center">2205220</td>
                  <td width="103" align="center">&nbsp;</td>
                </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>