<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">राज्य संपत्ति विभाग अनुभाग - 2</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000" height="15">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="184" align="center"><b>पदनाम </b></td>
                  <td width="110" align="center"><b>कार्यालय नंबर</b></td>
                  <td width="101" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्री महेन्द्र कुमार वर्मा </td>
                  <td width="184" align="left">अनुभाग अधिकारी </td>
                  <td width="110" align="center">2213457</td>
                  <td width="101" align="center">9454413990</td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्रीमती निधि गंगवार, </td>
                  <td width="184" align="left">समीक्षा अधिकारी </td>
                  <td width="110" align="center">2213457</td>
                  <td width="101" align="center">9454419939</td>
                </tr>
            <tr>
                  <td width="25%" align="left">सुश्री मंजू वर्मा </td>
                  <td width="184" align="left">समीक्षा अधिकारी </td>
                  <td width="110" align="center">2213457</td>
                  <td width="101" align="center">9454419907</td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्री नीरज सिंह कार्की </td>
                  <td width="184" align="left">कनिष्ठ सहायक </td>
                  <td width="110" align="center">2213457</td>
                  <td width="101" align="center">9454421162</td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्री बृजेन्द्र यादव </td>
                  <td width="184" align="left">पूछताछ लिपिक </td>
                  <td width="110" align="center">2213457</td>
                  <td width="101" align="center">9454421153</td>
                </tr>
            <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="" align="left">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री कादिर 
  बख्श </font></span></span>
  </td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="" align="left">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">कंप्यूटर 
  ऑपेरटर </font></span></span>
  </td>
  <td style="width:98px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p align="center">2213457</p></td>
  <td style="width:80px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  &nbsp;</td>
            </tr>
      <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री कमलेश 
  कुमार </font></span></span>
  </td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">कंप्यूटर 
  ऑपेरटर </font></span></span>
  </td>
  <td style="width:98px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p align="center">2213457</p></td>
  <td style="width:80px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  &nbsp;</td>
            </tr>
            <tr>
                  <td width="25%" align="center">&nbsp;</td>
                  <td width="184" align="center">&nbsp;</td>
                  <td width="110" align="center">&nbsp;</td>
                  <td width="101" align="center">&nbsp;</td>
                </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>