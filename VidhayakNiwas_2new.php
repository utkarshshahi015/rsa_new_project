<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-9">
            <div class="tab-content" id="myTabContent">
              <h2>विधायक निवास - 2 नया</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000" height="1">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="193" align="center"><b>पदनाम </b></td>
                  <td width="134" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="21%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
  <td width="162" nowrap="" valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:121.35pt;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:1; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          श्रीमती रश्मि खरे</td>
  <td width="203" nowrap="" valign="top" style="width:152.45pt;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:1; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  कार्यवाहक व्यवस्था अधिकारी</td>
  <td width="121" valign="top" style="width:90.75pt;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:1" align="center">
  2629753</td>
  <td width="121" valign="top" style="width:90.75pt;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:1">
  <p class="MsoNormal" align="center">
  <span style="font-family: Times New Roman" lang="en-us"><font size="3">
  9415549778</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font> </p>
  </td>
            </tr>
            <tr>
  <td width="162" nowrap="" valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:121.35pt;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:22; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  हरीशंकर शुक्ला
  </td>
  <td width="203" nowrap="" valign="top" style="width:152.45pt;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:22; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  कनिष्ठ सहायक
  </td>
  <td width="121" valign="top" style="width:90.75pt;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:22" align="center">
  2629753</td>
  <td width="121" valign="top" style="width:90.75pt;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:22" align="center">
  <p class="MsoNormal"><span style="font-family: Times New Roman">9454421084</span><font face="Times New Roman" style="font-size: 13pt">
                    </font> </p>
  </td>
            </tr>
            <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री चंद्र 
  प्रकाश राम</font></span></span></td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">&nbsp;स्टोर 
  कीपर </font></span></span>
  </td>
  <td style="width:98px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p align="center">2629753</p></td>
  <td style="width:80px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <font color="#000000"><span style="font-weight: 400">9839924412</span></font><span style="font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></td>
            </tr>
            <tr>
  <td width="162" nowrap="" valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:121.35pt;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:19; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  रोहन यादव
  </td>
  <td width="203" nowrap="" valign="top" style="width:152.45pt;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:19; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  सहायक स्टोर कीपर
  </td>
  <td width="121" valign="top" style="width:90.75pt;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:19" align="center">
  2629753</td>
  <td width="121" valign="top" style="width:90.75pt;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:19" align="center">
  <p class="MsoNormal"><span style="font-family: Times New Roman">9454421087</span><font face="Times New Roman" style="font-size: 13pt">
                    </font> </p>
  </td>
            </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>