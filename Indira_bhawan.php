<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">इंदिरा भवन</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="193" align="center"><b>पदनाम </b></td>
                  <td width="134" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="21%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  श्री श्रद्धानन्द चौधरी
  </td>
  <td width="132" nowrap="" valign="top" style="width:99.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.75pt .75pt 0in .75pt;
  height:16.5pt">
  व्यवस्थाधिकारी
  </td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  2287223</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  <p>9335281647&nbsp;<o:p></o:p></p>
  </td>
            </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  <p class="MsoNormal">श्री रविन्द्र कुमार सिंह</p>
  </td>
  <td width="132" nowrap="" valign="top" style="width:99.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.75pt .75pt 0in .75pt;
  height:16.5pt">
  व्यवस्थापक </td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  2287223</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  &nbsp;-</td>
            </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  <p class="MsoNormal">ब्रजराज सिंह</p>
  </td>
  <td width="132" nowrap="" valign="top" style="width:99.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.75pt .75pt 0in .75pt;
  height:16.5pt">
  <p class="MsoNormal">स्टोर कीपर</p>
  </td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  2287223</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  <p>&nbsp;9235858698</p>
  </td>
            </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  <p class="MsoNormal">श्रीमती शीला सिंह </p>
  </td>
  <td width="132" nowrap="" valign="top" style="width:99.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.75pt .75pt 0in .75pt;
  height:16.5pt">
  सहायक स्टोर कीपर
  </td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  2287223</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  <p>&nbsp;9415915008</p>
  </td>
            </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  <p class="MsoNormal">श्री विजय कुमार </p>
  </td>
  <td width="132" nowrap="" valign="top" style="width:99.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.75pt .75pt 0in .75pt;
  height:16.5pt">
  <p class="MsoNormal">कनिष्ठ लिपिक </p>
  </td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  2287223</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  <p>&nbsp;9335784042</p>
  </td>
            </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  <p class="MsoNormal">विपिन सिंह </p>
  </td>
  <td width="132" nowrap="" valign="top" style="width:99.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.75pt .75pt 0in .75pt;
  height:16.5pt">
  <p class="MsoNormal">टेलीफोन ऑपरेटर</p>
  </td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  2287223</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  <p>&nbsp;9044321170</p>
  </td>
            </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  <p class="MsoNormal">शशिभूषण तिवारी </p>
  </td>
  <td width="132" nowrap="" valign="top" style="width:99.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.75pt .75pt 0in .75pt;
  height:16.5pt">
  कनिष्ठ सहायक
  </td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  2287223</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  <p>&nbsp;8115176451</p>
  </td>
            </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>