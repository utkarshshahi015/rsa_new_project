<!-- <!DOCTYPE html> -->
<?php include 'header.php';?>
<html lang="en">
      <!--/#page-head-->
    
      <!--/#paragraph-->
      <section id="list" class="wrapper list-wrapper">
       <body>
        <h1>AAN Number Login</h1>
        <form action="">
            <!-- Headings for the form -->
            <div class="headingsContainer">
                <h3>Sign in</h3>
                <p>Sign in with your username and password</p>
            </div>
    
            <!-- Main container for all inputs -->
            <div class="mainContainer">
                <!-- Username -->
                <label for="username">Your username</label>
                <input type="text" placeholder="Enter Username" name="username" required>
    
                <br><br>
    
                <!-- Password -->
                <label for="pswrd">Your password</label>
                <input type="password" placeholder="Enter Password" name="pswrd" required>  
                <!-- sub container for the checkbox and forgot password link -->
                <div class="subcontainer">
                    <label>
                      <input type="checkbox" checked="checked" name="remember"> Remember me
                    </label>
                    <p class="forgotpsd"> <a href="#">Forgot Password?</a></p>
                </div>
    
    
                <!-- Submit button -->
                <button type="submit">Login</button>
    
                <!-- Sign up link -->
                <p class="register">Not a member?  <a href="#">Register here!</a></p>
    
            </div>
    
        </form>
       </body>
      </section>



      <style>
        body 
{
  font-family:sans-serif; 
  background: -webkit-linear-gradient(to right, #158959, #158957);  
  background: linear-gradient(to right, #158959, #158957); 
  color:whitesmoke;
}

h1{
    text-align: center;
}

form{
    width:35rem;
    margin: auto;
    color:whitesmoke;
    backdrop-filter: blur(16px) saturate(180%);
    -webkit-backdrop-filter: blur(16px) saturate(180%);
    background-color: rgba(11, 15, 13, 0.582);
    border-radius: 12px;
    border: 1px solid rgba(255, 255, 255, 0.125);
    padding: 20px 25px;
}

input[type=text], input[type=password]{
    width: 100%;
    margin: 10px 0;
    border-radius: 5px;
    padding: 15px 18px;
    box-sizing: border-box;
  }

button {
    background-color: #030804;
    color: white;
    padding: 14px 20px;
    border-radius: 5px;
    margin: 7px 0;
    width: 100%;
    font-size: 18px;
  }

button:hover {
    opacity: 0.6;
    cursor: pointer;
}

.headingsContainer{
    text-align: center;
}

.headingsContainer p{
    color: gray;
}
.mainContainer{
    padding: 16px;
}


.subcontainer{
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
}

.subcontainer a{
    font-size: 16px;
    margin-bottom: 12px;
}

span.forgotpsd a {
    float: right;
    color: whitesmoke;
    padding-top: 16px;
  }

.forgotpsd a{
    color: rgb(74, 146, 235);
  }
  
.forgotpsd a:link{
    text-decoration: none;
  }

  .register{
    color: white;
    text-align: center;
  }
  
  .register a{
    color: rgb(74, 146, 235);
  }
  
  .register a:link{
    text-decoration: none;
  }

  /* Media queries for the responsiveness of the page */
  @media screen and (max-width: 600px) {
    form{
      width: 25rem;
    }
  }
  
  @media screen and (max-width: 400px) {
    form{
      width: 20rem;
    }
  }
  </style>
   
      <section class="wrapper carousel-wrapper">
         <div class="container common-container four_content carousel-container">
            <div id="flexCarousel" class="flexslider carousel">
               <ul class="slides">
                  <li><a target="_blank" href="http://digitalindia.gov.in/" title="Digital India, External Link that opens in a new window"><img src="assets/images/carousel/digital-india.png" alt="Digital India"></a></li>
                  <li><a target="_blank" href="http://www.makeinindia.com/" title="Make In India, External Link that opens in a new window"> <img src="assets/images/carousel/makeinindia.png" alt="Make In India"></a></li>
                  <li><a target="_blank" href="http://india.gov.in/" title="National Portal of India, External Link that opens in a new window"><img src="assets/images/carousel/india-gov.png" alt="National Portal of India"></a></li>
                  <li><a target="_blank" href="http://goidirectory.nic.in/" title="GOI Web Directory, External Link that opens in a new window"><img src="assets/images/carousel/goidirectory.png" alt="GOI Web Directory"></a></li>
                  <li><a target="_blank" href="https://data.gov.in/" title="Data portal, External Link that opens in a new window" ><img src="assets/images/carousel/data-gov.png" alt="Data portal"></a></li>
                  <li><a target="_blank" href="https://mygov.in/" title="MyGov, External Link that opens in a new window"><img src="assets/images/carousel/mygov.png" alt="MyGov Portal"></a></li>
               </ul>
            </div>
         </div>
      </section>
      <!--/.carousel-wrapper-->
 
      <?php include 'footer.php';?>
    
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js" integrity="sha512-nhY06wKras39lb9lRO76J4397CH1XpRSLfLJSftTeo3+q2vP7PaebILH9TqH+GRpnOhfAGjuYMVmVTOZJ+682w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
     <!-- jQuery Migration v1.4.1 -->
     <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
     <!-- jQuery v3.6.0 -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
     <!-- jQuery Migration v3.4.0 -->
     <script src="https://code.jquery.com/jquery-migrate-3.4.0.min.js"></script>
     
     <script src="assets/js/jquery-accessibleMegaMenu.js"></script>
     <script src="assets/js/framework.js"></script>
     <script src="assets/js/jquery.flexslider.js"></script>
     <script src="assets/js/font-size.js"></script>
     <script src="assets/js/swithcer.js"></script>
     <script src="theme/js/ma5gallery.js"></script>
     <script src="assets/js/megamenu.js"></script>
     <script src="theme/js/easyResponsiveTabs.js"></script>
     <script src="theme/js/custom.js"></script>
  </body>
</html>