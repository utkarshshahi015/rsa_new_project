<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">राज्य अतिथि गृह विक्रमादित्य मार्ग</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000" height="15">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="184" align="center"><b>पदनाम </b></td>
                  <td width="110" align="center"><b>कार्यालय नंबर</b></td>
                  <td width="101" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री महेंद्र गुप्ता </td>
                  <td width="193" align="center">कार्यवाहक व्यवस्थाधिकारी </td>
                  <td width="110" align="center">2235213</td>
                  <td width="101" align="center">9454421185</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री फ़िरोज़ अहमद </td>
                  <td width="193" align="center">स्टोर कीपर </td>
                  <td width="110" align="center">2235213</td>
                  <td width="101" align="center">9454421157</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री भगवत सिंह </td>
                  <td width="193" align="center">लेखाकार </td>
                  <td width="110" align="center">2235213</td>
                  <td width="101" align="center">9454421169</td>
                </tr>
            <tr>
              <td height="18" align="center">श्री मुकेश कुमार </td>
                  <td height="18" align="center">सहायक स्टोर कीपर </td>
                  <td height="18" align="center">2235213</td>
                  <td height="18"><p align="center">
          <font face="Times New Roman">9936245335</font></p></td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री राम नरेश पाल </td>
                  <td width="193" align="center">टेलीफोन ऑपरेटर </td>
                  <td width="110" align="center">2235213</td>
                  <td width="101" align="center">9454421160</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री वेद प्रकाश शुक्ला </td>
                  <td width="193" align="center">टेलीफोन ऑपरेटर </td>
                  <td width="110" align="center">2235213</td>
                  <td width="101" align="center">9454421159</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री सुमित सिंह </td>
                  <td width="193" align="center">टेलीफोन ऑपरेटर </td>
                  <td width="110" align="center">2235213</td>
                  <td width="101" align="center">9454421093</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री सौरभ पांडेय </td>
                  <td width="193" align="center">कनिष्ठ सहायक </td>
                  <td width="110" align="center">2235213</td>
                  <td width="101" align="center">9454421088</td>
                </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>