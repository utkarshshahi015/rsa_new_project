<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <meta name="format-detection" content="telephone=no" />
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="apple-touch-icon" href="assets/images/favicon/apple-touch-icon.png">
      <link rel="icon" href="assets/images/favicon/favicon.png">
      <title>Home |  Ministry | Department | GoI</title>
      <!-- Custom styles for this template -->
      <link href="assets/css/base.css" rel="stylesheet" media="all">
      <link href="assets/css/base-responsive.css" rel="stylesheet" media="all">
      <link href="assets/css/grid.css" rel="stylesheet" media="all">
      <link href="assets/css/font.css" rel="stylesheet" media="all">
      <link href="assets/css/font-awesome.min.css" rel="stylesheet" media="all">
      <link href="assets/css/flexslider.css" rel="stylesheet" media="all">
      <link href="assets/css/print.css" rel="stylesheet" media="print" />
      <link href="assets/css/modules.css" rel="stylesheet" media="all">
      <!-- Theme styles for this template -->
      <link href="assets/css/megamenu.css" rel="stylesheet" media="all" />
      <link href="theme/css/site.css" rel="stylesheet" media="all">
      <link href="theme/css/site-responsive.css" rel="stylesheet" media="all">
      <link href="theme/css/ma5gallery.css" rel="stylesheet" type="text/css">
      <!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
         <header>
            <h1 style="display: none;">Header</h1>
            <div class="region region-header-top">
               <div id="block-cmf-content-header-region-block" class="block block-cmf-content first last odd">
                  <noscript class="no_scr">"JzavaScript is a standard programming language that is included to provide interactive features, Kindly enable Javascript in your browser. For details visit help page"
                  </noscript>
                  <div class="wrapper common-wrapper">
                     <div class="container common-container four_content top-header">
                        <div class="common-left clearfix">
                           <ul>
                              <li class="gov-india"><span class="responsive_go_hindi" lang="hi"><a target="_blank" href="https://india.gov.in/hi" title="भारत सरकार ( बाहरी वेबसाइट जो एक नई विंडो में खुलती है)" role="link">भारत सरकार</a></span></li>
                              <li class="ministry"><span class="li_eng responsive_go_eng"><a target="_blank" href="https://india.gov.in/" title="Government of india,External Link that opens in a new window" role="link">Government of india</a></span></li>
                           </ul>
                        </div>
                        <div class="common-right clearfix">
                           <ul id="header-nav">
                              <li class="ico-skip cf"><a href="#skipCont" title="">Skip to main content</a>
                              </li>
                              <li class="ico-site-search cf">
                                 <a href="javascript:void(0);" id="toggleSearch" title="Site Search">
                                 <img class="top" src="assets/images/ico-site-search.png" alt="Site Search" /></a>
                                 <div class="search-drop both-search">
                                    <div class="google-find">
                                       <form method="get" action="http://www.google.com/search" target="_blank">
                                          <label for="search_key_g" class="notdisplay">Search</label>
                                          <input type="text" name="q" value="" id="search_key_g"/> 
                                          <input type="submit" value="Search" class="submit" /> 
                                          <div >
                                             <input type="radio" name="sitesearch" value="" id="the_web"/> 
                                             <label for="the_web">The Web</label> 
                                             <input type="radio" name="sitesearch" value="india.gov.in" checked id="the_domain"/> <label for="the_domain"> INDIA.GOV.IN</label>
                                          </div>
                                       </form>
                                    </div>
                                    <div class="find">
                                       <form name="searchForm" action="">
                                          <label for="search_key" class="notdisplay">Search</label>
                                          <input type="text" name="search_key" id="search_key" onKeyUp="autoComplete()" autocomplete="off" required />
                                          <input type="submit" value="Search" class="bttn-search"/>
                                       </form>
                                       <div id="auto_suggesion"></div>
                                    </div>
                                 </div>
                              </li>
                              <li class="ico-accessibility cf">
                                 <a href="javascript:void(0);" id="toggleAccessibility" title="Accessibility Dropdown" role="link">
                                 <img class="top" src="assets/images/ico-accessibility.png" alt="Accessibility Dropdown" />
                                 </a>
                                 <ul style="visibility: hidden;">
                                    <li> <a onClick="set_font_size(&#39;increase&#39;)" title="Increase font size" href="javascript:void(0);" role="link">A<sup>+</sup>
                                       </a> 
                                    </li>
                                    <li> <a onClick="set_font_size()" title="Reset font size" href="javascript:void(0);" role="link">A<sup>&nbsp;</sup></a> </li>
                                    <li> <a onClick="set_font_size(&#39;decrease&#39;)" title="Decrease font size" href="javascript:void(0);" role="link">A<sup>-</sup></a> </li>
                                    <li> <a href="javascript:void(0);" class="high-contrast dark" title="High Contrast" role="link">A</a> </li>
                                    <li> <a href="javascript:void(0);" class="high-contrast light" title="Normal Contrast" style="display: none;" role="link">A</a> </li>
                                 </ul>
                              </li>
                              <li class="ico-social cf">
                                 <a href="javascript:void(0);" id="toggleSocial" title="Social Medias">
                                 <img class="top" src="assets/images/ico-social.png" alt="Social Medias" /></a>
                                 <ul>
                                    <li>
                                       <a target="_blank" title="External Link that opens in a new window" href="http://www.facebook.com/">
                                       <img alt="Facebook Page" src="assets/images/ico-facebook.png">
                                       </a>
                                    </li>
                                    <li>
                                       <a target="_blank" title="External Link that opens in a new window" href="http://www.twitter.com/">
                                       <img alt="Twitter Page" src="assets/images/ico-twitter.png">
                                       </a>
                                    </li>
                                    <li>
                                       <a target="_blank" title="External Link that opens in a new window" href="http://www.youtube.com/">
                                       <img alt="youtube Page" src="assets/images/ico-youtube.png">
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li class="ico-sitemap cf"><a href="" title="Sitemap">
                                 <img class="top" src="assets/images/ico-sitemap.png" alt="Sitemap" /></a>
                              </li>
                              <li class="hindi cmf_lan d-hide">
                                 <label class="de-lag">
                                    <span>Language</span>
                                    <select title="Select Language">
                                       <option>English</option>
                                       <option>हिन्दी</option>
                                    </select>
                                 </label>
                              </li>
                              <li class="hindi cmf_lan m-hide">
                                 <a href="javascript:;" title="Select Language">Language</a> 
                                 <ul>
                                    <li><a target="_blank" href="" lang="hi" class="alink" title="Click here for हिन्दी version.">हिन्दी</a></li>
                                 </ul>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <p id="scroll" style="display: none;"><span></span></p>
            </div>
            <!--Top-Header Section end-->
            <section class="wrapper header-wrapper">
                <div class="container common-container four_content  header-container">
                   <h2 class="logo">
                      <a href="home.html" title="Home" rel="home" class="header__logo" id="logo">
                         <img class="national_emblem" src="assets/images/up-logo-hi.png" alt="national emblem" >
                         <p><span>राज्य संपत्ति विभाग  </span>                       
                            <span> उत्तर प्रदेश</span>
                         </p>
                      </a>
                   </h2>
                   <!-- <div class="header-right clearfix">
                      <div class="right-content clearfix">
                         <div class="float-element" style="margin-left:60px;">
                      <button type="button" class="btn btn-primary"><a href="https://estate-up.gov.in/rsav2/applicant" style="color: #fff;">आवेदक लॉगिन</a> </button>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <button type="button" class="btn btn-primary"><a href="https://estate-up.gov.in/rsav2/applicant-register" style="color: #fff;">सरकारी आवास हेतु आवेदन</a></button>
                         </div>
                      </div>
                   </div> -->
                   &nbsp;&nbsp;&nbsp;&nbsp;
                   <div class="header-right clearfix">
                      <div class="right-content clearfix">
                          <div class="float-element">
                              <a class="sw-logo1" target="_blank" href="https://www.skillindia.gov.in/" title="Skill India, External link that open in a new windows">
                                  <img src="assets/images/g20.png" alt="Skill India">
                              </a>
                              <a class="sw-logo" target="_blank" href="https://swachhbharat.mygov.in/" title="Swachh Bharat, External link that open in a new windows">
                                  <img src="assets/images/swach-bharat.png" alt="Swachh Bharat">
                              </a>
                              <a class="sw-logo" target="_blank" href="https://swachhbharat.mygov.in/" title="Swachh Bharat, External link that open in a new windows">
                                  <img src="assets/images/emblem-dark.png" alt="Swachh Bharat">
                              </a>
                             
                          </div> 
                      </div>
                   </div>
              <!-- <div class="header-right clearfix">
                      <div class="right-content clearfix">
                         <div class="float-element" style="margin-left:60px;">
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <button type="button" class="btn btn-primary"><a href="https://estate-up.gov.in/rsav2/applicant-register" style="color: #fff;">सरकारी आवास हेतु आवेदन</a></button>
                         </div>
                      </div>
                   </div> -->
                 
                </div>
             </section>
             <!--/.header-wrapper-->
             <section class="wrapper megamenu-wraper">
                <div class="container common-container four_content">
                   <p class="showhide"><em></em><em></em><em></em></p>
                   <nav class="main-menu clearfix" id="main_menu" style="">
                      <ul class="nav-menu">
                         <li class="nav-item"> <a href="home.html" class="home"><i class="fa fa-home"></i></a> </li>
                         <li class="nav-item"> <a href="About.html">हमारे बारे में</a></li>
    
                         <li class="nav-item"> <a href="http://estate-up.gov.in/RajayaSampatti/">विभाग के कार्यकलाप</a></li>
                
                         <li class="nav-item"> <a href="inner.html">  गैलरी </a>
                            <div class="sub-nav">
                            <ul class="sub-nav-group">
                               <li><a href="inner.html">फोटो गैलरी</a></li>
                               <li><a href="inner.html">वीडियो गैलरी</a></li>
                               <!-- <li><a href="inner.html">Circulars</a></li>
                               <li><a href="inner.html">Events</a></li> -->
                            </ul>
                            </div>
                         </li>
                
                         

                         <li class="nav-item"><a href="https://estate-up.gov.in/rsav2/house-allotment-links"> आवास का आवंटन</a>
                            <div class="sub-nav">
                            <ul class="sub-nav-group">
                               <li><a href="inner.html">Right to Information Act</a></li>
                               <li><a href="inner.html">Other Act And Rules</a></li>
                            </ul>
                            </div>
                         </li>
                         
                     
                         <!-- <li class="nav-item"><a href="https://estate-up.gov.in/rsav2/house-allotment-links">Allotment of Accommodation </a></li> -->
                
                         <li class="nav-item"><a href="https://ehrms.upsdc.gov.in/">  मानव सम्पदा पोर्टल</a></li>
    
                         
                         <li class="nav-item"><a href="https://estate-up.gov.in/RajayaSampatti/">आरटीआई </a></li>
                
                         <li class="nav-item"><a href="https://estate-up.gov.in/rsav2/contact-us">   संपर्क सूत्र</a></li>

                         <li class="nav-item"> <a href="http://estate-up.gov.in/RajayaSampatti/">विभाग के कार्यकलाप</a></li>


                            <li class="nav-item"><a> 
                            लॉगिन                
                         </a>
                            <div class="sub-nav">
                            <ul class="sub-nav-group">
                              <li><a href="https://estate-up.gov.in/nideshaalay/">निदेशालय-लॉगिन</a></li></label>  
                               <li><a href="https://estate-up.gov.in/rsav2/superadmin"> विभाग-लॉगिन</a></li>
                               <li><a href="https://estate-up.gov.in/rsav2/engineer">   इंजीनियर-लॉगिन</a></li>
                            </ul>
                            </div>
                         </li>
                
                      </ul>
                      </nav>
                   <nav class="main-menu clearfix" id="overflow_menu">
                      <ul class="nav-menu clearfix">
                      </ul>
                   </nav>
                </div>
                <style type="text/css">
                   body ~ .sub-nav {
                   right: 0
                   }
    
                </style>
             </section>
         </header>
      <!--/.nav-wrapper-->
      <section class="wrapper banner-wrapper">
         <div id="flexSlider" class="flexslider">
            <div class="inner-banner"><img src="theme/images/banner/inner-banner.jpg" alt="Inner Banner of Consumer Affairs"></div>
         </div>
      </section>
      <div class="wrapper" id="skipCont"></div>
      <!--/#skipCont-->
      <section id="fontSize" class="wrapper body-wrapper ">
      <div class="bg-wrapper inner-wrapper">
      <div class="breadcam-bg breadcam">
         <div class="container common-container four_content">
            <ul>
               <li><a href="home.html">Home </a></li>
               <li><a href="#">About</a></li>
               <li><a href="#">About Us</a></li>
            </ul>
         </div>
      </div>
      <!-- <section id="page-head" class="wrapper headings-wrapper">
         <div class="container common-container four_content">
      
            <h2><em>राज्य सम्पत्ति विभाग, उ0प्र0 शासन के प्रमुख कार्यकलाप</em></h2>
          
            <hr />
         </div>
      </section> -->
      <!--/#page-head-->
      <!-- <section id="paragraph" class="wrapper paragraph-wrapper">
         <div class="container common-container four_content">
               <p>राज्य सम्पत्ति विभाग द्वारा राज्य मुख्यालय पर मा0 मंत्रीगण, मा0 विधायकगण, लखनऊ में कार्यरत सरकारी अधिकारियों/कर्मचारियों की आवासीय व्यवस्था तथा लखनऊ में सरकारी कार्यालय भवन यथा विधान भवन, लाल बहादुर शास्त्री भवन (एनेक्सी), बापू भवन, योजना भवन तथा जवाहर भवन एवं इंदिरा भवन के रख-रखाव की व्यवस्था सुनिश्चित की जाती है। राज्य से दिल्ली, कोलकाता एवं मुम्बई जाने वाले मा0 मंत्रीगण, मा0 विधायकगण, अधिकारियों एवं अन्य महानुभावों के ठहरने हेतु अतिथि गृह अवस्थित हैं। इसी प्रकार मा0 संासदों तथा अधिकारियों एवं अन्य महानुभावों के लिए लखनऊ में अतिथि गृह अवस्थित हैं। इन अतिथि गृहों की प्रबन्धकीय तथा रख-रखाव की व्यवस्था भी विभाग द्वारा सम्पादित की जाती है। राज्य सरकार के मा0 मंत्रीगण हेतु वाहनों की व्यवस्था भी की जाती है। राज्य अतिथियों एवं भारत सरकार के मा0 मंत्रीगण के प्रदेश (लखनऊ) आगमन पर उनकी सुविधानुसार आवास एवं भोजन की व्यवस्था भी सुनिश्चित की जाती है।
                  </p>
             
            <hr />
            <div class="align-lt">
               <img src="assets/images/paragraph-img/img1.jpg" alt="img 1">
               <h3>Headling 2 goes here...</h3>
               <p>Description of the heading 2 goes here. An informative text section that outlines the work portfolio of the ministry and the initiatives/ schemes and other 
                     useful purpose that the ministry website serves. Description of the heading 2 goes here. An informative text section that outlines the work portfolio of the 
                     ministry and the initiatives/ schemes and other useful purpose that the ministry website serves. Description of the heading 2 goes here. An informative text 
                     section that outlines the work portfolio of the ministry and the initiatives/ schemes and other useful purpose that the ministry website serves.</p>
               <a href="#" class="more">View More</a> 
            </div>
            <hr />
            <div class=" align-rt">
               <img src="assets/images/paragraph-img/img1.jpg" alt="img 2">
               <h3>Headling 3 goes here...</h3>
               <p>Description of the heading 3 goes here. An informative text section that outlines the work portfolio of the ministry and the initiatives/ schemes and other 
                     useful purpose that the ministry website serves. Description of the heading 3 goes here. An informative text section that outlines the work portfolio of the 
                     ministry and the initiatives/ schemes and other useful purpose that the ministry website serves. </p>
               <p>Description of the heading  3 goes here. An informative text section that outlines the work portfolio of the ministry and the initiatives/ schemes and 
                  other useful purpose that the ministry website serves.</p>
               <a href="#" class="more">View More</a> 
            </div>
            <hr />
         </div>
      </section> -->
      <!--/#paragraph-->
      <section id="list"  class="wrapper news-section">
        <div class="wrapper home-btm-slider">
            <div class="container common-container four_content gallery-container">
               <div class="gallery-right">
                  <div class="video-heading">
                     <h3>Photo Gallery</h3>
                     <a clas  s="bttn-more bttn-view" href="#" alt="View all" title="View All About video">
                     <span>View All</span>
                     </a> 
                  </div>
                  <div class="galleryCarousel">
                     <img src="theme/images/crousal/imgrsa3.jpg" alt="gallery iamge"/>
                  </div>
               </div>
               <div class="gallery-area clearfix">
                  <!-- <div class="gallery-heading">
                     <h3>Photo Gallery</h3>
                     <a class="bttn-more bttn-view" href="" alt="View all" title="View All About Photo Gallery">
                     <span>View All</span>
                     </a> 
                  </div> -->
                  <div class="gallery-holder">
                     <div id="galleryCarousel" class="flexslider">
                        <ul class="slides">
                           <li data-thumb="theme/images/crousal/imagersa2.0.jpg" data-thumb-alt="slider1">
                              <img src="theme/images/crousal/imgrsa3.jpg" alt="gallery iamge"/>
                           </li>
                           <li data-thumb="theme/images/crousal/imagersa2.0.jpg" data-thumb-alt="slider2">
                              <img src="theme/images/crousal/imgrsa3.jpg" alt="gallery iamge"/>
                           </li>
                           <li data-thumb="theme/images/crousal/imagersa2.0.jpg" data-thumb-alt="slider3">
                              <img src="theme/images/crousal/imgrsa3.jpg" alt="gallery iamge"/>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--/#list-->
      <!--/#article-->
      <!-- <section id="tables" class="wrapper tables-wrapper">
         <div class="container common-container four_content">
            <div class="scroller">
               <table cellspacing="0" cellpadding="0">
                  <tbody>
                     <caption>Department Official</caption>
                     <tr>
                        <th align="center" valign="middle">Serial No.</th>
                        <th align="left" valign="middle">NAME</th>
                        <th align="left" valign="middle">DESIGNATION</th>
                        <th align="left" valign="middle">TEL NO.(O)</th>
                        <th align="left" valign="middle">TEL NO.(R)</th>
                        <th align="left" valign="middle">Email Address</th>
                        <th align="left" valign="middle">OFFICE ADDRESS</th>
                     </tr>
                     <tr>
                        <td align="center" valign="middle">1</td>
                        <td align="left" valign="middle">Shri xyz</td>
                        <td align="left" valign="middle">Direction</td>
                        <td align="left" valign="middle">12345678</td>
                        <td align="left" valign="middle">87654321</td>
                        <td align="left" valign="middle">abc@nic.in</td>
                        <td align="left" valign="middle">New Delhi</td>
                     </tr>
                     <tr>
                        <td align="center" valign="middle">2</td>
                        <td align="left" valign="middle">Shri xyz</td>
                        <td align="left" valign="middle">Direction</td>
                        <td align="left" valign="middle">12345678</td>
                        <td align="left" valign="middle">87654321</td>
                        <td align="left" valign="middle">abc@nic.in</td>
                        <td align="left" valign="middle">New Delhi</td>
                     </tr>
                     <tr>
                        <td align="center" valign="middle">3</td>
                        <td align="left" valign="middle">Shri xyz</td>
                        <td align="left" valign="middle">Direction</td>
                        <td align="left" valign="middle">12345678</td>
                        <td align="left" valign="middle">87654321</td>
                        <td align="left" valign="middle">abc@nic.in</td>
                        <td align="left" valign="middle">New Delhi</td>
                     </tr>
                     <tr>
                        <td align="center" valign="middle">4</td>
                        <td align="left" valign="middle">Shri xyz</td>
                        <td align="left" valign="middle">Direction</td>
                        <td align="left" valign="middle">12345678</td>
                        <td align="left" valign="middle">87654321</td>
                        <td align="left" valign="middle">abc@nic.in</td>
                        <td align="left" valign="middle">New Delhi</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <hr />
         </div>
      </section> -->
      <!--/.body-wrapper-->
      <section class="wrapper carousel-wrapper">
         <div class="container common-container four_content carousel-container">
            <div id="flexCarousel" class="flexslider carousel">
               <ul class="slides">
                  <li><a target="_blank" href="http://digitalindia.gov.in/" title="Digital India, External Link that opens in a new window"><img src="assets/images/carousel/digital-india.png" alt="Digital India"></a></li>
                  <li><a target="_blank" href="http://www.makeinindia.com/" title="Make In India, External Link that opens in a new window"> <img src="assets/images/carousel/makeinindia.png" alt="Make In India"></a></li>
                  <li><a target="_blank" href="http://india.gov.in/" title="National Portal of India, External Link that opens in a new window"><img src="assets/images/carousel/india-gov.png" alt="National Portal of India"></a></li>
                  <li><a target="_blank" href="http://goidirectory.nic.in/" title="GOI Web Directory, External Link that opens in a new window"><img src="assets/images/carousel/goidirectory.png" alt="GOI Web Directory"></a></li>
                  <li><a target="_blank" href="https://data.gov.in/" title="Data portal, External Link that opens in a new window" ><img src="assets/images/carousel/data-gov.png" alt="Data portal"></a></li>
                  <li><a target="_blank" href="https://mygov.in/" title="MyGov, External Link that opens in a new window"><img src="assets/images/carousel/mygov.png" alt="MyGov Portal"></a></li>
               </ul>
            </div>
         </div>
      </section>
      <!--/.carousel-wrapper-->
      <footer class="wrapper footer-wrapper">
        <div class="footer-top-wrapper">
           <div class="container common-container four_content footer-top-container">
              <ul>
                 <li><a href="inner.html">Feedback</a></li>
                 <li><a href="inner.html">Website policies</a></li>
                 <li><a href="inner.html">Terms and Conditions </a></li>
                 <li><a href="inner.html">Contact Us</a></li>
                 <li><a href="inner.html">Help</a></li>
                 <li><a href="inner.html">Web Information Manager</a></li>
                 <li><a href="inner.html">Visitor Pass</a></li>
                 <li><a href="inner.html">FAQ</a></li>
                 <li><a href="inner.html">Disclaimer</a></li>
              </ul>
           </div>
        </div>
        <div class="footer-bottom-wrapper">
            <div class="container common-container four_content footer-bottom-container">
               <div class="footer-content clearfix">
                <div class="copyright-content"> 
                    <strong> इस वेब साइट का कंटेंट राज्य संपत्ति विभाग उत्तर प्रदेश कृषि भवन, लखनऊ द्वारा प्रकाशित एवं संचालित किया जाता है।'</strong>
                    <span>इस वेबसाइट के बारे में किसी भी प्रश्न के लिए, कृपया "वेब सूचना प्रबंधक" से संपर्क करें।
                    <!-- <a target="_blank" title="NIC, External Link that opens in a new window" href="http://www.nic.in/"> -->
                    <strong>कापीराइट © राज्य संपत्ति विभाग , उत्तर प्रदेश को सभी अधिकार सुरक्षित हैं।</strong></span> 
                 </div>
                  <!-- <div class="logo-cmf"> <a target="_blank" href="http://cmf.gov.in/" title="External link that opens in new tab, cmf"><img alt="cmf logo" src="assets/images/cmf-logo.png"></a> </div> -->
               </div>
            </div>
         </div>
     </footer>
     <!--/.footer-wrapper-->
     <!-- jQuery v1.11.1 -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js" integrity="sha512-nhY06wKras39lb9lRO76J4397CH1XpRSLfLJSftTeo3+q2vP7PaebILH9TqH+GRpnOhfAGjuYMVmVTOZJ+682w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
     <!-- jQuery Migration v1.4.1 -->
     <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
     <!-- jQuery v3.6.0 -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
     <!-- jQuery Migration v3.4.0 -->
     <script src="https://code.jquery.com/jquery-migrate-3.4.0.min.js"></script>
     
     <script src="assets/js/jquery-accessibleMegaMenu.js"></script>
     <script src="assets/js/framework.js"></script>
     <script src="assets/js/jquery.flexslider.js"></script>
     <script src="assets/js/font-size.js"></script>
     <script src="assets/js/swithcer.js"></script>
     <script src="theme/js/ma5gallery.js"></script>
     <script src="assets/js/megamenu.js"></script>
     <script src="theme/js/easyResponsiveTabs.js"></script>
     <script src="theme/js/custom.js"></script>
  </body>
</html>