<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">विहित प्राधिकारी राज्य संपत्ति विभाग</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="180" align="center"><b>नाम </b></td>
                  <td width="271" align="center"><b>पदनाम </b></td>
                  <td width="68" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="84" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in">
  <p class="MsoNormal">श्री सुधीर कुमार रूंगटा </p>
  </td>
  <td valign="top" style="width:260px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in">
  <p class="MsoNormal">विहित प्राधिकारी राज्य संपत्ति विभाग </p>
  </td>
  <td valign="top" style="width:57px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" align="center">
  <p class="MsoNormal"><font face="Arial" style="font-size: 11pt">2286406</font></p></td>
  <td valign="top" style="width:73px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" align="center">
  <p class="MsoNormal"><font face="Arial" style="font-size: 11pt">9454412031</font></p>
  </td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in">
  <p class="MsoNormal">श्री राम विलास चौरसिया </p>
  </td>
  <td valign="top" style="width:260px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in">
  <p class="MsoNormal">पेशकर </p>
  </td>
  <td valign="top" style="width:57px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" align="center">
  <p class="MsoNormal"><font face="Arial" style="font-size: 11pt">2286406</font></p>
  </td>
  <td valign="top" style="width:73px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" align="center">
  <p class="MsoNormal"><font face="Arial" style="font-size: 11pt">9454421221</font></p></td>
            </tr>
            <tr>
  <td valign="top" style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:168px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in">
  श्री संजय तिवारी
  </td>
  <td valign="top" style="width:260px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in">
  कनिष्ठ सहायक
  </td>
  <td valign="top" style="width:57px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" align="center">
  <font face="Arial" style="font-size: 11pt">2286406</font></td>
  <td valign="top" style="width:73px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" align="center">
  <font face="Arial" style="font-size: 11pt">9454421137</font></td>
            </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>