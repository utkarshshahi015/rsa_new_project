<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h4 class="row justify-content-md-center"><u>भूतपूर्व मुख्यमंत्री हेतु आवास आवंटन की व्यवस्था
</u></h4>
              </div>
              
               <table width="100%" border="2" cellspacing="3" cellpadding="3" align="center">
                <tbody><tr valign="top"> 
                  <td width="96%" class="style2"><div align="justify">
          <font color="#000000"><span style="font-weight: 400">उत्तर 
          प्रदेश के भूतपूर्व मुख्यमंत्रियों के लिए सरकारी आवासों का 
          आवंटन "भूतपूर्व मुख्यमंत्री निवास स्थान आवंटन नियमावली, 1997 
          (असाविधिक) , भूतपूर्व मुख्यमंत्री निवास स्थान आवंटन (प्रथम 
          एवं दितीय संसोधन ) नियमावली, 2001 (असाविधिक) एवं भूतपूर्व 
          मुख्यमंत्री निवास स्थान आवंटन (तृतीया संसोधन ) नियमावली, 
          2007 (असाविधिक) में उल्लिखित प्राविधानों के अनुसार किया जाता 
          है| </span></font></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="96%" class="style2">
          <font color="#000000"><span style="font-weight: 400">
          भूतपूर्व मुख्यमंत्रियों के लिए सरकारी आवासों का आवंटन उनके 
          जीवनकाल तक के लिए होगा। </span></font></td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>