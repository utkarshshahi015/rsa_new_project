<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h4 class="row justify-content-md-center"><u>सुविख्यात व्यक्तियों के नाम से गठित ट्रस्टो को सरकारी आवास आवंटित करने सम्बन्धी व्यवस्था
</u></h4>
              </div>
              
               <table width="100%" border="2" cellspacing="3" cellpadding="3" align="center">
                <tbody><tr valign="top"> 
                  <td width="94%" class="style3">
                    <div align="justify"><font face="Kruti Dev 010">ऐसे 
            सुविख्यात व्यक्ति जिन्हे राष्ट्रीय नायक के रूप में जाना 
            जाता है, के नाम से संस्थापित समाजसेवी ट्रस्ट के उपयोग के 
            लिए राज्य संपत्ति विभाग के प्रशासनिक नियंतरण में लखनऊ 
            विकास प्राधिकरण की सीमा के भीतर सरकारी भवनो का आवंटन 
            सुविख्यात व्यक्तियों के उपयोगार्थ राज्य संपत्ति विभाग के 
            प्रशासनिक नियंतरण में लखनऊ स्थित भवनो का आवंटन
            <font color="#0000FF">
            <a target="_blank" href="../assets/doc/awas18.pdf">नियमावली<span style="font-size: 15pt" lang="en-us">
            </span></a></font><span style="font-size: 15pt">
            <a target="_blank" href="../assets/doc/awas18.pdf">2003</a>(</span><span lang="en-us">
            </span>असंविधिक) में उल्लिखित प्राविधानों के अंतर्गत किया 
            जाता है। </font></div>                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="94%" class="style3">
                    <div align="justify">आवंटी ट्रस्ट को आवास का कब्ज़ा प्राप्त 
            करने के दिनांक से प्रत्येक माह राजनैतिक दलों को आवंटित 
            आवासो की भांति तत्समय प्रभावी फुलेट रेंट के दो गुनी दर 
            से किराया भुगतान करना होगा। आवास के किराये के अतिरिक्त 
            ऐसी ट्रस्ट को जलकर एवं सीवरकर की धनराशि का भुगतान भी 
            प्रतिमाह करना होग। </div>                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="94%" height="2" class="style3">
                    <div align="justify">ट्रस्ट के पक्ष में आवास का आवंटन अधिकतम 
            तीस वर्ष के लिए ही किया जायेगा, जिसे सम्बंधित आवंटन आदेश 
            में विनिर्दिष्ट किया जायेगा। विनिर्दिस्ट अवधी समाप्त होने 
            पर उसे आगे बढ़ाने अथवा न बढ़ाने का अधिकार राज्य सरकार का 
            होगा। <br>
&nbsp;</div>                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="94%" class="style3">
                    <div align="justify">आवंटी ट्रस्ट को आवंटन की अवधि समाप्त 
            होने पर आवंटित आवास का कब्ज़ा राज्य संपत्ति विभाग को सौपना 
            होगा। </div>                  </td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>