<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">माननीय मंत्रीगणों के आवास आवंटन सम्बन्धी व्यवस्था</h2>
               <table width="100%" border="2" cellspacing="3" cellpadding="3" align="center">
                <tbody><tr valign="top"> 
                  <td height="23" class="style3">
                    <div align="justify"><span lang="en-us">
            <font face="Kruti Dev 010">उत्तर प्रदेश सरकार मे माननीय 
            मंत्री, राज्यमंत्री, विधानसभा के अध्यक्ष उपाध्यक्ष विधान परिषद के सभापति उपसभापति को उत्तर प्रदेश मंत्री 
            और राज्य विधान मंडल अधिकारी निवास स्थान </font></span>
            <b>
            <font face="Kruti Dev 010">
            <a target="_blank" href="../../GEOS/Awas/awas1.pdf">नियमावली<font style="font-size: 15pt"> 1959</font></a></font></b><span lang="en-us"><font face="Kruti Dev 010"><b>
            </b>के अंतर्गत एक निशुल्क सुसज्जित आवास पदावधि तक के लिए 
            उपलब्ध कराये जाने का प्राविधान है। </font></span></div>                  </td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>