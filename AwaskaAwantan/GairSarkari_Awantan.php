<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h4 class="row justify-content-md-center"><u>गैर सरकारी व्यक्ति / गैर सरकारी संस्था, ट्रस्ट/ कर्मचारी संघों को
आवास आवंटन सम्बन्धी व्यवस्था</u></h4>
              </div>
              
               <table width="100%" border="2" cellspacing="2" cellpadding="3" align="center">
                  <tbody><tr valign="top"> 
                    <td width="92%" class="style2">
                      <div align="justify"><span style="font-weight: 400">
            <font face="Kruti Dev 010" color="#000000">गैर सरकारी 
            व्यक्ति गैर सरकारी संस्था ट्रस्ट<span lang="en-us">
            </span>कर्मचारी संघों को 
            आवास आवंटन राज्य सम्पत्ति अनुभाग<span style="font-size: 15pt"><span lang="en-us">
            </span>-2</span> के शासनादेश </font>
            <a target="_blank" href="../assets/doc/awas20.pdf">
            <font face="Kruti Dev 010" color="#0000FF">संख्या-</font><font face="Kruti Dev 010"><font color="#0000FF">
            </font> आर<span style="font-size: 15pt">- 2839/32 
            -2-2005-27/2004</span> टी0सी0 दिनांक
            <span style="font-size: 15pt">4-7-2005</span></font></a><font color="#000000" face="Kruti Dev 010"><span style="font-size: 15pt">
            </span>
            </font>
            <font face="Kruti Dev 010" color="#000000">में उल्लिखित 
            प्राविधानों के अनुसार किया जाता ह</font></span></div>                  </td>
                  </tr>
                  <tr valign="top"> 
                    <td width="92%" class="style2">
                      <div align="justify"><span style="font-weight: 400">
            <font color="#000000">ऐसी गैर सरकारी संस्थायें/ ट्रस्ट 
            जो सोसाइटी रजिस्ट्रेशन एक्ट- 1860, अथवा ट्रस्ट अधिनियम 
            1882 , के अन्र्तगत पंजीकृत हैं, और ”सुविख्यात व्यक्ति के 
            ट्रस्ट के उपयोगार्थ राज्य सम्पत्ति विभाग के प्रशासनिक 
            नियन्त्रण में लखनऊ स्थित भवनों की आवंटन नियमावली, 2003 
            से आच्छादित नहीं है, तथा जिनका मुख्यालय लखनऊ में है, और 
            संस्था/ ट्रस्ट अन्र्तराष्ट्रीय/ राष्ट्रीय / प्रदेश स्तर 
            पर ख्याति प्राप्त है तथा जिसका सामाजिक, सांस्कृतिक, शैक्षिक, 
            खेलकूद, वैज्ञानिक अथवा साहित्यिक क्षेत्रों में विशिष्ट 
            योगदान रहा हो, आवास आवंटन के लिए अर्ह हैं।</font></span></div>                  </td>
                  </tr>
                  <tr valign="top"> 
                    <td width="92%" height="30" class="style2">
                      <div align="justify"><span style="font-weight: 400">
            <font color="#000000">ऐसे कर्मचारी संघ जो राज्य सरकार के 
            द्वारा मान्यता प्राप्त हैं, और जिनका मुख्यालय लखनऊ में 
            है, आवास आवंटन के लिए पात्र होंगे। </font></span></div>                  </td>
                  </tr>
                  <tr valign="top"> 
                    <td width="92%" class="style2">
                      <div align="justify"><span style="font-weight: 400">
            <font color="#000000">गैर सरकारी व्यक्ति/गैर सरकारी 
            संस्था जिनका राजनैतिक,सामाजिक, सांस्कृतिक, शैक्षिक, 
            वैज्ञानिक, खेलकूद अथवा साहित्यिक क्षेत्रों में विशिष्ट 
            योगदान रहा हो, अथवा राज्य सरकार के द्वारा गठित विभिन्न 
            आयोगों/ निगमों/ स्वायत्तशासी संस्थाओं के अध्यक्ष व 
            सदस्यगण भवन आवंटन के लिए पाात्र होंगे।</font></span></div>                  </td>
                  </tr>
                  <tr valign="top"> 
                    <td width="92%" class="style2">
                      <div align="justify"><span style="font-weight: 400">
            <font color="#000000">गैर सरकारी संस्था, गैर सरकारी 
            ट्रस्ट, गैर सरकारी व्यक्ति/ कर्मचारी संघ के पक्ष में 
            श्रेणी-4 तक का आवास उपलब्धता के आधार पर तीन वर्ष के लिए 
            किया जा सकेगा। प्रथम बार आवंटन एक वर्ष के लिए किया जायेगा, 
            तथा उसके पश्चात् पुनः एक-एक वर्ष के लिए नवीनीकरण करते 
            हुए अधिकतम दो वर्ष के लिए आवंटन किया जा सकेगा । विशेष 
            परिस्थितियों में राज्य सरकार द्वारा नवीनीकरण अवधि तीन 
            वर्ष के पश्चात् भी बढायी जा सकती है जो तीन वर्ष के 
            उपरान्त दो वर्ष से अधिक नहीं होगी।</font></span></div>                  </td>
                  </tr>
                  <tr valign="top"> 
                    <td width="92%" class="style2">
                      <div align="justify"><span style="font-weight: 400">
            <font color="#000000">गैर सरकारी संस्था/ ट्रस्ट/गैर 
            सरकारी व्यक्ति/ कर्मचारी संघ को आवंटित आवास का किराया 
            राज्य सम्पत्ति अनुभाग-2 के कार्यालय ज्ञाप संख्या- आर . 
            5336ध्32.2.2001 .29ध्2001 दिनांक 29.10.2001 के प्राविधानों 
            के अनुसार बाजार दर में 25 प्रतिशत की छूट देते हुए 
            निर्धारित किया जायेगा। उपरोक्त श्रेणी के आवंटियों को 
            जलकर/ सीवर कर तथा विद्युत बिल का भुगतान स्वयं करना होता 
            है।</font></span></div>                  </td>
                  </tr>
                  <tr valign="top"> 
                    <td width="92%" class="style2">
                      <div align="justify"><span style="font-weight: 400">
            <font color="#000000">आवंटन अवधि की समाप्ति के ठीक 
            पश्चात् आवंटन बिना पूर्व सूचना के स्वतः निरस्त माना 
            जायेगा, तथा उक्त तिथि के एक माह के अन्दर आवास खाली करना 
            पडेगा। </font></span></div>                  </td>
                  </tr>
                </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>