<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h4 class="row justify-content-md-center"><u>पत्रकारों को सरकारी आवास आवंटित करने सम्बन्धी व्यवस्था
</u></h4>
              </div>
              
               <table width="100%" border="2" cellspacing="3" cellpadding="3" align="center">
                <tbody><tr valign="top"> 
                  <td width="86%" class="style4" height="70">
                    <div align="justify"><font face="Kruti Dev 010">मान्यता 
            प्राप्त सम्पादकों, उपसम्पादकों तथा कार्यरत पत्रकारों, जो 
            पूर्णकालिक रूप से लखनऊ में सेवारत् हैं तथा जिनके समाचार 
            पत्रों के पंजीकृत कार्यालय लखनऊ में स्थापित हैं,को राज्य 
            सम्पत्ति विभाग के अधीन सरकारी आवासों का आवंटन कार्यालय 
            ज्ञाप संख्या-
            <a target="_blank" href="../assets/doc/awas5.pdf">आर- 
            <span style="font-size: 15pt">1408/32-2-85-21177</span> टी0सी0 दिनांक
            <span style="font-size: 15pt">21</span> मई
            <span style="font-size: 15pt">1985</span></a> में 
            उल्लिखित प्राविधानों के अनुसार सीमित अवधि के लिए किया 
            जाता है। </font></div>                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="86%" class="style4" height="51">
                    <div align="justify">मान्यता प्राप्त पत्रकारेा को लखनऊ में 
            सरकारी आवास का आवंटन केवल उसी अवधि तक के लिए किया जायेगा 
            जब तक कि वह लखनऊ में किसी मान्यता प्राप्त समाचार पत्र 
            में कार्यरत रहेगें, और उसके बाद उसका आवंटन स्वतः समाप्त 
            माना जायेगा। </div>                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="86%" height="2" class="style4">
                    <div align="justify">मान्यता प्राप्त पत्रकारों को लखनऊ में 
            सरकारी आवासों के आवंटन हेतु निर्धारित आवेदन पत्र का 
            प्रारूप निम्नवत् है</div>                  </td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>