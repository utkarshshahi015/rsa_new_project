<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h5 class="row justify-content-md-center"><u>मध्यम गृह निर्माण योजना के अन्र्तगत निर्मित आवासों का आवंटन
</u></h5>
              </div>
              
               <table width="100%" border="1" cellspacing="4" cellpadding="4" align="center">
                <tbody><tr valign="top"> 
                  <td height="23" class="style3">
                    <div align="justify"><font face="Kruti Dev 010"> मध्यम गृह 
            निर्माण योजना के अन्र्तगत निर्मित आवासों का आवंटन राज्य 
            सम्पत्ति विभाग द्वारा निर्गत नोटिस संख्या-
            <a target="_blank" href="../assets/doc/awas2.pdf">आर<span style="font-size: 15pt">- 236</span> रा0 स0 वि0 
            <span style="font-size: 15pt">1962</span> दिनांक<span style="font-size: 15pt"> 11-01-1962</span></a> एवं नोटिस संशोधन विषयक 
            शासनादेश&nbsp; में उल्लिखित प्राविधानों के अनुसार सरकारी गैर सरकारी व्यक्तियों को किया जाता है।</font></div>                  </td>
                </tr>
              </tbody></table>
              <div align="center"><span class="style3"><b><font size="4" face="Kruti Dev 010">&nbsp;<u><br>
        </u></font></b><font face="Kruti Dev 010" size="4">उक्त योजना के 
        अन्र्तगत निम्नांकित कालोनियों में आवास उपलब्ध हैं </font><br>
                </span>
                <table width="100%" border="2" cellspacing="2" cellpadding="2" align="center">
                <tbody><tr valign="top"> 
                  <td class="style3" align="center"> 
                    <b>कालोनी का नाम </b>                  </td>
                  <td class="style3" width="132" align="center"> 
                    <div align="center"><b>श्रेणी-2 के आवास की संख्या </b></div>                  </td>
                  <td class="style3" width="115" align="center"> 
                    <div align="center"><b>श्रेणी-3 के आवास की संख्या</b></div>                  </td>
                  <td class="style3" width="124" align="center"> 
                    <div align="center"><b>श्रेणी-4 के आवास की संख्या</b></div>                  </td>
                  <td class="style3" width="76" align="center"> 
                    <div align="center"><b>कुल आवास </b></div>                  </td>
                </tr>
                <tr valign="top"> 
                  <td class="style3">राजेन्द्र नगर, बसीरतगंज सी/ डी कालोनी </td>
                  <td class="style3" width="132"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">108</font></div>                  </td>
                  <td class="style3" width="115"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">08</font></div>                  </td>
                  <td class="style3" width="124"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">-</font></div>                  </td>
                  <td class="style3" width="76"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">116</font></div>                  </td>
                </tr>
                <tr valign="top"> 
                  <td class="style3">महानगर मध्यम आय वर्ग</td>
                  <td class="style3" width="132"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">-</font></div>                  </td>
                  <td class="style3" width="115"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">-</font></div>                  </td>
                  <td class="style3" width="124"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">07</font></div>                  </td>
                  <td class="style3" width="76"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">07</font></div>                  </td>
                </tr>
                <tr valign="top"> 
                  <td class="style3">हैवलक रोड सी/बी</td>
                  <td class="style3" width="132"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">24</font></div>                  </td>
                  <td class="style3" width="115"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">08</font></div>                  </td>
                  <td class="style3" width="124"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">-</font></div>                  </td>
                  <td class="style3" width="76"> 
                    <div align="center">
            <font face="Kruti Dev 010" style="font-size: 15pt">32</font></div>                  </td>
                </tr>
              </tbody></table>

              <span class="style3"><br>
              </span>
              <table width="100%" border="1" cellspacing="4" cellpadding="4" align="center">
                <tbody><tr> 
                  <td valign="top" class="style3">
                    <div align="justify">उक्त आवासों के आवंटी की मृत्यु के 
            पश्चात् यदि आवंटी सरकारी कर्मचारी है तो उक्त आवास का 
            आवंटन उसके आश्रित को जो परिवार की परिभाषा में सम्मिलित 
            हैं, को आवंटन उन्ही शर्तो एवं प्रतिबन्धों के अधीन किया 
            जा सकता है। यदि आवंटी गैर सरकारी व्यक्ति है तो उसकी 
            मृत्यु के पश्चात् आवास उसके आश्रितों को आवंटित नहीं किया 
            जाता है। </div>                  </td>
                </tr>
                <tr> 
                  <td valign="top" class="style3">
                    <div align="justify"><font face="Kruti Dev 010">यदि उक्त 
            योजना के अन्र्तगत निर्मित आवासों के आवंटी जो सरकारी 
            कर्मचारी है, उसके कोई ऐसा आश्रित नहीं है जो परिवार की 
            परिभाषा में सम्मिलित है तथा यदि आवंटी गैर सरकारी व्यक्ति 
            है तो उसकी मृत्यु के उपरान्त आवास रिक्त होने पर वर्तमान 
            समय में उसका आवंटन सरकारी निवास स्थान (लखनऊ स्थित समस्त 
            सामान्य निवास स्थान)
            <a target="_blank" href="../assets/doc/awas22.pdf">आवंटन नियमावली 
            <span style="font-size: 15pt">1980</span></a> के अधीन किया जायेगा। </font></div>                  </td>
                </tr>
              </tbody></table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>