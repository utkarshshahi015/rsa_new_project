<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">माननीय विधायकगणों के आवास आवंटन सम्बन्धी व्यवस्था</h2>
               <table width="100%" border="2" cellspacing="3" cellpadding="3" align="center">
                <tbody><tr valign="top"> 
                  <td height="23" class="style3">
                    <div align="justify"><font face="Kruti Dev 010">उत्तर प्रदेश 
            राज्य विधान मण्डल के सदस्यों की निवास स्थान सम्बन्धी
            <font color="#0000FF">
            <a target="_blank" href="../assets/doc/awas3.pdf">नियमावली<span lang="en-us"> </span>
            </a>
            </font> 
                        <a target="_blank" href="../../GEOS/Awas/awas3.pdf">
            <font style="font-size: 15pt">1963</font> </a>में उत्तर 
            प्रदेश विधान मण्डल के सदस्यों को विभिन्न विधायक निवासों 
            में एक निःशुल्क कक्ष उनकी सदस्यता अवधि तक के लिए उपलब्ध 
            कराये जाने का प्राविधान है। </font></div>
          <div align="justify">&nbsp;</div>                  </td>
                </tr>
                <tr valign="top"> 
                  <td height="2" class="style3">
                    <div align="justify">विधान मण्डल के सदस्यों को किसी आवास की 
            व्यवस्था न होने पर 300 रू0 प्रतिमास की दर से भत्ता पाने 
            के हक़दार होंगे तथा जहां किसी सदस्य को ऐसे आवास की 
            व्यवस्था की जाये जिसका मानक किराया 300 रू0 प्रतिमास से 
            कम हो, वहां किराये के अंतर का भुगतान ऐसे सदस्य को 
            प्रतिकर आवास भत्ते के रूप में किया जायेगा और जहा इस 
            प्रकार व्यवस्थित आवास का मानक किराया उक्त धनराशि से अधिक 
            हो वहां किराये के अंतर को सदस्य से वसूल किया जायेगा। </div>                  </td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>