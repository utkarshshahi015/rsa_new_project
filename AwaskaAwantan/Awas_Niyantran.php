<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="index.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">राज्य संपत्ति विभाग अनुभाग - 1</h2>
               <table width="100%" border="2" cellspacing="4" cellpadding="4" align="center">
                <tbody><tr valign="top"> 
                  <td width="6%" height="23"> 
                    <div align="center"><font color="006699">1</font></div>
                  </td>
                  <td width="62%" height="23">
          <a href="Mantri_Awantan.php" target="_blank">
          <font face="Kruti Dev 010">माननीय मंत्रीगणों के आवास आवंटन 
          सम्बन्धी व्यवस्था। </font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="24"> 
                    <div align="center"><font color="006699">2</font></div>
                  </td>
                  <td width="62%" height="24">
          <font face="Kruti Dev 010" color="006699"> 
                    <a href="Vidhayak_Awantan.php" target="_blank">माननीय विधायकगणों 
          के आवास आवंटन सम्बन्धी व्यवस्था। </a></font></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="2"> 
                    <div align="center"><font color="006699">3</font></div>
                  </td>
                  <td width="62%" height="2">
          <font face="Kruti Dev 010" color="006699">
          <a href="Sarkari_Awantan.php" target="_blank">सरकारी अधिकारियों
          <span lang="en-us">@</span> कर्मचारियों के आवास आवंटन 
          सम्बन्धी व्यवस्था। </a></font></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="23"> 
                    <div align="center"><font color="006699">4</font></div>
                  </td>
                  <td width="62%"><a href="GairSarkari_Awantan.php" target="_blank">
          <font face="Kruti Dev 010">गैर सरकारी व्यक्ति
          <span lang="en-us">@</span> गैर सरकारी संस्था<span lang="en-us">]</span> 
          ट्रस्ट <span lang="en-us">@</span> कर्मचारी संघो को आवास 
          आवंटन सम्बन्धी व्यवस्था। </font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="24"> 
                    <div align="center"><font color="006699">5</font></div>
                  </td>
                  <td width="62%"><a href="Trust_Awantan.php" target="_blank">
          <font face="Kruti Dev 010">सुविख्यात व्यक्तियों के नाम से 
          गठित ट्रस्टो को सरकारी आवास आवंटित करने सम्बन्धी व्यवस्था।
          </font></a></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%"> 
                    <div align="center"><font color="006699">6</font></div>
                  </td>
                  <td width="62%"><font face="Kruti Dev 010" color="006699">
          <a href="PoliParti_awantan.php" target="_blank">राजनैतिक दलों को 
          कार्यालय हेतू आवास आवंटन सम्बन्धी व्यवस्था।</a></font></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="2"> 
                    <div align="center"><font color="006699">7</font></div>
                  </td>
                  <td width="62%" height="2">
          <font face="Kruti Dev 010" color="006699">
          <a href="ChiefMinister_awantan.php" target="_blank">भूतपूर्व 
          मुख्यमंत्रियों को सरकारी आवास आवंटित करने सम्बन्धी व्यवस्था। </a></font></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="24"> 
                    <div align="center"><font color="006699">8</font></div>
                  </td>
                  <td width="62%"><font face="Kruti Dev 010" color="006699">
          <a href="Patrakarawantan.php" target="_blank">पत्रकारों को सरकारी 
          आवास आवंटित करने सम्बन्धी व्यवस्था। </a></font></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%"> 
                    <div align="center"><font color="006699">9</font></div>
                  </td>
                  <td width="62%"><font face="Kruti Dev 010" color="006699"> 
                    <a href="Medium_Awantan.php" target="_blank">माध्यम गृह निर्माण 
          योजना के अंतर्गत निर्मित आवासो के आवंटन सम्बन्धी व्यवस्था।</a></font></td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>