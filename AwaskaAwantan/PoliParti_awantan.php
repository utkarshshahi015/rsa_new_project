<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <div class="col-md-6 offset-md-3">
                <h4 class="row justify-content-md-center"><u>राजनैतिक दलों के कार्यालय हेतु आवास
आवंटित करने सम्बन्धी व्यवस्था
</u></h4>
              </div>
              
               <table width="100%" border="2" cellspacing="3" cellpadding="3" align="center" height="213">
                <tbody><tr valign="top"> 
                  <td width="85%" class="style3" height="77">
                    <div align="justify"><font face="Kruti Dev 010">राजनैतिक दलों 
            को पार्टी के प्रयोजनार्थ लखनऊ स्थित राजकीय आवासों का 
            आवंटन
            <a target="_blank" href="../assets/doc/awas4.pdf">शासनादेश दिनांक
            <span style="font-size: 15pt">22-6-1982</span></a> में उल्लिखित प्राविधानों के अनुसार किया 
            जाता है ऐसे राजनैतिक दल जो निर्वाचन आयोग द्वारा 
            राष्ट्रीय अथवा उत्तर प्रदेश हेतु क्षेत्रीय दल के रूप में 
            मान्यता प्राप्त 
            हों आवास आवंटन हेतु अर्ह होंगें।</font></div>                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="85%" class="style3">
                    <div align="justify">विधान सभा में यदि किसी राजनैतिक दल के 
            सदस्यों की संख्या 50 या अधिक है, तो उस दल के प्रयोजनार्थ 
            एक टाइप-5 का आवास (भांति हो) उपलब्धता के आधार पर आवंटित 
            किया जा सकेगा। जनसामान्य की आवश्यकताओं को ध्यान में रखकर 
            उपलब्धता होने पर प्रत्येक 100 सदस्यों पर एक बंगला ऐसे दल 
            के प्रयोजनार्थ आवंटित किया जा सकेगा। राजनैतिक दल के 
            विधान सभा में सदस्यों का प्रतिनिधित्व 50 से कम होने पर 
            एक आवासीय फ्लेट लखनऊ स्थित किसी भी सरकारी कालोनी में 
            उपलब्धता पर आवंटित किया जा सकेगा, जिस राजनैतिक दल का 
            विधान सभा में प्रतिनिधित्व 05 से कम हो उसे कोई आवास 
            आवंटित नहीं किया जायेगा।</div>                  </td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>