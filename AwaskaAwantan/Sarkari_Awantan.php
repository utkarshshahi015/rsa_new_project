<?php include('../header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="<?php echo url(); ?>assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
      <font face="Verdana, Arial, Helvetica, sans-serif">
        <a href="Awas_Niyantran.php" class="style2">
        <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">सरकारी अधिकारी / कर्मचारीयों के आवास आवंटन सम्बन्धी व्यवस्था</h2>
               <table width="100%" border="2" cellspacing="3" cellpadding="3" align="center">
                <tbody><tr valign="top"> 
                  <td width="93%" height="23" class="style3"><div align="justify">
          <font face="Kruti Dev 010">सरकारी अधिकारियों कर्मचारीयों 
          का आवास आवंटन सरकारी निवास स्थान (लखनऊ स्थित समस्त सामान्य 
          निवास स्थान) आवंटन
          <b><a target="_blank" href="../assets/doc/awas22.pdf">नियमावली 
          <span style="font-size: 15pt">1980</span></a></b> में 
          उल्लिखित प्राविधानों के अनुसार किया जाता है।</font></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="93%" height="24" class="style3"><div align="justify">
          <font face="Kruti Dev 010">आवास आवंटन के लिए मात्र लखनऊ में 
          तैनात अधिकारी&nbsp; कर्मचारी अहर्त है।&nbsp; </font></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="93%" height="30" class="style3"><div align="justify">
          अधिकारियों / कर्मचारीयों का आवास आवंटन की श्रेणी उनको 
          प्राप्त होने वाले उस पद के साधारण वेतनमान के आधार पर 
          निर्धारित की जाती है। </div></td>
                </tr>
                <tr valign="top"> 
                  <td width="93%" class="style3"><div align="justify">
          <font face="Kruti Dev 010">लखनऊ स्थित राज्य संपत्ति विभाग के 
          नियंत्रणाधीन श्रेणी 
          <span style="font-size: 15pt">-1</span> 
          से श्रेणी<span style="font-size: 15pt"> - 6</span> के समस्त 
          राजकीय आवासो का किराया शासनादेश संख्या<span lang="en-us">
          </span> 
          <span style="font-weight: 700">
          <a target="_blank" href="../assets/doc/awas17.pdf">आर
          <span style="font-size: 15pt">4528/32-2-02-21/01</span> दिनांक
          <span style="font-size: 15pt">20-2-2003</span></a></span> 
                    के अनुसार लिविंग एरिया के आधार पर फ्लैट रेंट की दर से लिया 
          जाता है । </font></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="93%" class="style3"><div align="justify">
          <font face="Kruti Dev 010">राज्य संपत्ति विभाग के प्रबंधन एवं 
          नियंत्रणाधीन सभी श्रेणी के आवासो के अध्यासियो के स्थानांतरण सेवानिवृत<span lang="en-us">
          </span>मृत्यु होने की दशा में एक माह ही अध्यासन 
          अनुमान्य होता है। उक्त अनुमान्य अवधी समाप्त होने पर 
          और आवास रिक्त न करने की दशा में अध्यासन अप्राधिकृत हो जाता 
          है </font></div></td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('../footer.php')?>