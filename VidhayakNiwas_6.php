<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">विधायक निवास - 6, पार्करोड, लखनऊ</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="193" align="center"><b>पदनाम </b></td>
                  <td width="134" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="21%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री मनोज कुमार </td>
                  <td width="193" align="center">कार्यवाहक व्यवस्थाधिकारी </td>
                  <td width="134" align="center">2236715</td>
                  <td width="21%" align="center">9454421184<br>
          9473896758</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री लाल जी मौर्या </td>
                  <td width="193" align="center">स्टोर कीपर </td>
                  <td width="134" align="center">2236715</td>
                  <td width="21%" align="center">9454421127</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री मनोज कुमार </td>
                  <td width="193" align="center">सहायक स्टोर कीपर </td>
                  <td width="134" align="center">2236715</td>
                  <td width="21%" align="center">9454421129</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्री गिरीश चन्द्र जोशी </td>
                  <td width="193" align="center">कनिष्ठ सहायक </td>
                  <td width="134" align="center">2236715</td>
                  <td width="21%" align="center">9454421128</td>
                </tr>
            <tr>
                  <td width="25%" align="center">चन्द्र भूषण मिश्रा </td>
                  <td width="193" align="center">टेलीफोन ऑपरेटर </td>
                  <td width="134" align="center">2236715</td>
                  <td width="21%" align="center">9454421130</td>
                </tr>
            <tr>
                  <td width="25%" align="center">राहुल कश्यप </td>
                  <td width="193" align="center">टेलीफोन ऑपरेटर </td>
                  <td width="134" align="center">2236715</td>
                  <td width="21%" align="center">9454421296</td>
                </tr>
            <tr>
                  <td width="25%" align="center">श्रीमती शिल्पी सिंह </td>
                  <td width="193" align="center">टेलीफोन ऑपरेटर </td>
                  <td width="134" align="center">2236715</td>
                  <td width="21%" align="center">9454421249</td>
                </tr>
            <tr>
                  <td width="25%" align="center">राम सेवक </td>
                  <td width="193" align="center">फर्रास </td>
                  <td width="134" align="center">2236715</td>
                  <td width="21%" align="center">9454696227</td>
                </tr>
            <tr>
                  <td width="25%" align="center">राजबहादुर यादव </td>
                  <td width="193" align="center">मैसेंजर </td>
                  <td width="134" align="center">2236715</td>
                  <td width="21%" align="center">9452108855</td>
                </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>