<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">जवाहर भवन</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="197" align="center"><b>पदनाम </b></td>
                  <td width="110" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="21%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  श्री शशांक चतुर्वेदी
  </td>
  <td nowrap="" valign="top" style="width:198px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  मुख्यव्यवस्था अधिकारी&nbsp;
  </td>
  <td valign="top" style="width:113px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:16.5pt" align="center">
  2286856</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  7668426311&nbsp;<o:p></o:p></td>
            </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  श्रीमती मधु चौधरी
  </td>
  <td nowrap="" valign="top" style="width:198px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  वरिष्ठ सहायक </td>
  <td valign="top" style="width:113px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:16.5pt" align="center">
  2286856</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  &nbsp;-</td>
            </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  <p class="MsoNormal">श्रीमती रश्मि शुक्ला </p>
  </td>
  <td nowrap="" valign="top" style="width:198px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  <p class="MsoNormal">कनिष्ठ सहायक </p>
  </td>
  <td valign="top" style="width:113px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:16.5pt" align="center">
  2286856</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  &nbsp;-</td>
            </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  <p class="MsoNormal">श्री प्रमोद साहनी </p>
  </td>
  <td nowrap="" valign="top" style="width:198px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  कनिष्ठ सहायक
  </td>
  <td valign="top" style="width:113px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:16.5pt" align="center">
  2286856</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  &nbsp;-</td>
            </tr>
            <tr>
  <td width="196" nowrap="" valign="top" style="width:146.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.75pt .75pt 0in .75pt;height:16.5pt">
  <p class="MsoNormal">श्रीमती संतोष सिंह&nbsp; </p>
  </td>
  <td nowrap="" valign="top" style="width:198px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:16.5pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
  <p class="MsoNormal">टेलीफोन ऑपरेटर</p>
  </td>
  <td valign="top" style="width:113px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:16.5pt" align="center">
  2286856</td>
  <td width="180" valign="top" style="width:134.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 0in 0in 0in;height:16.5pt" align="center">
  &nbsp;-</td>
            </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>