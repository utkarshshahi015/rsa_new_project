<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">अतिथि गृह नियम/नियमावली एवं महत्वपूर्ण शासनादेश</h2>
               <table width="100%" border="2" cellspacing="2" cellpadding="2" align="center">
                <tbody><tr valign="top"> 
                  <td width="6%" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" class="style2"><div align="justify">
          <span style="font-weight: 400">
          <font face="Kruti Dev 010" color="#000000">उत्तर 
          प्रदेश भवन, नई दिल्ली स्थित अतिथिगृह में ठहरने के लिये 
          नियमावली (असांविधिक)। </font><a href="#">
          <font face="Kruti Dev 010">
          शा0सं0-एम0-7597/32-3-1981दि0-10.08.1981</font></a></span></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" class="style2"><div align="justify">
          <span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">
          उ0प्र0 सदन में ठहरने के लिए पात्र महानुभाव। <br>
                    </font><a href="#">
          <font face="Kruti Dev 010">शा0सं0- 
          एम-2444/32-3-1997 दि0-14.05.1997</font></a></span></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" class="style2"><div align="justify">
          <span style="font-weight: 400">
          <font face="Kruti Dev 010" color="#000000">यू0पी0 
          भवन, यू0पी0 सदन, अति विशिष्ट अतिथिगृह, राज्य अतिथिगृह तथा 
          विशिष्ट अतिथिगृह के किराये का दर का निर्धारण। </font><a href="#">
          <font face="Kruti Dev 010">
          शा0सं0-एम-1351/32-3-2004-2एन0टी0/92 दि0-03.03.2004</font></a></span></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" class="style2"><div align="justify">
          <span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">
          समस्त राज्य अतिथि गृहों में भोजन/नास्तें की दरे का निर्धारण।
          </font><a href="#">
          <font face="Kruti Dev 010">
          शा0सं0-एम-6031/32-3-2005-57-81 दि0-07.09.2005</font></a></span></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" class="style2"><div align="justify">
          <span style="font-weight: 400">
          <font face="Kruti Dev 010" color="#000000">राज्य 
          अतिथिगृह, कोलकाता के नान ए0सी0 कक्षों का किराया निर्धारण। </font><a href="#">
          <font face="Kruti Dev 010">
          शा0सं0-4668/32-3-2005-2(एन0टी0)/92 दि0-03.10.2005</font></a></span></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="2" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" height="2" class="style2"><div align="justify">
          <span style="font-weight: 400">
          <font face="Kruti Dev 010" color="#000000">निगमों 
          के चेरमैन की अतिथिगृहों में अध्यासन अवधि का किराया हेतु </font><a href="#">
          <font face="Kruti Dev 010">शासनादेश 
          संख्या-एम-08/32-3-99 दिनांक-12.10.1999</font></a></span></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="2" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" height="2" class="style2"><div align="justify">
          <span style="font-weight: 400">
          <font face="Kruti Dev 010"><font color="#000000">
          शासकीय ड्यूटी पर उपयोग हेतु संगत नियमावली के अनुसार पात्र 
          अधिकारी को यदि आवास उपलब्ध न हो सकने पर अतिथिगृह का आवंटन 
          होता है तो उनसे मूल वेतन की 10 प्रतिशत धनराशि प्रतिमाह किराये 
          के रूप में ली जायेगी। </font>
          <a href="#">
          <font color="#000000">a</font></a></font><a href="#"><font face="Kruti Dev 010">संख्या-एम-3595/32-3-99 
          दिनांक 26.05.1999</font></a></span></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="47" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" height="47" class="style2"><div align="justify">
          <span style="font-weight: 400">
          <font color="#000000" face="Kruti Dev 010">
          सेवानिवृत्त मुख्य सचिवों के उ0प्र0 सदन, नई दिल्ली में कक्ष 
          आवंटन हेतु। </font>
          <a href="#">
          <font face="Kruti Dev 010">
          संख्या-एम-1865/32-3-05 दिनांक 14.03.2005</font></a></span></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="47" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" height="47" class="style2"><div align="justify">
          <span style="font-weight: 400">
          <font face="Kruti Dev 010" color="#000000">नई दिल्ली 
          स्थित उ0प्र0 सदन/उ0प्र0 भवन में भोजन की दर। </font><a href="#">
          <font face="Kruti Dev 010">
          संख्या-एम-2045/32-3-2006-57/81 दिनांक 21.03.2006</font></a></span></div></td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="85" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" height="85" class="style2"> 
                    <p align="justify">
          <span style="font-weight: 400">
          <font face="Kruti Dev 010" color="#000000">प्रदेश के 
          सार्वजनिक उपक्रमों/निगमों के निदेशक मण्डल में गैर सरकारी 
          उपाध्यक्षें जो विधान मण्डल के सदस्य नही है अथवा जिन्हें 
          मंत्री, राज्यमंत्री या उपमंत्री स्तर प्राप्त नही है हेतु 
          अतिथिगृह के किराये का दर का निर्धारण। </font><a href="#">
          <font face="Kruti Dev 010">
          संख्या-एम-6991/32-3-07-47/2005 दिनांक 18.07.2007</font></a></span></p>                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="43" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" height="43" class="style2"> 
                    <p align="justify">
          <span style="font-weight: 400">
          <font face="Kruti Dev 010" color="#000000">वि0अ0गृह, डालीबाग, 
          लखनऊ में मा0 पूर्व विधायकगण हेतु किराया का निर्धारण। </font><a href="#">
          <font face="Kruti Dev 010">
          संख्या-एम-9362/32-3-2006-2(एन0टी0)/92टी0सी0 दिनांक 
          13.10.2006</font></a></span></p>                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="42" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" height="42" class="style2"> 
                    <p align="justify"><span style="font-weight: 400">
          <font face="Kruti Dev 010">
          <font color="#000000">निजी/सरकारी आवास आवंटित महानुभावों के 
          लिए लखनऊ स्थित अतिथिगृह में आवंटन हेतु कार्यालय-ज्ञाप </font>
          <a href="#">
          <font color="#000000">a</font></a></font><a href="#"><font face="Kruti Dev 010">संख्या-एम-4065/32- 
          3-2008 दिनांक 30.04. 2008</font></a></span></p>                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="60" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" height="60" class="style2"> 
                    <p align="justify"><span style="font-weight: 400"><font face="Kruti Dev 010"> 
          <font color="#000000"><span lang="en-us">
          राज्य सम्पत्ति विभाग के नियंत्रणाधीन अतिथिगृहो की साज-सज्जा 
          हेतु वित्तीय वर्ष 2008-09 के लिए बजट की स्वीकृति/निदेशन।&nbsp;
          </span></font>
          <a href="#">
          संख्या-एम-6106/32-3-2008-98/07 दिनांक 13.03.2008</a><font color="#0000FF"> </font><font color="#000000"> 
          एवं </font></font><a href="#">
          <font face="Kruti Dev 010">
          संख्या-एम- 379/32- 3-2008-98/07 दिनांक 09.05.2008</font></a></span></p>                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="6%" height="60" class="style2"> 
                    <div align="center"></div>                  </td>
                  <td width="84%" height="60" class="style2"> 
                    <p align="justify"><span style="font-weight: 400">
          <font face="Kruti Dev 010">
          <font color="#000000">विधायक निवासों की साज-सज्जा हेतु धनराशि 
          की स्वीकृति/निदेशन के संबंध में। </font>
          <a href="#">
          संख्या-एम-3790/32- 3-2008-97-2007 
          दिनांक 15.04. 2008</a><font color="#000000"> एवं 
          कार्यालय-ज्ञाप </font> </font>
          <a href="#">
          <font face="Kruti Dev 010">
          संख्या-एम-4420/32-3-2008-97/2007 दिनांक 16.05.2008 </font>
          </a></span></p>                  </td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>