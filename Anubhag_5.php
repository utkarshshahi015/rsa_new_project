<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">राज्य संपत्ति विभाग अनुभाग - 5</h2>
               <table border="1" width="100%">
        <tbody><tr>
                  <td width="31%" align="center"><b>नाम </b></td>
                  <td width="199" align="center"><b>पदनाम </b></td>
                  <td width="98" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="21%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
        <tr>
          <td>श्री रमेश चन्द्र साहू</td>
          <td align="center">अनुभाग अधिकारी</td>
          <td align="center">
                    -</td>
          <td width="179" align="center">
                    <span style="font-family: Times New Roman; font-size: 13pt">
          9454413597</span></td>
        </tr>
        <tr>
          <td>श्रीमती प्रथमा सिंह </td>
          <td align="center">समीक्षा अधिकारी </td>
          <td align="center">
                    &nbsp;</td>
          <td width="179" align="center">
                    9454413973</td>
        </tr>
        <tr>
          <td>श्री अजय कुमार भारती </td>
          <td align="center">सहायक समीक्षा अधिकारी</td>
          <td align="center">
                    &nbsp;</td>
          <td width="179" align="center">
                    9454419391</td>
        </tr>
        <tr>
                  <td width="31%" align="center">
          <p align="left">श्री अजीत कुमार 
          श्रीवास्तव</p></td>
                  <td width="199" align="center">वरिष्ठ स्टोर कीपर </td>
                  <td width="98" align="center"> 
                    &nbsp;</td>
                  <td width="101" align="center">
          <p align="right">9454421025</p></td>
                </tr>
        <tr>
                  <td width="25%" align="left">श्री पारस राम यादव </td>
                  <td width="184" align="center">वरिष्ठ स्टोर कीपर </td>
                  <td width="110" align="center">&nbsp;</td>
                  <td width="101" align="center">
          <p align="right">9454421230</p></td>
                </tr>
        <tr>
                  <td width="25%" align="center">
          <p align="left">श्री निसार हुसैन </p></td>
                  <td width="184" align="center">स्टोर कीपर </td>
                  <td width="110" align="center"> 
                    &nbsp;</td>
                  <td width="101" align="center">
          <p align="right">9454421045</p></td>
                </tr>
        <tr>
                  <td width="25%" align="center">
          <p align="left">श्री आनंद कुमार </p></td>
                  <td width="184" align="center">सहायक स्टोर कीपर </td>
                  <td width="110" align="center"> 
                    &nbsp;</td>
                  <td width="101" align="center">
          <p align="right">9454421064</p></td>
                </tr>
        <tr>
          <td>श्री वीरेंद्र सिंह </td>
          <td align="center">&nbsp;कंप्यूटर ऑपेरटर</td>
          <td align="center">
                    &nbsp;</td>
          <td width="179" align="center">
                    9454421028</td>
        </tr>
        <tr>
          <td>श्री सुरेश शर्मा </td>
          <td align="center">&nbsp;कंप्यूटर ऑपेरटर</td>
          <td align="center">
                    &nbsp;</td>
          <td width="179" align="center">
                    9454421029</td>
        </tr>
        <tr>
          <td>श्री रामचन्द्र तिवारी </td>
          <td align="center">&nbsp;कंप्यूटर ऑपेरटर</td>
          <td align="center">
                    &nbsp;</td>
          <td width="179" align="center">
                    9454421019</td>
        </tr>
        <tr>
          <td>&nbsp;कुमारी रूबी </td>
          <td align="center">सहायक स्टोर कीपर </td>
          <td align="center">
                    &nbsp;</td>
          <td width="179" align="center">
                    9454421120</td>
        </tr>
        <tr>
          <td>श्री ओम प्रकाश </td>
          <td align="center">कनिष्ठ सहायक </td>
          <td align="center">
                    &nbsp;</td>
          <td width="179" align="center">
                    9454421205</td>
        </tr>
      </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>