<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         <font face="Verdana, Arial, Helvetica, sans-serif">
            <a href="<?php echo url(); ?>" class="style2">
          <font color="#000000">Back</font></a></font>
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">राज्य सम्पत्ति अधिकारी कैम्प कार्यालय</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000" height="277" >
            <tbody><tr bgcolor="#006699">
                  <td width="30%" align="center"><b>नाम </b></td>
                  <td width="136" align="center"><b>पदनाम </b></td>
                  <td width="119">
          <p align="center"><b>कार्यालय नंबर&nbsp; </b></p></td>
                  <td width="22%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
              <td height="26" align="left">
              श्री तिलक राज </td>
                  <td height="26">निजी सचिव </td>
                  <td height="26" align="center">
          <p>0522 - 2238203, <br>
          फैक्स न0 . 0522 - 2238417, 2213456</p></td>
                  <td height="26">
                  <p align="center"><font face="Times New Roman">9454410322</font></p></td>
                </tr>
            <tr>
              <td height="26" align="left">
        <p align="center">-</p></td>
                  <td height="26"><p>अपर निजी सचिव</p></td>
                  <td height="26" align="center">0522 - 2238203</td>
                  <td height="26"><p align="center">
                  -</p></td>
                </tr>
            <tr>
              <td height="18" align="left"><p>श्री राधेश्याम राम </p></td>
                  <td height="18" align="left"><p>वरिष्ठ सहायक </p></td>
                  <td height="18" align="center">0522 - 2238203</td>
                  <td height="18"><p align="center">
          <font face="Times New Roman">9415757924</font></p></td>
                </tr>
            <tr>
              <td height="14" align="left">श्री पूर्णमासी यादव </td>
                  <td height="14" align="left"><p>
                  कनिष्ठ सहायक </p></td>
                  <td height="14" align="center">0522 - 2238203</td>
                  <td height="14">
          <p align="center"><font face="Times New Roman">9455211041</font></p></td>
                </tr>
            <tr>
              <td height="18" align="left">श्री राम शरण सिंह </td>
                  <td height="18" align="left">कंप्यूटर ऑपरेटर </td>
                  <td height="18" align="center">0522 - 2238203</td>
                  <td height="18"><p align="center">
          <font face="Times New Roman">-</font></p></td>
                </tr>
            <tr>
              <td height="18" align="left"><p>श्री विजय कुमार कनौजिया</p></td>
                  <td height="18" align="left">
          <p>&nbsp;बियरर </p></td>
                  <td height="18" align="center">0522 - 2238203</td>
                  <td height="18"><p align="center">
          <font face="Times New Roman">&nbsp;-</font></p></td>
                </tr>
            <tr>
              <td height="18" align="left"><p>श्री गेना लाल ी </p></td>
                  <td height="18" align="left"><p>चपरास</p></td>
                  <td height="18" align="center">0522 - 2238203</td>
                  <td height="18"><p align="center">
          <font face="Times New Roman">9794730238</font></p></td>
                </tr>
            <tr>
              <td height="18" align="left"><p>श्री राजेंद्र कुमार टम्टा </p></td>
                  <td height="18" align="left"><p>सफाई कर्मी </p></td>
                  <td height="18" align="center">0522 - 2238203</td>
                  <td height="18"><p align="center">
          <font face="Times New Roman">9889903489</font></p></td>
                </tr>
            <tr>
              <td height="18" align="left"><p>श्री सिद्धनाथ वर्मा </p></td>
                  <td height="18" align="left"><p>अशिक्षित श्रमिक </p></td>
                  <td height="18" align="center">0522 - 2238203</td>
                  <td height="18"><p align="center">
          <font face="Times New Roman">9936881276</font></p></td>
                </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>