<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">सहायक राज्य संपत्ति अधिकारी कैंप कार्यालय</h2>
               <table width="97%" border="1" align="center" bordercolor="#000000">
            <tbody><tr bgcolor="#006699">
                  <td width="28%" align="center"><b>नाम </b></td>
                  <td width="186" align="center"><b>पदनाम </b></td>
                  <td width="112" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="90" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
              <td>श्री सुधीर कुमार 
        रूंगटा </td>
                  <td>सहा0 राज्य 
          संपत्ति अधिकारी</td>
                  <td align="center">2237591</td>
                  <td align="center"><span lang="en-us">
          9454412032</span>         </td>
                </tr>
            <tr>
              <td>श्री नित्यानंद तिवारी </td>
                  <td>स्वागती </td>
                  <td align="center">2237591</td>
                  <td align="center">9454421262</td>
                </tr>
            <tr>
              <td>कुमारी अंकिता श्रीवास्तव </td>
                  <td>कनिष्ठ सहायक </td>
                  <td align="center">2237591</td>
                  <td align="center">9454421141</td>
                </tr>
            <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="" align="left">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री दिलीप 
  कुमार पांडेय </font></span></span>
  </td>
  <td style="width:175px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="" align="left">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">कंप्यूटर 
  ऑपेरटर</font></span></span></td>
  <td style="width:101px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <p align="center">2237591</p></td>
  <td style="width:79px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  &nbsp;</td>
            </tr>
            <tr>
              <td>श्री राम दुलारो मिश्रा </td>
                  <td>फर्रास </td>
                  <td align="center">2237591</td>
                  <td align="center">9616920121</td>
                </tr>
            <tr>
              <td>श्री ललित पाल </td>
                  <td>फर्रास </td>
                  <td align="center">2237591</td>
                  <td align="center">9307272924</td>
                </tr>
            <tr>
              <td>श्री रमेश सिंह </td>
                  <td>चतुर्थ श्रेणी कर्मचारी </td>
                  <td align="center">2237591</td>
                  <td align="center">9984789422</td>
                </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>