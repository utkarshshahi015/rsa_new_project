<!-- <!DOCTYPE html> -->


 
      <!--/.nav-wrapper-->

      <section class="wrapper carousel-wrapper">
         <div class="container common-container four_content carousel-container">
            <div id="flexCarousel" class="flexslider carousel">
               <ul class="slides">
                  <li><a target="_blank" href="http://digitalindia.gov.in/" title="Digital India, External Link that opens in a new window"><img src="assets/images/carousel/digital-india.png" alt="Digital India"></a></li>
                  <li><a target="_blank" href="http://www.makeinindia.com/" title="Make In India, External Link that opens in a new window"> <img src="assets/images/carousel/makeinindia.png" alt="Make In India"></a></li>
                  <li><a target="_blank" href="http://india.gov.in/" title="National Portal of India, External Link that opens in a new window"><img src="assets/images/carousel/india-gov.png" alt="National Portal of India"></a></li>
                  <li><a target="_blank" href="http://goidirectory.nic.in/" title="GOI Web Directory, External Link that opens in a new window"><img src="assets/images/carousel/goidirectory.png" alt="GOI Web Directory"></a></li>
                  <li><a target="_blank" href="https://data.gov.in/" title="Data portal, External Link that opens in a new window" ><img src="assets/images/carousel/data-gov.png" alt="Data portal"></a></li>
                  <li><a target="_blank" href="https://mygov.in/" title="MyGov, External Link that opens in a new window"><img src="assets/images/carousel/mygov.png" alt="MyGov Portal"></a></li>
               </ul>
            </div>
         </div>
      </section>
      <!--/.carousel-wrapper-->
      <footer class="wrapper footer-wrapper">
         <div class="footer-top-wrapper">
            <div class="container common-container four_content footer-top-container">
               <ul>
                  <li><a href="inner.html">प्रतिक्रिया</a></li>
                  <li><a href="inner.html">वेबसाइट पॉलिसी</a></li>
                  <li><a href="inner.html">नियम और शर्तें </a></li>
                  <!-- <li><a href="inner.html">Contact Us</a></li> -->
                  <li><a href="inner.html">सहायता</a></li>
                  <li><a href="inner.html">वेब सूचना प्रबंधक                  </a></li>
                  <!-- <li><a href="inner.html">Visitor Pass</a></li> -->
                  <!-- <li><a href="inner.html">FAQ</a></li> -->
                  <!-- <li><a href="inner.html">Disclaimer</a></li> -->
               </ul>
            </div>
         </div>
         <div class="footer-bottom-wrapper">
            <div class="container common-container four_content footer-bottom-container">
               <div class="footer-content clearfix">
                  <div class="copyright-content"> 
                     <strong> इस वेब साइट का कंटेंट राज्य संपत्ति विभाग उत्तर प्रदेश, लखनऊ द्वारा प्रकाशित एवं संचालित किया जाता है।'</strong>
                     <span>इस वेबसाइट के बारे में किसी भी प्रश्न के लिए, कृपया "वेब सूचना प्रबंधक" से संपर्क करें।
                     <!-- <a target="_blank" title="NIC, External Link that opens in a new window" href="http://www.nic.in/"> -->
                     <strong>कापीराइट © राज्य संपत्ति विभाग , उत्तर प्रदेश को सभी अधिकार सुरक्षित हैं।</strong></span> 
                  </div>
                  <!-- <div class="logo-cmf"> 
                     <a target="_blank" href="http://cmf.gov.in/" title="External link that opens in new tab, cmf"><img alt="cmf logo" src="assets/images/cmf-logo.png"></a>
                   </div> -->
               </div>
            </div>
         </div>
      </footer>
      <!--/.footer-wrapper-->
      <!-- jQuery v1.11.1 -->
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js" integrity="sha512-nhY06wKras39lb9lRO76J4397CH1XpRSLfLJSftTeo3+q2vP7PaebILH9TqH+GRpnOhfAGjuYMVmVTOZJ+682w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
      <!-- <script src="assets/vendor/jquery/jquery.min.js"></script> -->
      <!-- <script src="<?php # echo url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
      <!-- jQuery Migration v1.4.1 -->
      <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
      <!-- jQuery v3.6.0 -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <!-- jQuery Migration v3.4.0 -->
      <script src="https://code.jquery.com/jquery-migrate-3.4.0.min.js"></script>
      
      <script src="assets/js/jquery-accessibleMegaMenu.js"></script>
      <script src="assets/js/framework.js"></script>
      <script src="assets/js/jquery.flexslider.js"></script>
      <script src="assets/js/font-size.js"></script>
      <script src="assets/js/swithcer.js"></script>
      <script src="theme/js/ma5gallery.js"></script>
      <script src="assets/js/megamenu.js"></script>
      <script src="theme/js/easyResponsiveTabs.js"></script>
      <script src="theme/js/custom.js"></script>
      <!-- <script src="assets/js/main.js"></script> -->
      <script src="assets/js/main.js"></script>
      <!-- <script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script> -->

   </body>
</html>