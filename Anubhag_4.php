<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">सहायक राज्य संपत्ति अधिकारी कैंप कार्यालय</h2>
               <table border="1" width="100%">
        <tbody><tr>
                  <td width="31%" align="center"><b>नाम </b></td>
                  <td width="198" align="center"><b>पदनाम </b></td>
                  <td width="113" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="114" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
        <tr>
          <td>श्री दर्शन पाल आर्य</td>
          <td>अनुभाग अधिकारी, अनुभाग - 4</td>
          <td align="center">
                    <font face="Times New Roman" style="font-size: 13pt">2213455</font></td>
          <td width="114" align="center">
                    <span lang="en-us">9454412247</span></td>
        </tr>
        <tr>
          <td>श्री सुधाकर </td>
          <td>समीक्षा अधिकारी </td>
          <td align="center">
                    <font face="Times New Roman" style="font-size: 13pt">2213455</font></td>
          <td width="114" align="center">
                    &nbsp;</td>
        </tr>
        <tr>
                  <td width="31%" align="center">
          <p align="left">श्री चेतन्य स्वरूप </p></td>
                  <td width="198" align="center">
          <p align="left">वरिष्ठ स्टोर कीपर </p></td>
                  <td width="113" align="center"> 
                    <font face="Times New Roman" style="font-size: 13pt">2213455</font></td>
                  <td width="114" align="center">9454421042</td>
                </tr>
        <tr>
                  <td width="31%" align="left">श्री मोहम्मद आरिफ सिददीकी </td>
                  <td width="198" align="left">कनिष्ठ सहायक </td>
                  <td width="113" align="center"> 
                    <font face="Times New Roman" style="font-size: 13pt">2213455</font></td>
                  <td width="114" align="center">9454421234</td>
                </tr>
        <tr>
                  <td width="25%" align="left">श्री ज्ञानेंद्र कुमार मिश्रा </td>
                  <td width="198" align="left">कनिष्ठ सहायक </td>
                  <td width="113" align="center"> 
                    <font face="Times New Roman" style="font-size: 13pt">2213455</font></td>
                  <td width="114" align="center">9454421112</td>
                </tr>
        <tr>
                  <td width="25%" align="left">सुश्री जयती श्रीवास्तव </td>
                  <td width="184" align="left">सहायक स्टोर कीपर </td>
                  <td width="110" align="center">
                    <font face="Times New Roman" style="font-size: 13pt">2213455</font></td>
                  <td width="101" align="center">9454421016</td>
                </tr>
        <tr>
      <td valign="top" style="border-right:.5pt solid windowtext; border-top:.5pt solid windowtext; border-bottom:.5pt solid windowtext; width:202;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in"> 
    सुश्री अर्चना वर्मा </td>
      <td valign="top" style="border-right:.5pt solid windowtext; border-top:.5pt solid windowtext; border-bottom:.5pt solid windowtext; width:167px;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in">  
    कनिष्ठ सहायक </td>
      <td valign="top" style="border-right:.5pt solid windowtext; border-top:.5pt solid windowtext; border-bottom:.5pt solid windowtext; width:99px;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" align="center"> 
                    <font face="Times New Roman" style="font-size: 13pt">2213455</font></td>
      <td valign="top" style="border-right:.5pt solid windowtext; border-top:.5pt solid windowtext; border-bottom:.5pt solid windowtext; width:91px;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;height:6.7pt; padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in"> &nbsp;</td>
              </tr>
      </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>