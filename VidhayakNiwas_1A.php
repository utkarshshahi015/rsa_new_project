<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">विधयक निवास-1 (अ)</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="177" align="center"><b>पदनाम </b></td>
                  <td width="123" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="111" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
      <td style="width:206px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p class="MsoNormal">श्रीमती सुधा कुमार</p>      </td>
      <td style="width:178px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p class="MsoNormal">मुख्य व्यवस्था 
    अधिकारी </p>      </td>
      <td style="width:124px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
      <p align="center">2625914</p></td>
      <td style="width:112px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
      <p align="center"> 
      <font face="Kruti Dev 010">
                    <span style="font-family: Times New Roman"> 
                    9453044801</span></font></p></td>
            </tr>
            <tr>
      <td style="width:206px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:.25in; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4"><span class="SpellE"><span style="font-size:14.0pt;
  font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:Arial">JhdkUr</span></span><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:
  Arial"> '<span class="SpellE">kekZ</span><o:p></o:p></span></font></p>      </td>
      <td style="width:178px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:.25in; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4" color="006699"><span class="SpellE"><span style="font-size:14.0pt;
  font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:Arial;color:black">LVksjdhij</span></span><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:
  Arial;color:black"> ¼T;s"B Js.kh½<o:p></o:p></span></font></p>      </td>
      <td style="width:124px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:.25in; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p align="center">2625914</p></td>
      <td style="width:112px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:.25in; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        &nbsp;</td>
            </tr>
            <tr>
      <td style="width:206px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4"><span class="SpellE"><span style="font-size:14.0pt;
  font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:Arial">jke izrki ;kno</span></span></font><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:
  Arial"><o:p></o:p></span></p>      </td>
      <td style="width:178px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4" color="006699"><span class="SpellE"><span style="font-size:14.0pt;
  font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:Arial;color:black">Loxrh</span></span><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:
  Arial;color:black"><o:p></o:p></span></font></p>      </td>
      <td style="width:124px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p align="center">2625914</p></td>
      <td style="width:112px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        &nbsp;</td>
            </tr>
            <tr>
      <td style="width:206px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="58"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">clUr ;kno<o:p></o:p></span></font></p>      </td>
      <td style="width:178px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="58"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4" color="006699"><span class="SpellE"><span style="font-size:14.0pt;
  font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:Arial;color:black">Loxrh</span></span><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:
  Arial;color:black"><o:p></o:p></span></font></p>      </td>
      <td style="width:124px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
      <p align="center">2625914</p></td>
      <td style="width:112px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="58"> 
        &nbsp;</td>
            </tr>
            <tr>
      <td style="width:206px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">lqJh fMEiy flag<o:p></o:p></span></font></p>      </td>
      <td style="width:178px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4" color="006699"><span class="SpellE"><span style="font-size:14.0pt;
  font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:Arial;color:black">dk0 DydZ 
          de LVksjdhij</span></span><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:
  Arial;color:black"><o:p></o:p></span></font></p>      </td>
      <td style="width:124px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:.25in; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p align="center">2625914</p></td>
      <td style="width:112px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        &nbsp;</td>
            </tr>
            <tr>
      <td style="width:206px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">Jherh vatw f}osnh<o:p></o:p></span></font></p>      </td>
      <td style="width:178px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4" color="006699"><span class="SpellE"><span style="font-size:14.0pt;
  font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:Arial;color:black">l0 LVksjdhij</span></span><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:
  Arial;color:black"><o:p></o:p></span></font></p>      </td>
      <td style="width:124px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
      <p align="center">2625914</p></td>
      <td style="width:112px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        &nbsp;</td>
            </tr>
            <tr>
      <td style="width:206px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">Jherh lq/kk ;kno<o:p></o:p></span></font></p>      </td>
      <td style="width:178px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4" color="006699"><span class="SpellE"><span style="font-size:14.0pt;
  font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:Arial;color:black">l0 LVksjdhij</span></span><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:
  Arial;color:black"><o:p></o:p></span></font></p>      </td>
      <td style="width:124px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:.25in; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p align="center">2625914</p></td>
      <td style="width:112px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        &nbsp;</td>
            </tr>
            <tr>
      <td style="width:206px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">lqJh izhfr ;kno<o:p></o:p></span></font></p>      </td>
      <td style="width:178px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4" color="006699"><span class="SpellE"><span style="font-size:14.0pt;
  font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:Arial;color:black">l0 LVksjdhij</span></span><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:
  Arial;color:black"><o:p></o:p></span></font></p>      </td>
      <td style="width:124px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p align="center">2625914</p></td>
      <td style="width:112px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="55"> 
        &nbsp;</td>
            </tr>
            <tr>
      <td style="width:206px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="48"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">lS0 <span class="SpellE">vyh</span> <span class="SpellE">vdcj</span> <span class="SpellE">fjt+oh</span><o:p></o:p></span></font></p>      </td>
      <td style="width:178px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="48"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4" color="006699"><span class="SpellE"><span style="font-size:14.0pt;
  font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:Arial;color:black">dfu"B</span></span><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:
  Arial;color:black"> <span class="SpellE">fyfid</span><o:p></o:p></span></font></p>      </td>
      <td style="width:124px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:15.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
      <p align="center">2625914</p></td>
      <td style="width:112px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in" height="48"> 
        &nbsp;</td>
            </tr>
            <tr>
      <td style="width:206px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4"><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;
  mso-bidi-font-family:Arial">eks0 <span class="SpellE">vglu</span><o:p></o:p></span></font></p>      </td>
      <td style="width:178px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p class="MsoNormal"><font face="Kruti Dev 010" size="4" color="006699"><span class="SpellE"><span style="font-size:14.0pt;
  font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:Arial;color:black">dfu"B</span></span><span style="font-size:14.0pt;font-family:&quot;Kruti Dev 010&quot;;mso-bidi-font-family:
  Arial;color:black"> <span class="SpellE">fyfid</span><o:p></o:p></span></font></p>      </td>
      <td style="width:124px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:.25in; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p align="center">2625914</p></td>
      <td nowrap="" style="width:112px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        &nbsp;</td>
            </tr>
            <tr>
      <td style="width:206px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        श्री कमला कान्त पाण्डेय      </td>
      <td style="width:178px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        लिफ्टमैन      </td>
      <td nowrap="" style="width:124px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        <p align="center">2625914</p></td>
      <td nowrap="" style="width:112px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:18.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in"> 
        &nbsp;</td>
            </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>