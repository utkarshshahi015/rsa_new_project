<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">राज्य संपत्ति विभाग अनुभाग - 1</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000" height="15">
            <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="184" align="center"><b>पदनाम </b></td>
                  <td width="110" align="center"><b>कार्यालय नंबर</b></td>
                  <td width="101" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्री रविकेश चन्द्र </td>
                  <td width="184" align="left">अनुभाग अधिकारी </td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
                  <td width="101" align="center">9454413090</td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्री भगौती प्रसाद वर्मा </td>
                  <td width="184" align="left">समीक्षा अधिकारी </td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
                  <td width="101" align="center">9454412553</td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्री रमेश चन्द्र </td>
                  <td width="184" align="left">समीक्षा अधिकारी </td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
                  <td width="101" align="center">9454411770</td>
                </tr>
            <tr>
                  <td width="25%" align="left">सुश्री पूजा पाण्डेय </td>
                  <td width="184" align="left">समीक्षा अधिकारी </td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
                  <td width="101" align="center">9454410659</td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्री जीतेन्द्र कुमार श्रीवास्तव</td>
                  <td width="184" align="left">कंप्यूटर सहायक </td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
                  <td width="101" align="center">9454411745</td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्री सैयद मौहम्मद हैदर </td>
                  <td width="184" align="left">कंप्यूटर ऑपरेटर ग्रेड - ए </td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
                  <td width="101" align="center">9454421015</td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्री इशरत हुसैन </td>
                  <td width="184" align="left">कनिष्ठ सहायक </td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
                  <td width="101" align="center">9454421101</td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्री राजेश मोहन त्रिवेदी </td>
                  <td width="184" align="left">कनिष्ठ सहायक </td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
                  <td width="101" align="center">9454421021</td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्री श्रवण कुमार </td>
                  <td width="184" align="left">काउन्टर क्लर्क </td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
                  <td width="101" align="center">9454421090</td>
                </tr>
            <tr>
                  <td width="25%" align="left">श्रीमती अर्णिमा </td>
                  <td width="184" align="left">काउन्टर क्लर्क </td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
                  <td width="101" align="center">-</td>
                </tr>
      <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="" align="left">
  <p class="MsoNormal"><span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री 
  गोविन्द सिंह </font></span></span></p>
  </td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="" align="left">
  <p class="MsoNormal"><span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">कनिष्ठ 
  लिपिक </font></span> </span></p>
  </td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
  <td style="width:80px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span style="font-weight:400"><font color="#000000">9839446869 </font> </span> <o:p></o:p>
  </td>
            </tr>
      <tr>
  <td style="border-left:1.0pt solid windowtext; border-right:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:197px;border-top:medium none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">श्री पंकज 
  कुमार </font></span></span>
  </td>
  <td style="width:199px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  <span class="style131">
  <span style="mso-bidi-font-size:16.0pt;color:black"><font size="3">कनिष्ठ 
  सहायक</font></span></span></td>
                  <td width="110" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></td>
  <td style="width:80px;border-top:medium none;border-left:medium none;
  border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding-left:5.4pt; padding-right:5.4pt; padding-top:0in; padding-bottom:0in" height="20" nowrap="">
  &nbsp;</td>
            </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>