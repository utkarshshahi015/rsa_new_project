<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
          <div class="col-md-3 mb-3">
            <ul class="nav nav-pills flex-column" id="myTab" role="tablist">
              <li class="nav-item"> <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">विभाग के अधीन कालोनियाँ</a> </li>
              <li class="nav-item"> <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">आवासों का आवंटन </a> </li>
              <li class="nav-item"> <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">अतिथि गृह </a> </li>
              <!-- <li class="nav-item"> <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">अनुरक्षण  </a> </li> -->
              <li class="nav-item"> <a class="nav-link" id="vachile-tab" data-toggle="tab" href="#vachile" role="tab" aria-controls="contact" aria-selected="false">वाहनो का प्रबंधन </a> </li>
               <li class="nav-item"> <a class="nav-link" id="nideshalay-tab" data-toggle="tab" href="#nideshalay" role="tab" aria-controls="contact" aria-selected="false">निदेशालय का विवरण</a> </li> 
              <li class="nav-item"> <a class="nav-link" id="off-directory-tab" data-toggle="tab" href="#off-directory" role="tab" aria-controls="contact" aria-selected="false">अधिकारी / कर्मचारी डायरेक्टरी  </a> </li>
              <li class="nav-item"> <a class="nav-link" id="rules-tab" data-toggle="tab" href="#rules" role="tab" aria-controls="contact" aria-selected="false">विभाग के नियम / नियमावली </a> </li>
              <li class="nav-item"> <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contactnumber" role="tab" aria-controls="contactnumber" aria-selected="false">महत्वपूर्ण दूरभाष सo  </a> </li>
              <li class="nav-item"> <a class="nav-link" id="jansuchna-tab" data-toggle="tab" href="#jansuchna" role="tab" aria-controls="contact" aria-selected="false">जनसूचना अधिकारी </a> </li>
              <li class="nav-item"> <a class="nav-link" id="rti-tab" data-toggle="tab" href="#rti" role="tab" aria-controls="contact" aria-selected="false">सूचना का अधिकार </a> </li>
              <!-- <li class="nav-item"> <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">सेवा योजन </a> </li> -->
	             <li class="nav-item"> <a class="nav-link" id="guarante-tab" data-toggle="tab" href="#guarante" role="tab" aria-controls="contact" aria-selected="false">जनहित गारंटी अधिनियम -2011 के सम्बंधित राज्य संपत्ति विभाग के अधिकारीगण </a> </li>
	             <li class="nav-item"> <a class="nav-link" id="guarante_sevaye-tab" data-toggle="tab" href="#guarante_sevaye" role="tab" aria-controls="contact" aria-selected="false">
		            जनहित गारंटी अधिनियम -2011 के अधीन राज्य संपत्ति विभाग के सेवाएँ </a> </li>
            </ul>
          </div>
          <div class="col-md-9">
            <div class="tab-content" id="myTabContent">
               <?php include('section/department-colony.php')?>
               <?php include('section/houseallotment.php')?>
               <?php include('section/guesthouse.php')?>
               <?php include('section/maintenance.php')?>
               <?php include('section/vachile.php')?>
               <?php include('section/nideshalay.php')?>
               <?php include('section/officer-directory.php')?>
               <?php include('section/department-rules.php')?>
               <?php include('section/imp-contactno.php')?>
               <?php include('section/jansuchna.php')?>
               <?php include('section/rti.php')?>
               <?php include('section/sewayojan.php')?>
	             <?php include('section/gaurante.php')?>
	             <?php include('section/gaurante_sevaye.php')?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>