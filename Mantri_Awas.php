<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">सहायक राज्य संपत्ति अधिकारी कैंप कार्यालय</h2>
               <table width="100%" border="1" align="center" bordercolor="#000000">
            <tbody><tr>
                  <td width="29%" align="center"><b>नाम </b></td>
                  <td width="24%" align="center"><b>पदनाम </b></td>
                  <td width="19%" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="21%" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
            <tr>
              <td>श्री संजय चौधरी</td>
                  <td>&nbsp;व्यवस्था अधिकारी</td>
                  <td><p align="center" class="MsoNormal"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font>   </p></td>
                  <td align="center"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">9450004700</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font> </td>
                </tr>
            <tr>
              <td>श्री पुष्प चंद्र त्रिपाठी </td>
                  <td>स्टोर कीपर </td>
                  <td><p align="center" class="MsoNormal"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font>   </p></td>
                  <td align="center"> 
                    <span style="font-size: 13pt; font-family: Times New Roman">
          9454421215</span></td>
                </tr>
      <tr>
              <td>श्री राज बहादुर</td>
                  <td>सहायक स्टोर कीपर </td>
                  <td><p align="center" class="MsoNormal"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font>   </p></td>
                  <td align="center"> 
                    <span style="font-size: 13pt">9454421217</span></td>
                </tr>
      <tr>
              <td>श्री रणजीत कुमार </td>
                  <td>सहायक स्टोर कीपर </td>
                  <td><p align="center" class="MsoNormal"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font>   </p></td>
                  <td align="center"> 
                    <span style="font-size: 13pt">9454421219</span></td>
                </tr>
      <tr>
              <td>कुमारी अंजलि यादव </td>
                  <td>सहायक स्टोर कीपर </td>
                  <td><p align="center" class="MsoNormal"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font>   </p></td>
                  <td align="center"> 
                    <span style="font-size: 13pt">9454421218</span></td>
                </tr>
      <tr>
              <td>श्री मोहम्मद नुसरत </td>
                  <td>कंप्यूटर ऑपेरटर </td>
                  <td><p align="center" class="MsoNormal"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font>   </p></td>
                  <td align="center"> 
                    <span style="font-size: 13pt">9454421223</span></td>
                </tr>
      <tr>
              <td>श्री हारून खां </td>
                  <td>कंप्यूटर ऑपेरटर </td>
                  <td><p align="center" class="MsoNormal"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font>   </p></td>
                  <td align="center"> 
                    <span style="font-size: 13pt">9454421050</span></td>
                </tr>
      <tr>
              <td>श्री विनय कुमार </td>
                  <td>टेलीफोन ऑपेरटर </td>
                  <td><p align="center" class="MsoNormal"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font>   </p></td>
                  <td align="center"> 
                    <span style="font-size: 13pt">9454421225</span></td>
                </tr>
      <tr>
              <td>श्री राजेन्द्र प्रसाद सिंह यादव</td>
                  <td>&nbsp;काउंटर क्लर्क </td>
                  <td><p align="center" class="MsoNormal"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font>   </p></td>
                  <td align="center"> 
                    <span style="font-size: 13pt">9454421220</span></td>
                </tr>
      <tr>
              <td>श्री सुभाष कुमार रावत </td>
                  <td>कनिष्ठ सहायक </td>
                  <td><p align="center" class="MsoNormal"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font>   </p></td>
                  <td align="center"> 
                    <span style="font-size: 13pt">9454421179</span></td>
                </tr>
      <tr>
              <td>श्री अंकित राज </td>
                  <td>कनिष्ठ सहायक </td>
                  <td><p align="center" class="MsoNormal"> 
                    <span style="font-family: Times New Roman">
                    <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                    </font>   </p></td>
                  <td align="center"> 
                    <span style="font-size: 13pt">9454421103</span></td>
                </tr>
            </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>