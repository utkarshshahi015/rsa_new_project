<?php include('header.php')?>
  <!-- ======= Hero Section ======= -->
  <section id="about" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="row">
        <div class="col-md-12">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
              <div class="carousel-item"> <img src="assets/img/Lokbhavan.png" alt="" width="10"> </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a>
            <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">
        <div class="row">
         
          <div class="col-md-12">
            <div class="tab-content" id="myTabContent">
              <h2 class="row justify-content-md-center">ट्रांजिट हॉस्टल</h2>
               <table width="100%" border="1" align="center" cellpadding="1" cellspacing="1" bordercolor="#000000" style="margin-left:-.5pt;border-collapse:collapse;mso-table-layout-alt:fixed;
 mso-padding-alt:0in 0in 0in 0in">
                <tbody><tr>
                  <td width="25%" align="center"><b>नाम </b></td>
                  <td width="149" align="center"><b>पदनाम </b></td>
                  <td width="120" align="center"><b>कार्यालय नंबर&nbsp; </b></td>
                  <td width="118" align="center"><b>मोबाइल नंबर</b></td>
                </tr>
                <tr style="mso-yfti-irow:0;height:2.75pt" valign="top">
                  <td nowrap="" style="border-right:1.0pt solid windowtext; border-top:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:179;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;height:2.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
                  श्री राहुल तन्खा</td>
                      <td nowrap="" style="border-right:1.0pt solid windowtext; border-top:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:149px;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;height:2.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
                      व्यवस्था अधिकारी </td>
                      <td style="border-right:1.0pt solid windowtext; border-top:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:122px;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in;height:2.75pt" align="center">2209892</td>
                      <td style="border-right:1.0pt solid windowtext; border-top:1.0pt solid windowtext; border-bottom:1.0pt solid windowtext; width:120px;border-left:medium none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in;height:2.75pt" align="center"> 
                    <span style="font-family: Times New Roman; font-size: 13pt">
                    9415937778</span><font face="Times New Roman" style="font-size: 13pt">
                    </font> </td>
                    </tr>
                <tr style="mso-yfti-irow:2;height:2.75pt" valign="top">
                  <td nowrap="" style="width:179;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:2.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          श्री प्यारेलाल </td>
                      <td nowrap="" style="width:149px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:2.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
            टेलीफोन ऑपेरटर </td>
                      <td style="width:122px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:2.75pt" align="center">
                      2209892</td>
                      <td style="width:120px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:2.75pt" align="center">
                      <span style="font-family: Times New Roman; font-size: 13pt">9792293421</span></td>
                    </tr>
                <tr>
                  <td nowrap="" class="style10 style12" style="width:179;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:2.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
                  <span style="font-weight: 400"><font size="3">श्री 
                  संतोष यादव </font></span>                    </td>
                      <td nowrap="" class="style12 style14" style="width:149px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:2.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
                      <span style="font-weight: 400"><font size="3">सहायक स्टोर 
                      कीपर</font></span></td>
                      <td style="width:122px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:2.75pt" align="center">
                      2209892</td>
                      <td style="width:120px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:2.75pt" align="center">
                      <span style="font-family: Times New Roman; font-size: 13pt">9451144442</span></td>
                    </tr>
                <tr style="mso-yfti-irow:2;height:2.75pt" valign="top">
                  <td nowrap="" style="width:179;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:2.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
          श्री गोकरन प्रसाद </td>
                      <td nowrap="" style="width:149px;border-top:medium none;
  border-left:medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;height:2.75pt; padding-left:.75pt; padding-right:.75pt; padding-top:.75pt; padding-bottom:0in">
            स्वागती </td>
                      <td style="width:122px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:2.75pt" align="center">
                      2209892</td>
                      <td style="width:120px;border-top:medium none;border-left:
  medium none;border-bottom:1.0pt solid windowtext;border-right:1.0pt solid windowtext;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in;height:2.75pt" align="center">
                      <span style="font-family: Times New Roman; font-size: 13pt">7379140700</span></td>
                    </tr>
                </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- End #main -->
  <!-- ======= Footer ======= -->
  <?php include('footer.php')?>