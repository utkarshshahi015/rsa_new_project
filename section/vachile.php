<div class="tab-pane fade" id="vachile" role="tabpanel" aria-labelledby="vachile-tab">
                <h2 class="row justify-content-md-center">राज्य सम्पत्ति विभाग में वाहनो का प्रबन्धन</h2>
                <table width="100%" border="2" cellspacing="2" cellpadding="2">
                        <tbody><tr> 
                          <td width="5%" class="style2"> 
                            <div align="center">
								<font size="4" face="Kruti Dev 010" color="#000000">1</font></div>                          </td>
                          <td width="79%" class="style2">
                            <div align="Justify"><font color="#000000">
								<span style="font-weight: 400">राज्य सम्पत्ति 
								विभाग में वाहनों का आबंटन वाहन आबंटन नियमावली 8 
								सितम्बर 1965 (यथा संशोधित) के प्रविधानों के 
								अनुसार किया जाता है। </span></font></div>                          </td>
                        </tr>
                        <tr> 
                          <td width="5%" class="style2"> 
                            <div align="center">
								<font size="4" face="Kruti Dev 010" color="#000000">2</font></div>                          </td>
                          <td width="79%" class="style2">
                            <div align="Justify"><font color="#000000">
								<span style="font-weight: 400">वाहन आवंटन 
								नियमावली 1965 (यथा संशोधित) के अन्तर्गत मा0 
								मुख्यमंत्री/मंत्रीगण/राज्य मंत्रीगण/महाधिवक्ता/अपर 
								महाधिवक्ता, अध्यक्ष/सदस्य उ0प्र0 लोक सेवा आयोग/ 
								वरिष्ठ अधिकारीयों का वाहन उपलब्ध कराया जाता हैं।</span></font></div>                          </td>
                        </tr>
                        <tr> 
                          <td width="5%" class="style2"> 
                            <div align="center">
								<font size="4" face="Kruti Dev 010" color="#000000">3</font></div>                          </td>
                          <td width="79%" class="style2">
                            <div align="Justify"><font color="#000000">
								<span style="font-weight: 400">मा0 मुख्यमंत्री 
								जी के अनुमोदन से मंत्री/राज्यमंत्री स्तर प्राप्त 
								विभिन्न आयोगो/निगम/ स्वातशासी संस्थाओं के 
								अध्यक्षों/उपाध्यक्षें को भी वाहन की सुविधा 
								उपलब्ध करायी जाती हैं।</span></font></div>                          </td>
                        </tr>
                        <tr> 
                          <td width="5%" class="style2"> 
                            <div align="center">
								<font size="4" face="Kruti Dev 010" color="#000000">4</font></div>                          </td>
                          <td width="79%" class="style2">
                            <div align="Justify"><font color="#000000">
								<span style="font-weight: 400">भूतपूर्व मुख्य 
								मंत्रीगणों एवं विशिष्ठ व्यक्तियों की सुरक्षा हेतु 
								भी राज्य सम्पत्ति विभाग से वाहन सम्बद्ध किये जाते 
								हैं। </span></font></div>                          </td>
                        </tr>
                        <tr> 
                          <td width="5%" class="style2"> 
                            <div align="center">
								<font size="4" face="Kruti Dev 010" color="#000000">5</font></div>                          </td>
                          <td width="79%" class="style2">
                            <div align="Justify"><font color="#000000">
								<span style="font-weight: 400">राज्य सम्पत्ति 
								विभाग के विभागीय पूल में कुल 260 वाहन कार्यरत है 
								जिसमें 16 वाहन उ0प्र0 सदन नई दिल्ली, 02 वाहन उ0 
								प्र0 भवन नई दिल्ली तथा 03 वाहन कोलकाता स्थित 
								अतिथि गृह में व शेष वाहन लखनऊ पूल में कार्यरत 
								है।</span></font></div>                          </td>
                        </tr>
                        <tr> 
                          <td width="5%" class="style2"> 
                            <div align="center">
								<font size="4" face="Kruti Dev 010" color="#000000">6</font></div>                          </td>
                          <td width="79%" class="style2">
                            <div align="Justify"><font color="#000000">
								<span style="font-weight: 400">विभाग में वाहनों 
								की कमी होने पर कार्य को सुचारू रूप से चलाने के 
								लिए निजी एजेन्सियों से भी वाहन अनुबन्धित किए जाते 
								है। </span></font></div>                          </td>
                        </tr>
                        <tr> 
                          <td width="5%" class="style2"> 
                            <div align="center">
								<font size="4" face="Kruti Dev 010" color="#000000">7</font></div>                          </td>
                          <td width="79%" class="style2">
                            <div align="Justify"><font color="#000000">
								<span style="font-weight: 400">राज्य सम्पत्ति 
								विभाग के वाहनो का अनुरक्षण परिवहन निगम की 
								कार्यशाला से कराया जाता है। इसी प्रकार वाहनो में 
								पेट्रोल/मोबिलआयल/सिस्टम जी आदि की आपूर्ति भी 
								परिवहन निगम के डिपो से की जाती है।</span></font></div>                          </td>
                        </tr>
                      </tbody></table>



<p><strong><u>वाहन आपूर्ति हेतु सम्पर्क</u></strong> </p>
<table width="100%" border="2" cellspacing="2" cellpadding="2">
                        <tbody><tr valign="top" bgcolor="#006699"> 
                          <td width="28%" class="style2" align="center"> 
                            नाम/पद नाम</td>
                          <td width="26%" class="style2" align="center"> 
                            फोन नं0 (आफिस)<br>
&nbsp;</td>
                          <td width="20%" class="style2" align="center"> 
                            आवास/मो0 नं0</td>
                          <td width="19%" class="style2" align="center"> 
                            फैक्स नं</td>
                        </tr>
                        <tr valign="top"> 
                          <td width="28%" class="style2" height="66">
							<p align="center"><span style="font-weight: 400">
							<font color="#000000">श्री 
					अंबरीष श्रीवास्तव, मोटर सहायक</font></span></p></td>
                          <td width="26%" class="style2" height="66"> 
                            <div align="center"><span style="font-weight: 400"><font color="#000000">
								0522-2238385</font></span></div>                          </td>
                          <td width="20%" class="style2" height="66"> 
                          <div align="center">
					<font color="#000000">
					<span class="style66">
					<span style="font-size: 13pt; font-weight:400" lang="en-us">9454421060</span></span></font>
                     </div>                          </td>
                          <td width="19%" class="style2" height="66"> 
                          <div align="center"><span style="font-weight: 400"><font color="#000000">0522-2238385</font></span></div>                          </td>
                        </tr>
                      </tbody></table>
              </div>