<div class="tab-pane fade" id="guarante_sevaye" role="tabpanel" aria-labelledby="guarante_sevaye-tab">
                



<h2 class="row justify-content-md-center"><u style="text-align: center">जनहित गारंटी अधिनियम -2011 के अधीन राज्य संपत्ति विभाग के सेवाएँ</u> </h2>
<table class="table" width="976" height="421" border="2" align="center" cellpadding="2" cellspacing="2" >
                  <tbody>
                    <tr bgcolor="#006699">
                      <th width="5" valign="top" height="25" align="center">
                        <div align="center" class="style66">क्र.</div></th>
                      <th width="150" valign="top" height="25" align="center">
                        <div align="center" class="style66">अधिसूचित सेवाएँ </div>
                      </th>
                      <th width="151" valign="top" height="25" align="center" colspan="3">
                        <div align="center" class="style66">निस्तारण की समय सीमा  </div>
                        <!-- <table class="table">
                          <tbody> -->
                            <!-- <tr bgcolor="#006699"> -->
                              
                            <!-- </tr> -->
                          <!-- </tbody>
                        </table> -->
                      </th>
                    </tr>
                    <tr cellpadding="3" cellspacing="3">
                      <th colspan="1"></th>
                      <th colspan="1"></th>
                      <th width="5" valign="top" height="25" align="center" bgcolor="#006699"><div align="center" class="style66">आवेदन की तिथि से </div></th>
                              <th width="5" valign="top" height="25" align="center" bgcolor="#006699"><div align="center" class="style66">प्रथम अपील</div></th>
                              <th width="5" valign="top" height="25" align="center" bgcolor="#006699"><div align="center" class="style66">द्वित्तीय अपील</div></th>
                    </tr>

                    <tr valign="top">
                      <td width="5" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">1 </span></font>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">पेंशन स्वीकृति पर निर्णय </span>
                        </font>
                      </p>
                      </td>
                      <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS"> 60 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                    </tr>

                    <tr valign="top">
                      <td width="5" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">2 </span></font>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">जी.पी.एफ. स्वीकृति पर निर्णय </span>
                        </font>
                      </p>
                      </td>
                      <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                      
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS"> 30 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">15 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">15 दिवस</span>
                          </font>
                        </p>
                      </td>
                    </tr>

                    <tr valign="top">
                      <td width="5" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">3 </span></font>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">चिकित्सा अवकाश स्वीकृति पर निर्णय </span>
                        </font>
                      </p>
                      </td>
                      <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                      
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS"> 15 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">15 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">15 दिवस</span>
                          </font>
                        </p>
                      </td>
                    </tr>

                    <tr valign="top">
                      <td width="5" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">4 </span></font>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">चिकित्सा प्रतिपूर्ति पर निर्णय (तकनीकि एवं अनिवार्यता परीक्षण के बाद) </span>
                        </font>
                      </p>
                      </td>
                      <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                      
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS"> 60 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                    </tr>

                    <tr valign="top">
                      <td width="5" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">5 </span></font>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">प्रोविजनल पेंशन स्वीकृति पर निर्णय </span>
                        </font>
                      </p>
                      </td>
                      <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                      
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS"> 30 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                    </tr>

                    <tr valign="top">
                      <td width="5" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">6 </span></font>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">उपार्जित अवकाश स्वीकृति पर निर्णय </span>
                        </font>
                      </p>
                      </td>
                      <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                      
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS"> 15 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">7 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">7 दिवस</span>
                          </font>
                        </p>
                      </td>
                    </tr>

                    <tr valign="top">
                      <td width="5" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">7 </span></font>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">वेतन भुगतान पर निर्णय </span>
                        </font>
                      </p>
                      </td>
                      <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                      
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS"> 15 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">7 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">7 दिवस</span>
                          </font>
                        </p>
                      </td>
                    </tr>

                    <tr valign="top">
                      <td width="5" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">8 </span></font>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">गोपनीय प्रविष्टि पर निर्णय</span>
                        </font>
                      </p>
                      </td>
                      <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                      
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS"> 30 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                    </tr>

                    <tr valign="top">
                      <td width="5" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">9 </span></font>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">सुनिश्चित कैरियर प्रोन्नयन पर निर्णय</span>
                        </font>
                      </p>
                      </td>
                      <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                      
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS"> 90 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                    </tr>

                    <tr valign="top">
                      <td width="5" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">10 </span></font>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">मृतक आश्रित नियुक्ति (सामान्य मामलों में) पर निर्णय</span>
                        </font>
                      </p>
                      </td>
                      <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                      
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS"> 90 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                      <td width="150" height="70" style="border-style: solid; border-width: 1px" align="center">
                        <p align="center" class="style66">
                          <font color="#000000"><span style="font-weight: 400">
                            <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">30 दिवस</span>
                          </font>
                        </p>
                      </td>
                    </tr>
                
                  
                  </tbody>
                </table>
              </div>