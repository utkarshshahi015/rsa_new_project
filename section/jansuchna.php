<div class="tab-pane fade" id="jansuchna" role="tabpanel" aria-labelledby="jansuchna-tab">
                <h2 class="row justify-content-md-center">जनसूचना अधिकारी </h2>
                <table class="table">
                  <tbody><tr class="MsoNormalTable" bgcolor="#006699"> 
                    <td height="29" valign="top" style="width: 49px; border-left: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-top-color:windowtext" align="center"> 
                    <font color="#FFFFFF"> 
                    <span class="style20"><strong>क्रं० सं०</strong></span></font></td>
                    <td height="29" valign="top" style="width: 408px; border-left: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-top-color:windowtext"> 
                    <p align="center" class="MsoNormal style20">
					<font color="#FFFFFF"><strong>नाम एवं पदनाम</strong></font></p></td>
                    <td height="29" valign="top" style="width: 121px; border-left: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-top-color:windowtext"> 
                    <p align="center" class="MsoNormal style20">
					<font color="#FFFFFF"><strong>कार्यालय&nbsp; नंबर </strong>
					</font></p></td>
                    <td height="29" valign="top" style="width: 90px; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid windowtext; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> 
                    <p align="center" class="MsoNormal style20">
					<font color="#FFFFFF"><strong>मोबाइल&nbsp; नंबर</strong></font></p></td>
                  </tr>
                  <tr class="MsoNormalTable"> 
                    <td height="40" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> 
                    <span lang="en-us">1.</span></td>
                    <td height="40" valign="top" bordercolor="#000000" style="width: 408px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> 
                    <p class="MsoNormal">श्री शुभ्रांत कुमार शुक्ल,&nbsp; 
					<b><u>अपीलीय </u></b>
					<u><font color="#000000"><span style="font-weight: 700">
					अधिकारी</span><span lang="en-us" style="font-weight: 400">
					</span></font></u><b><u><span lang="en-us">, </span></u></b>विशेष<span lang="en-us"> </span>सचिव 
                    एवं राज्य संपत्ति अधिकारी</p></td>
                    <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 121px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"><font face="Kruti Dev 010" class="style65">
                    <span style="font-size: 14.0pt; font-family: Times New Roman"><span class="style14">
                    <font style="font-size: 13pt">2238203<span lang="en-us">, 
					2213456</span></font></span></span><font face="Times New Roman"><span class="style14"><font style="font-size: 13pt"><br>
223841(fax)</font></span></font></font></td>
                    <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 90px; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                    <font face="Times New Roman" style="font-size: 13pt">9415011854</font></td>
                  </tr>
                  </tbody></table>



<h2 class="row justify-content-md-center"><u>जन सूचना अधिकारी</u> </h2>
<table class="table" width="976" height="421" border="2" align="center" cellpadding="2" cellspacing="2" >
                  <tbody><tr bgcolor="#006699">
                    <th width="170" valign="top" height="25" align="center"><div align="center" class="style66">नाम </div></th>
                    <th width="151" valign="top" height="25" align="center"><div align="center" class="style66">पद नाम </div></th>
                    <th width="213" valign="top" height="25" align="center"><div align="center" class="style66">कार्य विवरण </div></th>
                    <th width="158" valign="top" height="25" align="center"><div align="center" class="style66">कार्यालय </div></th>
                    <th width="126" valign="top" height="25" align="center"><div align="center" class="style66">फोन&nbsp; न0 कार्यालय </div></th>
                    <th width="104" valign="top" height="25" align="center"><p align="center" class="style66">फोन&nbsp; न0 आवास </p></th>
                  </tr>
                  <tr valign="top">
                    <td width="170" height="70" style="border-style: solid; border-width: 1px" align="center">
                    <p align="center" class="style66">
          <font color="#000000"><span style="font-weight: 400">
          <span style="font-size: 12.0pt; font-family: Mangal,serif">
          श्री</span><span style="font-size: 12.0pt; font-family: 'Times New Roman',serif">
          </span>
          <span style="font-size: 12.0pt; font-family: Mangal,serif">
          रविकेश</span><span style="font-size: 12.0pt; font-family: 'Times New Roman',serif">
          </span>
          <span style="font-size: 12.0pt; font-family: Mangal,serif">
          चन्द्र</span></span></font></p></td>
                    <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
          <u><font color="#000000"><span style="font-weight: 400">
          अनुभाग अधिकारी</span></font>, राज्य संपत्ति विभाग
                  </u></td>
                    <td width="213" style="border-style: solid; border-width: 1px" height="70" align="center">अनुभाग -
          <span lang="en-us">1</span> से सम्बंधित समस्त 
          सूचनाये उपलब्ध कराना!</td>
                    <td width="158" style="border-style: solid; border-width: 1px" height="70" align="center">नवीन भवन, उत्तर प्रदेश सचिवालय, लखनऊ !</td>
                    <td width="126" style="border-style: solid; border-width: 1px" height="70" align="center"><p class="style5">  
                    <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
          2213454</span></p></td>
                    <td width="104" style="border-style: solid; border-width: 1px" height="70" align="center">
                    <p class="style5">
                    <span style="font-size: 13.0pt; font-family: 'Times New Roman',serif">
          9454413090</span></p></td>
                  </tr>
                  <tr valign="top">
                    <td style="border-style: solid; border-width: 1px" width="170" height="68" align="center"><p class="MsoNormal">
          <span style="font-size: 12.0pt; font-family: Mangal,serif">
          श्री</span><span style="font-size: 12.0pt; font-family: 'Times New Roman',serif">
          </span>
          <span style="font-size: 12.0pt; font-family: Mangal,serif">
          महेन्द्र</span><span style="font-size: 12.0pt; font-family: 'Times New Roman',serif">
          </span>
          <span style="font-size: 12.0pt; font-family: Mangal,serif">
          कुमार</span><span style="font-size: 12.0pt; font-family: 'Times New Roman',serif">
          </span>
          <span style="font-size: 12.0pt; font-family: Mangal,serif">
          वर्मा</span></p></td>
                    <td style="border-style: solid; border-width: 1px" width="151" height="68" align="center">
          <u><font color="#000000"><span style="font-weight: 400">
          अनुभाग अधिकारी</span></font>, राज्य संपत्ति विभाग
                </u></td>
                    <td style="border-style: solid; border-width: 1px" width="213" height="68" align="center"><p>
          अनुभाग - <span lang="en-us">2</span> से सम्बंधित समस्त 
          सूचनाये उपलब्ध कराना!</p></td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:158px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="68" align="center">
          नवीन भवन, उत्तर प्रदेश सचिवालय, लखनऊ !</td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:126px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="68" align="center">
          <font face="Kruti Dev 010">
                    <span class="style65"><font face="Times New Roman" style="font-size: 13pt">2213457</font></span></font><font face="Times New Roman" style="font-size: 13pt">
                    </font> </td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:104px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="68" align="center">
          <span style="font-size: 13.0pt; font-family: 'Times New Roman',serif">
          9454413990</span><span style="font-size: 12.0pt; font-family: 'Times New Roman',serif">
          </span>
                     <p>&nbsp;</p></td>
                  </tr>
          <tr>
                    <td style="border-style: solid; border-width: 1px" height="71" align="center">
          श्री गुरु प्रसाद तिवारी, </td>
                    <td style="border-style: solid; border-width: 1px" height="71" align="center">
          <u><font color="#000000"><span style="font-weight: 400">
          अनुभाग अधिकारी</span></font>, राज्य संपत्ति विभाग</u></td>
                    <td style="border-style: solid; border-width: 1px" height="71" align="center">
          अनुभाग -
          <span lang="en-us">3</span> से सम्बंधित समस्त 
          सूचनाये उपलब्ध कराना<u>!</u></td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:158px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="71" align="center">
          <u>नवीन भवन, उत्तर प्रदेश सचिवालय, लखनऊ !</u></td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:126px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="71" align="center"><p align="center" class="MsoNormal"> 
                    
                    <font style="font-size: 13pt"><span style="font-family: Times New Roman"><span class="style66">
          <font color="#000000"><span style="font-weight: 400">2213439</span></font></span></span></font> <br>
                    2238385 (Fax) </p> </td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:104px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="71" align="center">
                    <p align="center" class="MsoNormal">9454411527</p>
          <p>&nbsp; </p></td>
                  </tr>
          <tr>
                    <td style="border-style: solid; border-width: 1px" height="60" align="center">
          श्री दर्शन पाल आर्य</td>
                    <td style="border-style: solid; border-width: 1px" height="60" align="center">
          <u><font color="#000000"><span style="font-weight: 400">
          अनुभाग अधिकारी</span></font>, राज्य संपत्ति विभाग
                  </u></td>
                    <td style="border-style: solid; border-width: 1px" height="60" align="center">
          अनुभाग - 4 से सम्बंधित समस्त सूचनाये उपलब्ध कराना !</td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:158px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="60" align="center">
          नवीन भवन, उत्तर प्रदेश सचिवालय, लखनऊ !</td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:126px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="60" align="center">
                    <font face="Times New Roman" style="font-size: 13pt">2213455</font></td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:104px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="60" align="center"> 
                    <span lang="en-us">9454412247</span></td>
                  </tr>
          <tr>
                    <td style="border-style: solid; border-width: 1px" height="60" align="center">
          &nbsp;</td>
                    <td style="border-style: solid; border-width: 1px" height="60" align="center">
          <u><font color="#000000"><span style="font-weight: 400">
          अनुभाग अधिकारी</span></font>, राज्य संपत्ति विभाग
                  </u></td>
                    <td style="border-style: solid; border-width: 1px" height="60" align="center">
          <u>अनुभाग - 
          <span lang="en-us">5</span> से सम्बंधित समस्त सूचनाये उपलब्ध कराना !</u></td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:158px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="60" align="center">
          <u>नवीन भवन, उत्तर प्रदेश सचिवालय, लखनऊ !</u></td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:126px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="60" align="center">
          &nbsp;</td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:104px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="60" align="center">
                    &nbsp;</td>
                  </tr>
          <tr>
                    <td style="border-style: solid; border-width: 1px" height="60" align="center">
          श्री राजेन्द्र कुमार </td>
                    <td style="border-style: solid; border-width: 1px" height="60" align="center">
          (अनु सचिव,&nbsp; लेखा), 
          राज्य संपत्ति विभाग </td>
                    <td style="border-style: solid; border-width: 1px" height="60" align="center">
          अनुभाग - लेखा<span lang="en-us"> </span>से 
          सम्बंधित समस्त सूचनाये उपलब्ध कराना !</td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:158px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="60" align="center">
          नवीन भवन, उत्तर प्रदेश सचिवालय, लखनऊ !</td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:126px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="60" align="center">
          <span lang="en-us">-</span></td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:104px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="60" align="center">
                    <span class="style65">
                    <font face="Times New Roman" style="font-size: 13pt">9454411888</font></span>&nbsp;</td>
                  </tr>
                  <tr>
                    <td style="border-style: solid; border-width: 1px" height="71" align="center">
          श्री संजय&nbsp; दुबे</td>
                    <td style="border-style: solid; border-width: 1px" height="71" align="center">
          &nbsp;मुख्य व्यवस्था अधिकारी (प्र<span id="6_TRN_36">0</span>), 
          राज्य संपत्ति विभाग </td>
                    <td style="border-style: solid; border-width: 1px" height="71" align="center">
          योजना कोष्दक से सम्बंधित समस्त सूचनाये उपलब्ध कराना</td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:158px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="71" align="center">
          नवीन भवन, उत्तर प्रदेश सचिवालय, लखनऊ !</td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:126px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="71" align="center">
          <span lang="en-us">
          <font face="Times New Roman" style="font-size: 13pt">2213441</font></span></td>
                    <td class="xl2614872" style="border-top:1px solid;border-left:1px solid;
  width:104px; border-right-style:solid; border-right-width:1px; border-bottom-style:solid; border-bottom-width:1px" height="71" align="center">
          <font face="Times New Roman" style="font-size: 13pt">
          9415091361</font></td>
                  </tr>
                </tbody></table>
              </div>