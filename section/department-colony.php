<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
      <h2 class="row justify-content-md-center" style="font-family:times">विभाग के अधीन कालोनियाँ</h2>
    <table width="100%" border="2" cellspacing="2" cellpadding="2">
      <tbody>
        <tr>
          <td width="5%" valign="top">
            <div align="center" class="style8"><strong>1</strong></div>
          </td>
          <td width="40%" valign="top">कालिदास मार्ग स्थित मंत्री आवास </td>
          <td width="5%" valign="top">
            <div align="center" class="style9">27</div>
          </td>
          <td width="46%" valign="top">राजेन्द्र नगर कालोनी </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">2</div>
          </td>
          <td valign="top">विक्रमादित्य मार्ग स्थित पुराना </td>
          <td valign="top">
            <div align="center" class="style9">28</div>
          </td>
          <td valign="top">नैपियर रोड कालोनी </td>
        </tr>
        <tr>
          <td height="20" valign="top">
            <div align="center" class="style9">3</div>
          </td>
          <td valign="top">मंत्री आवास </td>
          <td valign="top">
            <div align="center" class="style9">29</div>
          </td>
          <td valign="top">अशोक नगर कालोनी </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">4</div>
          </td>
          <td valign="top">विक्रमादित्य मार्ग मंत्री आवास</td>
          <td valign="top">
            <div align="center" class="style9">30</div>
          </td>
          <td valign="top">टिकैतराय कालोनी</td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">5</div>
          </td>
          <td valign="top">नया मंत्री आवास, विक्रमादित्य मार्ग</td>
          <td valign="top">
            <div align="center" class="style9">31</div>
          </td>
          <td valign="top">सीतापुर रोड सेक्टर डी&nbsp; </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">6</div>
          </td>
          <td valign="top">राजभवन कालोनी</td>
          <td valign="top">
            <div align="center" class="style9">32</div>
          </td>
          <td valign="top">विधायक निवास-3 स्थित चतुर्थ श्रेणी आवास </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">7</div>
          </td>
          <td valign="top">माल एवेन्यू</td>
          <td valign="top">
            <div align="center" class="style9">33</div>
          </td>
          <td valign="top">विधायक निवास-5 स्थित चतुर्थ श्रेणी आवास </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">8</div>
          </td>
          <td valign="top">गौतमपल्ली </td>
          <td valign="top">
            <div align="center" class="style9">34</div>
          </td>
          <td valign="top">सचिवालय कालोनी महानगर </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">9</div>
          </td>
          <td valign="top">ला-प्लास </td>
          <td valign="top">
            <div align="center" class="style9">35</div>
          </td>
          <td valign="top">दिलकुशा कालोनी </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">10</div>
          </td>
          <td valign="top">बटलर पैलेस कालोनी </td>
          <td valign="top">
            <div align="center" class="style9">36</div>
          </td>
          <td valign="top">बादशाह नगर कालोनी </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">11</div>
          </td>
          <td valign="top">डालीबाग कालोनी.</td>
          <td valign="top">
            <div align="center" class="style9">37</div>
          </td>
          <td valign="top">गुलिस्तां कालोनी </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">12</div>
          </td>
          <td valign="top">न्यू ड्राइवर कालोनी, डालीबाग</td>
          <td valign="top">
            <div align="center" class="style9">38</div>
          </td>
          <td valign="top">इन्दिरा नगर सेक्टर-4</td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">13</div>
          </td>
          <td valign="top">38-डी मेजर बैंक रोड </td>
          <td valign="top">
            <div align="center" class="style9">39</div>
          </td>
          <td valign="top">इन्दिरा नगर सेक्टर-5 </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">14</div>
          </td>
          <td valign="top">दारूलशफा कम्पाउण्ड स्थित आवास</td>
          <td valign="top">
            <div align="center" class="style9">40</div>
          </td>
          <td valign="top">इन्दिरा नगर सेक्टर-16 </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">15</div>
          </td>
          <td valign="top">लाल बहादूर शास्त्री मार्ग </td>
          <td valign="top">
            <div align="center" class="style9">41</div>
          </td>
          <td valign="top">इन्दिरा नगर सेक्टर-21 </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">16</div>
          </td>
          <td valign="top">कैबिनेट गंज / बन्दरिया बाग </td>
          <td valign="top">
            <div align="center" class="style9">42</div>
          </td>
          <td valign="top">विष्णुपुरी कालोनी </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">17</div>
          </td>
          <td valign="top">साउथ एवेन्यू </td>
          <td valign="top">
            <div align="center" class="style9">43</div>
          </td>
          <td valign="top">अलीगंज कालोनी सेक्टर-एल0के0एस0 </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">18</div>
          </td>
          <td valign="top">नेहरू एन्क्लेव (गोमती नगर)</td>
          <td valign="top">
            <div align="center" class="style9">44</div>
          </td>
          <td valign="top">अलीगंज सेक्टर-आई </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">19</div>
          </td>
          <td valign="top">निराला नगर </td>
          <td valign="top">
            <div align="center" class="style9">45</div>
          </td>
          <td valign="top">अलीगंज सेक्टर-जे</td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">20</div>
          </td>
          <td valign="top">गोमती नगर फेज-।। </td>
          <td valign="top">
            <div align="center" class="style9">46</div>
          </td>
          <td valign="top">अलीगंज सेक्टर-ई (एम0आई0जी0)</td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">21</div>
          </td>
          <td valign="top">डी. एम. कम्पाउण्ड </td>
          <td valign="top">
            <div align="center" class="style9">47</div>
          </td>
          <td valign="top">अलीगंज सेक्टर- ई (एम0जी0)</td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">22</div>
          </td>
          <td valign="top">कैसरबाग कालोनी</td>
          <td valign="top">
            <div align="center" class="style9">48</div>
          </td>
          <td valign="top">मोतीझील कालोनी</td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">23</div>
          </td>
          <td valign="top">लालबाग कालोनी </td>
          <td valign="top">
            <div align="center" class="style9">49</div>
          </td>
          <td valign="top">कानपुर रोड कालोनी</td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">24</div>
          </td>
          <td valign="top">डायमण्ड डेरी कालोनी </td>
          <td valign="top">
            <div align="center" class="style9">50</div>
          </td>
          <td valign="top">विधायक निवास-4/6 सर्वेन्ट क्वाटर </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">25</div>
          </td>
          <td valign="top">ऐशबाग कालोनी </td>
          <td valign="top">
            <div align="center" class="style9">51</div>
          </td>
          <td valign="top">तालकटोरा कालोनी </td>
        </tr>
        <tr>
          <td valign="top">
            <div align="center" class="style9">26</div>
          </td>
          <td valign="top">हैवलक रोड </td>
          <td valign="top">
            <div align="center" class="style9">52</div>
          </td>
          <td valign="top">योजना नगर </td>
        </tr>
      </tbody>
    </table>
  <div class="row" style="margin-top:10px">
    <h5 class="row justify-content-md-center">अनावासीय भवन</h5>
    <table class="table">
      <tbody>
        <tr valign="top">
          <td width="17%" align="center"><b>कार्यालय भवन</b></td>
          <td width="78%">मुख्य सचिवालय परिसर स्थित कार्यालय भवन, एनेक्सी भवन, विकास भवन, बापू भवन, इंदिरा भवन, जवाहर भवन, योज़ना भवन<span lang="en-us">, लोक भवन </span>&nbsp;| </td>
        </tr>
        <tr valign="top">
          <td width="17%" height="19" align="center"><b>विधायक निवास </b></td>
          <td width="78%" height="19"> दरूलसफा प्रांगढ़ स्थित विधायक निवास -1, विधायक निवास -2(पुराना), विधायक निवास -2(नया), विधायक निवास -3, विधायक निवास -4, विधायक निवास -5, विधायक निवास -6, बटलर रोड स्थित बहुखंडी मंत्री आवास | </td>
        </tr>
        <tr valign="top">
          <td width="17%" align="center"><b>
                
                <o:p>अतिथि गृह </o:p>
              </b></td>
          <td width="78%">लखनऊ स्थित अति विशिष्ट अतिथि गृह, एम0 जी0 मार्ग, विशिष्ट अतिथि गृह डालीबाग़, ट्रांजिस्ट हॉस्टल डालीबाग़, राज्य अतिथि गृह, मीराबाई मार्ग, राज्य अतिथि गृह विक्रमादित्य मार्ग, लखनऊ, नयी दिल्ली स्थित उत्तर प्रदेश भवन, उत्तर प्रदेश सदन, कोलकाता स्थित उत्तर प्रदेश राज्य अतिथि, नवी मुम्बई&nbsp; स्थित उत्तर प्रदेश राज्य अतिथि गृह</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>