<div class="tab-pane fade" id="contactnumber" role="tabpanel" aria-labelledby="contactnumber-tab">
                <h2 class="row justify-content-md-center">महत्वपूर्ण दूरभाष सo </h2>
                <table class="table table-bordered">
                  <tbody>
                    <tr class="MsoNormalTable">
                      <td height="16" valign="top" style="width: 49px; border-left: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-top-color:windowtext" align="center"> <span class="style20"><strong>क्रं०सं०</strong></span> </td>
                      <td height="16" valign="top" style="width: 561px; border-left: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-top-color:windowtext">
                        <p align="center" class="MsoNormal style20"><strong>नाम एवं पदनाम</strong></p>
                      </td>
                      <td height="16" valign="top" style="width: 205px; border-left: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-top-color:windowtext">
                        <p align="center" class="MsoNormal style20"><strong>कार्यालय&nbsp; नंबर </strong></p>
                      </td>
                      <td width="124" height="16" valign="top" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid windowtext; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal style20"><strong>मोबाइल&nbsp; नंबर</strong></p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="35" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">1</span> </td>
                      <td height="35" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p><span lang="en-us">श्री शशि प्रकाश गोयल, प्रमुख़</span> सचिव राज्य संपत्ति विभाग </p>
                      </td>
                      <td height="35" align="center" valign="top" bordercolor="#000000" class="style65" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> 
                        <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
                        2237135, 2238219, 2235413 (fax)</span> </td>
                      <td height="35" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> <span lang="en-us">-</span> </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="40" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">2</span> </td>
                      <td height="40" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal">श्री शुभ्रांत कुमार शुक्ल,&nbsp; विशेष<span lang="en-us"> </span>सचिव एवं राज्य संपत्ति अधिकारी </p>
                      </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <font face="Kruti Dev 010" class="style65">
                        <span style="font-size: 14.0pt; font-family: Times New Roman"><span class="style14">
                        <font style="font-size: 13pt">2238203<span lang="en-us">,2213456</span></font></span>
                        </span><font face="Times New Roman"><span class="style14"><font style="font-size: 13pt"><br>            223841(fax)</font></span>
                        </font>
                        </font>
                      </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> <font face="Times New Roman" style="font-size: 13pt">9415011854</font> </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">3</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal">श्री सुधीर कुमार रूंगटा, सहायक राज्य संपत्ति अधिकारी, राज्य संपत्ति विभाग </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">2237591, 2214004</span> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"><span lang="en-us">9454412032</span> </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">4</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal"><span style="font-weight: 400">
                        <font size="3">श्री अशोक कुमार सिंह<span lang="en-us">,</span> संयुक्त सचिव<span lang="en-us">,</span> राज्य संपत्ति विभाग</font>
                          </span>
                        </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font style="font-size: 13pt; font-family:Times New Roman">2213970<span lang="en-us">,2238231</span></font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span lang="en-us">9454411422</span> </p>
                      </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">5</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal"><span lang="en-us">-</span></p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <span lang="en-us">2213164</span> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> <span lang="en-us">-</span> </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">6</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> <span style="font-weight: 400"><font size="3">श्री राजा राम द्धिवेदी<span lang="en-us">,</span> अनु सचिव<span lang="en-us">,</span> राज्य संपत्ति विभाग</font>
                        </span>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> <span lang="en-us">2213601</span> </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> <span lang="en-us">9451113866</span> </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">7</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> श्री रमा शंकर&nbsp; यादव, अनु सचिव, राज्य संपत्ति विभाग </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> <span lang="en-us">2213522</span> </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> <span lang="en-us">-</span> </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">8</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> <span lang="en-us">-</span> </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> <span lang="en-us">2213844</span> </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> <span lang="en-us">
                        <font face="Times New Roman" style="font-size: 13pt">-</font></span> </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">9</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <div>&nbsp;श्री राजेन्द्र कुमार (<span style="font-weight: 400"><font size="3">अनु सचिव</font></span> लेखा)<span style="font-weight: 400"><font size="3"><span lang="en-us">,</span> राज्य संपत्ति विभाग</font>
                          </span>
                        </div>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010">
                        <span style="font-family: Times New Roman"> 
                        <font style="font-size: 13pt">&nbsp;-</font></span>
                          </font>
                        </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> <span class="style65">
                        <font face="Times New Roman" style="font-size: 13pt">9454411888</font></span> </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">10</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal">श्री संजय दुबे, मुख्य व्यवस्था अधिकारी (प्रा०)</p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Times New Roman" style="font-size: 13pt">2238086</font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010" style="font-size: 13pt">
                      <span style="font-family: Times New Roman"> &nbsp;9415091361</span></font><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">11</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">&nbsp;</td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">&nbsp;</td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">&nbsp;</td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">12</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal"> श्री रविकेश चन्द्र, अनुभाग अधिकारी, अनुभाग - 1 </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
                      2213454</span> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
                      9454413090</span> </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="40" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">13</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal"> श्री महेन्द्र कुमार वर्मा, अनुभाग अधिकारी, अनुभाग - 2 </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010">
                      <span style="font-family: Times New Roman"> 
                      <font style="font-size: 13pt">&nbsp;</font></span><span class="style65"><font face="Times New Roman" style="font-size: 13pt">2213457</font></span></font><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span class="style66">
                      <span style="font-size: 13pt" lang="en-us">9454413990</span></span>
                        </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">14</span> </td>
                      <td height="40" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal"> श्री गुरु प्रसाद तिवारी, अनुभाग अधिकारी, अनुभाग - 3 </p>
                      </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font style="font-size: 13pt"><span style="font-family: Times New Roman"><span class="style66">2213439</span></span></font>
                          <br> 2238385 (Fax) </p>
                      </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> 9454411527 </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">15</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal"> श्री दर्शन पाल आर्य, &nbsp;अनुभाग अधिकारी, अनुभाग - 4 </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="style61"> <font face="Times New Roman" style="font-size: 13pt">2213455</font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span lang="en-us">9454412247</span> </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="38" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">16</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> श्री रमेश चन्द्र साहू<span lang="en-us">,</span>&nbsp;अनुभाग अधिकारी, अनुभाग - 5 </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> <span lang="en-us">-</span> </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> <span lang="en-us">9454413597</span> </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">17</span> </td>
                      <td height="38" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">श्री अंबरीष श्रीवास्तव, मोटर सहायक </td>
                      <td height="38" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010">
                      <span style="font-family: Times New Roman"> 
                      <font style="font-size: 13pt">&nbsp;</font></span>
                          </font><font face="Times New Roman" style="font-size: 13pt">
                      </font> <font face="Kruti Dev 010"> 
                      <span style="font-family: Times New Roman">
                      <font style="font-size: 13pt">2238385<span lang="en-us">(fax),&nbsp; 
                      2213439</span></font></span>
                          </font>
                        </p>
                      </td>
                      <td height="38" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> <span class="style66">
                        <span style="font-size: 13pt" lang="en-us">9454421060, 9305657755</span></span>
                      </td>
                    </tr>
                    <tr>
                      <td height="56" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">18</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal">श्री संजय चौधरी, व्यवस्था अधिकारी, बहुखंडी मंत्री आवास</p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman">
                      <font style="font-size: 13pt">2208227</font></span><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman">
                      <font style="font-size: 13pt">9450004700</font></span><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">19</span> </td>
                      <td height="56" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">श्रीमती सुधा कुमार, मुख्य व्यवस्था अधिकारी<span lang="en-us">, विधायक निवास -1 (खण्ड 'अ ') व सी ब्लॉक, दारुल शफा, </span>लखनऊ 
                      </td>
                      <td height="56" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman">
                        <font style="font-size: 13pt">2625914</font></span><font face="Times New Roman" style="font-size: 13pt">
                        </font> </p>
                        <p align="center" class="MsoNormal"> &nbsp;</p>
                      </td>
                      <td height="56" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010">
                        <span style="font-family: Times New Roman"> 
                        <font style="font-size: 13pt">9453044801</font></span>
                          </font>
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">20</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal">श्री रवि कान्त चौधरी, व्यवस्था अधिकारी, विधायक निवास - १ (ब०)</p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman">
                        <font style="font-size: 13pt">2201066</font></span><font face="Times New Roman" style="font-size: 13pt">
                        </font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010" style="font-size: 13pt">
                        <span style="font-family: Times New Roman"> 9415002112</span></font><font face="Times New Roman" style="font-size: 13pt">
                        </font> </p>
                      </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">21</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal"> श्रीमती रश्मि खरे, कार्यवाहक व्यवस्था अधिकारी, विधायक निवास - 2 (नया) </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal" style="margin-left:12.6pt;text-indent:-12.6pt"> <font face="Kruti Dev 010">
                        <span style="font-family: Times New Roman"> 
                        <font style="font-size: 13pt">2629753</font></span>
                          </font><font face="Times New Roman" style="font-size: 13pt">
                        </font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
                        9415549778</span><font face="Times New Roman" style="font-size: 13pt">
                        </font> </p>
                      </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">22</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal"> श्री फिरोज़ कुमार, कार्यवाहक व्यवस्था अधिकारी, विधायक निवास - 2 (पुराना) </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <span style="font-size: 13pt" lang="en-us">2614397</span> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-size: 13pt" lang="en-us">9415582392, 
                      9454421134</span> </p>
                      </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">23</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal"> श्री नसीम अख्तर, व्यवस्था अधिकारी, विधायक निवास - ३&nbsp; (ओ० सी० आर०)
                          <br> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010">
                      <span style="font-family: Times New Roman"> 
                      <font style="font-size: 13pt">2237486</font></span>
                          </font><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span class="style58"><span class="style45">
                      <span class="style65">
                      <font face="Times New Roman" style="font-size: 13pt">9454456084 </font> </span></span>
                          </span><font face="Times New Roman" style="font-size: 13pt">
                       &nbsp;</font> </p>
                      </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-bottom-color:windowtext" align="center"> <span lang="en-us">24</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">श्री रजा&nbsp;आब्दी, प्रभारी विधायक निवास - ४ रोयल होटल</td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman">
                      <font style="font-size: 13pt">2236016</font></span><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> <span class="style65" style="font-family: &quot;Times New Roman&quot;">
                      <font style="font-size: 13pt">9415544026</font></span> </td>
                    </tr>
                    <tr>
                      <td height="40" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-bottom-color:windowtext" align="center"> <span lang="en-us">25</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal">श्रीमती किरन सिंह , प्रभारी, विधायक निवास&nbsp; - ५ </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010" style="font-size: 13pt">
                      <span style="font-family: Times New Roman"> 2205220</span></font><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
                      9454421183</span> </p>
                      </td>
                    </tr>
                    <tr>
                      <td height="56" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">26</span> </td>
                      <td height="40" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-bottom-color:windowtext">
                        <p class="MsoNormal"> श्री मनोज कुमार, कार्यवाहक व्यवस्था अधिकारी, विधायक निवास - 6, पार्क रोड, लखनऊ! </p>
                      </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-bottom-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010">
                        <span style="font-family: Times New Roman"> 
                        <font style="font-size: 13pt">2236715</font></span>
                          </font><font face="Times New Roman" style="font-size: 13pt">
                        </font> </p>
                      </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-size: 13pt" lang="en-us">9454421184, 
                      9473896758</span> </p>
                      </td>
                    </tr>
                    <tr>
                      <td height="42" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">27</span> </td>
                      <td height="56" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal">श्री राहुल तन्खा, व्यवस्था अधिकारी, ट्रांजिट हास्टल, नेहरू इन्क्लेव, मंत्री आवास विभूति खण्ड लखनऊ </p>
                      </td>
                      <td height="56" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman">
                      <font style="font-size: 13pt">2209892</font></span><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                      <td height="56" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman; font-size: 13pt">
                      9415937778</span><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="56" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">28</span> </td>
                      <td height="42" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal">श्री डी० के० सिंह, व्यवस्था अधिकारी,&nbsp; अति विशिस्ट अतिथि गृह</p>
                      </td>
                      <td height="42" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center"> <font face="Kruti Dev 010" style="font-size: 13pt">
                      <span style="font-family: Times New Roman"> 2610051, 2610072, 2610076, 2235893 </span></font> <span style="font-family: Times New Roman">
                      <font style="font-size: 13pt">&nbsp;</font></span> </p>
                      </td>
                      <td height="42" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman">
                      <font style="font-size: 13pt">9415911291</font></span><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">29</span> </td>
                      <td height="56" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal"> श्री रवीन्द्र प्रताप सिंह, कार्यवाहक व्यवस्था अधिकारी, राज्य अतिथि गृह, मीराबाई मार्ग, लखनऊ ! </p>
                      </td>
                      <td height="56" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010">
                      <span style="font-family: Times New Roman"> 
                      <font style="font-size: 13pt">2287318, 2287488, 2288634</font></span>
                          </font><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                      <td height="56" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-size: 13pt" lang="en-us">9415393803</span> </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">30</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal"> श्री राजीव कुमार, व्यवस्था अधिकारी, <span lang="en-us">विशिष्ट अतिथि गृह </span> <font face="Kruti Dev 010">ड</font>ा<font face="Kruti Dev 010"><span lang="en-us">लीबाग़</span></font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> <font face="Kruti Dev 010">
                      <span style="font-family: Times New Roman"> 
                      <font style="font-size: 13pt">2208608, 522, 516,&nbsp; 620</font></span>
                        </font><font face="Times New Roman" style="font-size: 13pt">
                      </font> <font face="Kruti Dev 010">
                      <span style="font-family: Times New Roman"> 
                      <font style="font-size: 13pt">, 591 (fax)</font></span>
                        </font>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman; font-size: 13pt">
                      9450910134</span> </p>
                      </td>
                    </tr>
                    <tr>
                      <td height="40" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">31</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">&nbsp;श्री महेंद्र कुमार, कार्यवाहक व्यवस्था अधिकारी,<span lang="en-us"> 
                      </span>राज्य अतिथि गृह, विक्रमादित्य मार्ग, लखनऊ<span lang="en-us">&nbsp;
                      </span> </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010" style="font-size: 13pt">
                      <span style="font-family: Times New Roman"> 2235213</span></font><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">
                      9454421185</span> </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="40" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">32</span> </td>
                      <td height="40" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal">श्री राजीव तिवारी, व्यवस्था अधिकार, उत्तर प्रदेश सदन, चाद्क्यपुरी, &nbsp; नई दिल्ली </p>
                      </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010" style="font-size: 13pt">
                      <span style="font-family: Times New Roman"> 011-24678754, 24678735, 
                      24678750 (fax)</span></font> </p>
                      </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span class="style58"><span class="style45"><span class="style65"><font face="Kruti Dev 010">
                      <span style="font-family: Times New Roman">
                      <font style="font-size: 13pt">09810363166</font></span></font><font face="Times New Roman" style="font-size: 13pt">,&nbsp;</font></span>
                          </span>
                          </span><font face="Kruti Dev 010" style="font-size: 13pt"><span style="font-family: Times New Roman">9335566880</span></font><span class="style58"><span class="style45"><span class="style65"><font face="Times New Roman" style="font-size: 13pt"> </font> </span></span>
                          </span>
                        </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="40" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-bottom-color:windowtext" align="center"> <span lang="en-us">33</span> </td>
                      <td height="40" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext"> श्री राजेश चौबे,<span lang="en-us"> </span>प्रभारी व्यवस्था अधिकारी, उत्तर प्रदेश भवन, &nbsp; नई दिल्ली </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010" style="font-size: 13pt">
                      <span style="font-family: Times New Roman"> 011-26110151-55 ,26114775(fax)</span></font><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010" style="font-size: 13pt">
                    <span style="font-family: Times New Roman">09868111044</span></font><font face="Times New Roman" style="font-size: 13pt">
                    </font> </p>
                      </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-bottom-color:windowtext" align="center"> <span lang="en-us">34</span> </td>
                      <td height="40" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-bottom-color:windowtext"> <span style="font-family: arial, sans-serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 24px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"> श्री  </span>पी०<span style="font-family: arial, sans-serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 24px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"> के० श्रीवास्तव, प्रभारी &nbsp;कोलकाता राज्य अतिथि गृह&nbsp;</span> </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-bottom-color:windowtext"> 033-24858223, 24858237,
                        <br> 22206798 (Fax) </td>
                      <td height="40" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> 0943335788 </td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">35</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-bottom-color:windowtext">
                        <p class="MsoNormal">श्री राम सागर तिवारी, प्रभारी, व्यवस्थाधिकारी, यू0 पी0 भवन, नवी मुम्बई </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext; border-bottom-color:windowtext"> 022-27811861 </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in"> <span class="style8 style10">08108510333</span></td>
                    </tr>
                    <tr>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">36</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal">श्री शशांक चतुर्वेदी, व्यवस्था अधिकारी, जवाहर भवन </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010">
                      <span style="font-family: Times New Roman"> 
                      <font style="font-size: 13pt">2286856</font></span>
                          </font><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010">
                        <span style="font-family: Times New Roman"> 
                        <font style="font-size: 13pt">&nbsp;</font></span>
                          </font><font face="Times New Roman" style="font-size: 13pt">7668426311
                        </font> </p>
                      </td>
                    </tr>
                    <tr class="MsoNormalTable">
                      <td height="29" valign="top" bordercolor="#000000" style="width: 49px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext" align="center"> <span lang="en-us">37</span> </td>
                      <td height="29" valign="top" bordercolor="#000000" style="width: 561px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p class="MsoNormal">श्री श्रधा नन्द चौधरी, व्यवस्था अधिकारी, इंदिरा भवन</p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 205px; border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; border-right-color:windowtext">
                        <p align="center" class="MsoNormal"> <font face="Kruti Dev 010" style="font-size: 13pt">
                      <span style="font-family: Times New Roman"> 2287223</span></font> </p>
                      </td>
                      <td height="29" align="center" valign="top" bordercolor="#000000" style="width: 111; border-left: 1px solid; border-right: 1px solid windowtext; border-top: 1px solid; border-bottom: 1px solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
                        <p align="center" class="MsoNormal"> <span style="font-family: Times New Roman">
                      <font style="font-size: 13pt">9335281647</font></span><font face="Times New Roman" style="font-size: 13pt">
                      </font> </p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>