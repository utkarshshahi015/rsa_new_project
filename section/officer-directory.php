<div class="tab-pane fade" id="off-directory" role="tabpanel" aria-labelledby="off-directory-tab">
<h2 class="row justify-content-md-center">अधिकारी/कर्मचारी डायरेक्टरी </h2>
<table border="1" width="100%">
				<tbody><tr>
					<td width="323" align="justify">
					<p align="left"><a href="RSA_Camp.php" target="_blank">राज्य 
					संपत्ति अधिकारी, कैम्प कार्यालय </a></p></td>
					<td width="323" align="left">
					<a href="VidhayakNiwas_3.php" target="_blank">
					विधायक निवास - 3</a></td>
				</tr>
				<tr>
					<td width="323" align="justify">
					<p align="left"><a href="ARSA_Camp.php" target="_blank">सहायक 
					राज्य संपत्ति अधिकारी, कैम्प कार्यालय </a></p></td>
					<td width="323" align="left">
					<a href="VidhayakNiwas_4.php" target="_blank">
					विधायक निवास - 4</a></td>
				</tr>
				<tr>
					<td width="323" align="left">
					<a href="Joint_Sec.php" target="_blank">संयुक्त सचिव कैम्प 
					कार्यालय</a></td>
					<td width="323" align="left">
					<a href="VidhayakNiwas_5.php" target="_blank">
					विधायक निवास - 5</a></td>
				</tr>
				<tr>
					<td width="323" align="left">
					<a href="Anubhag_1.php" target="_blank">राज्य संपत्ति अनुभाग - 1</a></td>
					<td width="323" align="left">
					<a href="VidhayakNiwas_6.php" target="_blank">
					विधायक निवास - 6</a></td>
				</tr>
				<tr>
					<td width="323" align="left">
					<a href="Anubhag_2.php" target="_blank">राज्य संपत्ति अनुभाग - 2</a></td>
					<td align="left">
					<a href="Guest_house_VikramadityaMarg.php" target="_blank">नव 
					निर्मित राज्य अतिथिगृह </a></td>
				</tr>
				<tr>
					<td width="323" align="left">
					<a href="Anubhag_3.php" target="_blank">राज्य संपत्ति अनुभाग - 3</a></td>
					<td align="left"><a href="Transit_Hostal.php" target="_blank">
					ट्रांजिट हॉस्टल </a></td>
				</tr>
				<tr>
					<td width="323" align="justify">
					<p align="left"><a href="Anubhag_4.php" target="_blank">राज्य 
					संपत्ति अनुभाग - 4</a></p></td>
					<td align="left"><a href="Mantri_Awas.php" target="_blank">
					बहुखंडी मंत्री आवास</a></td>
				</tr>
				<tr>
					<td width="323" align="justify">
					<p align="left"><a href="Anubhag_5.php" target="_blank">राज्य संपत्ति अनुभाग - 5</a></p></td>
					<td align="left"><a href="Jawahar_bhawan.php" target="_blank">
					जवाहर भवन </a></td>
				</tr>
				<tr>
					<td width="323" align="justify">
					<p align="left"><a href="Account_section.php" target="_blank">
					राज्य संपत्ति लेखा अनुभाग </a></p></td>
					<td align="left"><a href="Indira_bhawan.php" target="_blank">
					इंदिरा भवन </a></td>
				</tr>
				<tr>
					<td width="323" align="justify">
					<a href="VidhayakNiwas_1A.php" target="_blank">
					विधायक निवास - 1 (अ)</a></td>
					<td align="justify">
					<p align="left">
					<a href="Jawahar_bhawan_VihitPradikari.php" target="_blank">विहित 
					प्राधिकारी रा0 स0 वि0</a></p></td>
				</tr>
				<tr>
					<td width="323" align="justify">
					<a href="VidhayakNiwas_1B.php" target="_blank">
					विधायक निवास - 1 (ब)</a></td>
					<td align="justify">
					<p align="left">
					<a href="yojnakosthak.php" target="_blank">योजना कोष्ठक रा0 स0 
					वि0</a></p></td>
				</tr>
				<tr>
					<td width="323" align="left">
					<a href="VidhayakNiwas_2new.php" target="_blank">
					विधायक निवास - 2 (नया)</a></td>
					<td align="left">राज्य 
					संपत्ति निदेशालय </td>
				</tr>
				<tr>
					<td width="323" align="left">
					<a href="VidhayakNiwas_Old.php" target="_blank">
					विधायक निवास - 2 (पु0 )</a></td>
					<td align="left">राज्य 
					संपत्ति अनुभाग - 3, चालक</td>
				</tr>
				</tbody></table>
</div>