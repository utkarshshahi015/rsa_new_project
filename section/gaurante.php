<div class="tab-pane fade" id="guarante" role="tabpanel" aria-labelledby="guarante-tab">
                



<h2 class="row justify-content-md-center"><u style="text-align: center">जनहित गारंटी अधिनियम -२०११ से सम्बंधित राज्य संपत्ति विभाग के अधिकारीगण</u> </h2>
<table class="table" width="976" height="421" border="2" align="center" cellpadding="2" cellspacing="2" >
                  <tbody><tr bgcolor="#006699">
                    <th width="170" valign="top" height="25" align="center"><div align="center" class="style66">उ०प्र० जनहित गारंटी अधिनियम-2011 को लागू करवाने संबंधि विभागीय पद </div></th>
                    <th width="150" valign="top" height="25" align="center"><div align="center" class="style66">नामित अधिकारी का नाम </div></th>
                    <th width="151" valign="top" height="25" align="center"><div align="center" class="style66">नामित अधिकारी का पदनाम </div></th>
                    <th width="213" valign="top" height="25" align="center"><div align="center" class="style66">दूरभाष संख्या </div></th>
                    <!-- <th width="158" valign="top" height="25" align="center"><div align="center" class="style66">कार्यालय </div></th> -->
                    <th width="126" valign="top" height="25" align="center"><div align="center" class="style66">ई.मेल &nbsp; न0 आई०डी० </div></th>
                  </tr>
                  <tr valign="top">
                    <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">पदाविहित अधिकारी </span></font>
                    </td>
                    <td width="170" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">श्री</span>
                          <span style="font-size: 12.0pt; font-family: 'Times New Roman',serif"></span>
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">राजेश </span>
                          <span style="font-size: 12.0pt; font-family: 'Times New Roman',serif"></span>
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">कुमार </span>
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">पांडेय   </span>
                        </font>
                      </p>
                    </td>
                    <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">उप सचिव </span></font>, (लेखा)
                    </td>
                    <td width="126" style="border-style: solid; border-width: 1px" height="70" align="center">
                      <p class="style5">  
                      <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">9454413008</span></p>
                    </td>
                    <!-- <td width="213" style="border-style: solid; border-width: 1px" height="70" align="center">अनुभाग -
                      <span lang="en-us">1</span> से सम्बंधित समस्त सूचनाये उपलब्ध कराना!
                    </td> -->
                    <td width="158" style="border-style: solid; border-width: 1px" height="70" align="center">
                      <p class="style5">  
                      <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">rajesh.13008@gov.in</span></p>
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">प्रथम अपीलीय अधिकारी </span></font>
                    </td>
                    <td width="170" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">श्री</span>
                          <span style="font-size: 12.0pt; font-family: 'Times New Roman',serif"></span>
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">राजाराम </span>
                          <span style="font-size: 12.0pt; font-family: 'Times New Roman',serif"></span>
                          <span style="font-size: 12.0pt; font-family: Cordia New,Arial Unicode MS">द्विवेदी</span>
                        </font>
                      </p>
                    </td>
                    <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">संयुक्त सचिव</span></font>
                    </td>
                    <td width="126" style="border-style: solid; border-width: 1px" height="70" align="center">
                      <p class="style5">  
                      <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">0522-2213601</span></p>
                    </td>
                    
                    <td width="158" style="border-style: solid; border-width: 1px" height="70" align="center">
                      <p class="style5">  
                      <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">raja.13866@gov.in</span></p>
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400;font-family: Cordia New,Arial Unicode MS">द्वित्तीय अपीलीय अधिकारी </span></font>
                    </td>
                    <td width="170" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">श्री</span>
                          <span style="font-size: 12.0pt; font-family: 'Times New Roman',serif"></span>
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">बीरबल </span>
                          <span style="font-size: 12.0pt; font-family: 'Times New Roman',serif"></span>
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">सिंह</span>
                        </font>
                      </p>
                    </td>
                    <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">विशेष सचिव</span></font>
                    </td>
                    <td width="126" style="border-style: solid; border-width: 1px" height="70" align="center">
                      <p class="style5">  
                      <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">0522-2238231</span></p>
                    </td>
                    
                    <td width="158" style="border-style: solid; border-width: 1px" height="70" align="center">
                      <p class="style5">  
                      <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">birbal.12070@up.nic.in</span></p>
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">समीक्षा (reviewing) अधिकारी </span></font>
                    </td>
                    <td width="170" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">श्री</span>
                          <span style="font-size: 12.0pt; font-family: 'Times New Roman',serif"></span>
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">सतीश </span>
                          <span style="font-size: 12.0pt; font-family: 'Times New Roman',serif"></span>
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">पाल</span>
                        </font>
                      </p>
                    </td>
                    <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">विशेष सचिव एवं अपर राज्य संपत्ति अधिकारी</span></font>
                    </td>
                    <td width="126" style="border-style: solid; border-width: 1px" height="70" align="center">
                      <p class="style5">  
                      <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">0522-2213560</span></p>
                    </td>
                    
                    <td width="158" style="border-style: solid; border-width: 1px" height="70" align="center">
                      <p class="style5">  
                      <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">spal2@up.nic.in</span></p>
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">नोडल अधिकारी </span></font>
                    </td>
                    <td width="170" height="70" style="border-style: solid; border-width: 1px" align="center">
                      <p align="center" class="style66">
                        <font color="#000000"><span style="font-weight: 400">
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">श्री</span>
                          <span style="font-size: 12.0pt; font-family: 'Times New Roman',serif"></span>
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">बीरबल </span>
                          <span style="font-size: 12.0pt; font-family: 'Times New Roman',serif"></span>
                          <span style="font-size: 12.0pt; font-family: Mangal,serif">सिंह</span>
                        </font>
                      </p>
                    </td>
                    <td width="151" style="border-style: solid; border-width: 1px" height="70" align="center">
                        <font color="#000000"><span style="font-weight: 400">विशेष सचिव</span></font>
                    </td>
                    <td width="126" style="border-style: solid; border-width: 1px" height="70" align="center">
                      <p class="style5">  
                      <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">0522-2238231</span></p>
                    </td>
                    
                    <td width="158" style="border-style: solid; border-width: 1px" height="70" align="center">
                      <p class="style5">  
                      <span style="font-family: Times New Roman; font-size: 13pt" lang="en-us">birbal.12070@up.nic.in</span></p>
                    </td>
                  </tr>
                  
                </tbody></table>
              </div>